<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Liste Cours</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">
  <script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular-route.min.js"></script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('navbarheader.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include('menu.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ajouter cours</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Nouveau cours</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php echo isset($error) ? $error : ''; ?>
              <form method="post" ng-app="assignation" ng-controller="myCtrl" action="<?php echo site_url('Prof/ProfController/do_upload'); ?>" enctype="multipart/form-data"> 
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Cours</label>
                    <input type="text" name="nom" class="form-control" id="exampleInputEmail1" placeholder="Entrer le nom du cours">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Parcours</label>
                      <select name="parcour" id="classe1" class="custom-select"
                                                        required="required"
                                                        ng-options="classe.idparcour as classe.nom_parcour for classe in classe"
                                                        ng-change="getdetails1()" ng-model="parcour">
                                                        <option value="">Choisir Parcours</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Niveau</label>
                    <select name="niveau" id="classe3" class="custom-select"
                                                        ng-options="classe3.idniveau as classe3.chiffre for classe3 in classe3"
                                                        ng-change="getdetails2()" ng-model="niveau">
                                                        <option value="">Choisir Niveau</option>
                                                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">UE</label>
                    <select name="ue" id="classe4" class="custom-select"
                                                        ng-options="classe2.idmatiere as classe2.nom_matiere for classe2 in classe2"
                                                        ng-change="getdetails3()" ng-model="ue">
                                                        <option value="">Choisir UE</option>
                                                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">EC</label>
                    <select name="ec" id="classe4" class="custom-select"
                                                        ng-options="classe1.idec as classe1.nom_ec for classe1 in classe1"
                                                         ng-model="ec">
                                                        <option value="">Choisir EC</option>
                                                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Fichier</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="profilePic" id="profilePic">
                        <label class="custom-file-label" for="exampleInputFile">Choisir votre pièce jointe</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Télécharger</span>
                      </div>
                    </div>
                  </div>
                   </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success pull-right">Ajouter</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('navbarfoot.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="deleteform" name="delete" enctype="multipart/form-data" role="form">
				<input type="hidden" name="id_d" id="id_d" value="" />
					<div class="modal-header">						
						<h4 class="modal-title">Supprimer formateur</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
												<p class="text-warning"><small>Voulez-vous vraiment supprimer  définitivement ce formateur?</small></p>
</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Non">
						<input type="submit" class="btn btn-danger" value="Oui">
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/modal/modal/core/bootstrap-material-design.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- Page specific script -->
</script>
<script>
    var app = angular.module('assignation', []);
    app.controller('myCtrl', function($scope, $http, $location, $window) {
        $http({
            method: "GET",
            url: "<?php echo base_url('Api/Api/getparcour')?>"
        }).then(function mySuccess(response) {
            console.log(response);
            $scope.classe = response.data.data;
            //    $scope.response;


        }, function myError(response) {
            //   $scope.myWelcome = response.statusText;
        });
        $scope.getdetails1 = function() {
            $scope.classe2=[];
            $scope.classe1 = [];
            $scope.classe3=[];
            console.log($scope.parcour);
            $http({
                method: "POST",
                url: "<?php echo base_url('Api/Api/getparniveau')?>",
                data: {
                    idparcour: $scope.parcour
                }

            }).then(function(response) {
                console.log(response);
             //   $scope.classe2 = response.data.data;
                //  $scope.show = true;1
                
                $scope.classe2=[];
                $scope.classe3 = response.data.data;
                $scope.classe4 = [];
                //$scope.ec = []
            //    $scope.ue = response.data.Data;
                // console.log(response.data);
            });

        }
        $scope.getdetails3 = function() {
          $scope.classe1=[];
            console.log($scope.ue);
            if($scope.parcour!=null){
            $http({
                method: "POST",
                url: "<?php echo base_url('Api/Api/getec')?>",
                data: {
                    idue: $scope.ue,
                    idparcour: $scope.parcour
                }

            }).then(function(response) {
                console.log(response);
              //  $scope.classe4 = response.data.data;
                //  $scope.show = true;1
                $scope.classe1=response.data.data

               // $scope.ec = []
               // $scope.ue = response.data.Data;
                // console.log(response.data);
            });
            }
        }
        $scope.getdetails2 = function() {
            console.log($scope.userselected4);
            if($scope.parcour!=null){
            $http({
                method: "POST",
                url: "<?php echo base_url('Api/Api/getue')?>",
                data: {
                    idclasse: $scope.niveau,
                    idparcour: $scope.parcour
                }

            }).then(function(response) {
                console.log(response);
              //  $scope.classe4 = response.data.data;
                //  $scope.show = true;1
                $scope.classe2=response.data.data

              //  $scope.ec = []
             //   $scope.ue = response.data.Data;
                // console.log(response.data);
            });
            }
        }
    });
    </script>
</body>
</html>

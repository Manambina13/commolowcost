<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Liste Cours</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome --><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('navbarheader.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include('menu.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Liste des cours</h1>
          </div>
         </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-header">
                <h3 class="card-title">Liste des cours ajoutées et envoyées</h3>
                <div class="col-md-12" align="right">
                                        <span title="Ajouter un cour"> <a href="<?php echo base_url('Etudiant/ETController/addcours'); ?>"><button class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button></a></span>
                                    </div>
                                    
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nom</th>
                    <th>Niveau</th>
										<th>Matiere</th>
                    <th>Lien</th>
                  
                  </tr>
                  </thead>
                  <tbody>
                 
                  <?php for($i=0;$i<count($result);$i++)  {  ?>
                    <tr>
                    <td><?php echo $result[$i]->nom; ?></td>
                    <td>L<?php echo $result[$i]->idniveau; ?></td>
										<td><?php echo $result[$i]->nom_matiere; ?></td>
                    <td><a href="<?php echo $result[$i]->lien; ?>">Telecharger</a></td>
                    </tr>
                  <?php  } ?>
                                         
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('navbarfoot.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="deleteform" name="delete" enctype="multipart/form-data" role="form">
				<input type="hidden" name="id_d" id="id_d" value="" />
					<div class="modal-header">						
						<h4 class="modal-title">Supprimer formateur</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
												<p class="text-warning"><small>Voulez-vous vraiment supprimer  définitivement ce formateur?</small></p>
</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Non">
						<input type="submit" class="btn btn-danger" value="Oui">
					</div>
				</form>
			</div>
		</div>
	</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

<script>
$(document).ready(function(){
	// Activate tooltip
	$('[data-toggle="tooltip"]').tooltip();
	
	// Select/Deselect checkboxes
	var checkbox = $('table tbody input[type="checkbox"]');
	$("#selectAll").click(function(){
		if(this.checked){
			checkbox.each(function(){
				this.checked = true;                        
			});
		} else{
			checkbox.each(function(){
				this.checked = false;                        
			});
		} 
	});
	checkbox.click(function(){
		if(!this.checked){
			$("#selectAll").prop("checked", false);
		}
	});
	$('#editpicture').on('show.bs.modal', function(e) {
		var id=$(e.relatedTarget).data('id');
		console.log(id);
		$(e.currentTarget).find('input[name="id_u"]').val(id);
	});
	$('#editEmployeeModal').on('show.bs.modal', function(e) {
		var id=$(e.relatedTarget).data('id');
		var nom=$(e.relatedTarget).data('nom');
		var prenom=$(e.relatedTarget).data('prenom');
		var email=$(e.relatedTarget).data('email');
		$(e.currentTarget).find('input[name="id_e"]').val(id);
		$(e.currentTarget).find('input[name="prenom_e"]').val(prenom);
		$(e.currentTarget).find('input[name="name_e"]').val(nom);
		$(e.currentTarget).find('input[name="email_e"]').val(email);
	});
	$('#deleteEmployeeModal').on('show.bs.modal', function(e) {
		var id=$(e.relatedTarget).data('id');
		console.log(id);
		$(e.currentTarget).find('input[name="id_d"]').val(id);
	});
	$("#EditPictureform").submit(function(event){
        
		var name= $("#id_u").val();
		var fd = new FormData();
        var files = $('#file_e')[0].files;
		fd.append('file',files[0]);
		fd.append('name',name);
		console.log(fd);
		editpictureForm(fd);
		return false;
	})
	$("#contactForm").submit(function(event){
        
      
		var name= $("#nom").val();
		var prenom=$("#prenom").val();
		var email=$("#mail").val();
		console.log(name);
		console.log(prenom);
		console.log(email);
		var fd = new FormData();
        var files = $('#file')[0].files;
		fd.append('file',files[0]);
		fd.append('name',name);
		fd.append('prenom',prenom);
		fd.append('email',email)
		console.log(fd);
		submitForm(fd);
		return false;
	});
	
	$("#editForm").submit(function(event){
        var formData = {
			id:$("#id_e").val(),
            name: $("#name_e").val(),
			prenom: $("#prenom_e").val(),
			email: $("#email_e").val(),
        };
		console.log("ato bowo");
        submitEdit(formData);
		return false;
	});
	
	$("#deleteform").submit(function(event){
        var formData = {
			id:$("#id_d").val(),
		};
		console.log(formData)
        submitDelete(formData);
		return false;
	});
});

function submitDelete(formData){
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Prof/ProfController/submitdelete')?>",
	   cache:false,
	   data: formData,
	   dataType: 'json',
	   success: function(response){
		   console.log(response);    
		   if(response.status=="success"){
		   console.log("ato");
		   $("#deleteEmployeeModal").modal('hide');
		   var urls="<?php echo base_url('Prof/ProfController/index')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
	   },
	   error: function(){
		   alert("Fako");
	   }
   });
}


function submitForm(formData){
    
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Employeecontrolleur/submitcreate')?>",
	   data: formData,
	   dataType: 'json',
	   contentType: false,
       processData: false,
	   success: function(response){
	
			console.log(response);
		
		   if(response.status=="success"){
		   console.log("ato");
		   $("#addEmployeeModal").modal('hide');
		   var urls="<?php echo base_url('Employeecontrolleur/list')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
		   
	   },
	   error: function(){
		   alert("Error");
	   }
   });
   
}

function submitEdit(formData){
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Employeecontrolleur/submitedit')?>",
	   cache:false,
	   data: formData,
	   dataType: 'json',
	   success: function(response){
		   console.log(response);    
		   if(response.status=="success"){
		   console.log("ato");
		   $("#editEmployeeModal").modal('hide');
		   var urls="<?php echo base_url('Employeecontrolleur/list')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
	   },
	   error: function(){
		   alert("Fako");
	   }
   });
}

function editpictureForm(formData){
    
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Employeecontrolleur/editpicture')?>",
	   data: formData,
	   dataType: 'json',
	   contentType: false,
       processData: false,
	   success: function(response){
		console.log(response);
		   
		
		if(response.status=="success"){
		   console.log("ato");
		   $("#editpicture").modal('hide');
		   var urls="<?php echo base_url('Employeecontrolleur/list')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
		   
	   },
	   error: function(){
		   alert("Error");
	   }
   });
}
</script>
</body>
</html>

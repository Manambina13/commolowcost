<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Liste devoirs</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/jquery.dataTables.min.css">
  
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('navbarheader.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include('menu.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Liste des travaux reçus</h1>
          </div>
         </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
    <div class="col-3">
                    <label for="exampleInputEmail1">Année universitaire</label>
<form action="<?php echo base_url('Prof/ProfController/rechercheparannee')   ?>" method="POST">
<select name="myselect" id="myselect" onchange="this.form.submit()" class="form-control">
  <?php for($i=0;$i<count($annee);$i++) {?>
  <option value="<?php echo $annee[$i]->idanneeuniversitaire   ?>"><?php echo $annee[$i]->annee   ?></option>
  <?php  } ?>
</select>
</form>
    </div>
                 
        <!-- /.card-header -->
              <div class="card-body">
                <table id="userTable" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                  <th>Nom</th>
                  <th>Prénoms</th>
                  <th>Fichier</th>
                  <th>Niveau</th>
                    <th>Parcours</th>
                    <th>UE</th>
                    <th>EC</th>
                    <th>Télécharger</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                  <?php for($i=0;$i<count($result);$i++) {   ?>
                    <td><?php echo $result[$i]->nom_etudiant; ?></td>
                    <td><?php echo $result[$i]->prenom; ?></td>
                    <td><?php echo $result[$i]->nom; ?></td>
                    <td>L<?php echo $result[$i]->idniveau; ?></td>
                    <td><?php echo $result[$i]->nom_parcour; ?></td>
                    <td><?php echo $result[$i]->nom_matiere; ?></td>
                    <td><?php echo $result[$i]->nom_ec; ?></td>
                    <td style="text-align:center;"><a href="<?php echo base_url($result[$i]->lien) ?>"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                  <?php }?>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('navbarfoot.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- Page specific script -->
      <script type="text/javascript" src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js')?>"></script>	
<script>
	   $(document).ready(function() {
             
        $('#userTable').DataTable({
            "bLengthChange": false,
            "language": {
         "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        }
        });
        });

</script>
</body>
</html>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="<?php echo base_url(); ?>assets/dist/img/1.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Commo</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
   
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo base_url('CalendrierControlleur/formateur'); ?>" class="nav-link">
            <i class="fa fa-calendar" aria-hidden="true"></i>
             <p>
                Agenda
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Prof/ProfController/addcours'); ?>" class="nav-link">
            <i class="fa fa-plus-circle" aria-hidden="true"></i>  <p>
                Ajouter cours
              </p>
            </a>
          </li>
         
          <li class="nav-item">
            <a href="<?php echo base_url('Prof/ProfController/index'); ?>" class="nav-link">
            <i class="fa fa-book" aria-hidden="true"></i>  <p>
                Liste cours
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a href="<?php echo base_url('Prof/ProfController/listecouretudiant'); ?>" class="nav-link">
            <i class="fa fa-files-o" aria-hidden="true"></i> <p>
                Travaux par etudiant
              </p>
            </a>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

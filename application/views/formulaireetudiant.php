<!doctype html>
<html lang="fr">
  <head>
  	<title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/css/style.css?>">

	</head>
	<body>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section"></h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-12 col-lg-10">
					<div class="wrap d-md-flex">
						<div class="img" style="background-image: url(<?php echo base_url('assets/login/images/bg-1.jpg'); ?>">
			      </div>
						<div class="login-wrap p-4 p-md-5">
			      	<div class="d-flex">
			      		<div class="w-100">
			      			<h3 class="mb-4">Connexion</h3>
			      		</div>
						 	</div>
               <?php echo isset($error) ? $error : ''; ?> 
              <form action="<?php echo base_url('Login/Formulaire/submitajoutetudiant')  ?>" method="post">
                
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Matricule</label>
                    <input type="text" name="matricule" class="form-control" placeholder="Matricule d'utilisateur">
                  </div>
                  
                <div class="form-group">
                    <label for="exampleInputEmail1">Nom</label>
                    <input type="text"  name="nom" class="form-control" placeholder="Nom">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Prénom(s)</label>
                    <input type="text" name="prenom"  class="form-control" placeholder="Prénoms">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Numero(s)</label>
                    <input type="text" name="numero"  class="form-control" placeholder="Prénoms">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Parcour</label>
                   
                    <select name="parcour" class="custom-select">
                    
                                        <?php for ($i = 0; $i < count($parcour); $i++) { ?>
                                            <option value="<?php echo $parcour[$i]->idparcour; ?>"><?php echo $parcour[$i]->nom_parcour; ?></option>
                                        <?php } ?>
                                        </optgroup>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Niveau</label>
                    <select name="niveau" class="custom-select">
                    
                                        <?php for ($i = 0; $i < count($niveau); $i++) { ?>
                                            <option value="<?php echo $niveau[$i]->idniveau; ?>"><?php echo $niveau[$i]->chiffre; ?></option>
                                        <?php } ?>
                                        </optgroup>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">E-mail</label>
                    <input type="text" name="email" class="form-control" id="exampleInputPassword1" placeholder="faniry@gmail.com">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Mot de passe</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="xxxxxxxxxxxxxxxxxxx">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Confirmer votre votre de passe</label>
                    <input type="password" name="password2" class="form-control" id="exampleInputPassword1" placeholder="xxxxxxxxxxxxxxxxxxx">
                  </div>
                 
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Ajouter</button>
                </div>
              </form>
						
		        </div>
		      </div>
				</div>
			</div>
		</div>
	</section>

	<script src="<?php echo base_url(); ?>assets/login/js/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/login/js/popper.js"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>js/main.js"></script>

	</body>
</html>


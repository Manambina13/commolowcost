<!DOCTYPE html>
<html>

<head>
    <title>Calandar</title>

     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/jquery.dataTables.min.css">


    <link href='<?php echo base_url(); ?>assets/calendar/packages/core/main.css' rel='stylesheet' />
    <link href='<?php echo base_url(); ?>assets/calendar/packages/daygrid/main.css' rel='stylesheet' />
    <link href='<?php echo base_url(); ?>assets/calendar/packages/timegrid/main.css' rel='stylesheet' />
    <link href='<?php echo base_url(); ?>assets/calendar/packages/list/main.css' rel='stylesheet' />
    <link href='<?php echo base_url(); ?>assets/calendar/packages/bootstrap/css/bootstrap.css' rel='stylesheet' />
    <link href="<?php echo base_url(); ?>assets/calendar/packages/jqueryui/custom-theme/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">
    <link href='<?php echo base_url(); ?>assets/calendar/packages/datepicker/datepicker.css' rel='stylesheet' />
    <link href='<?php echo base_url(); ?>assets/calendar/packages/colorpicker/bootstrap-colorpicker.min.css' rel='stylesheet' />
    <script src='<?php echo base_url(); ?>assets/calendar/packages/core/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/daygrid/main.js'></script>
    <!-- -->
    <script src='<?php echo base_url(); ?>assets/calendar/packages/core/locales/fr.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/daygrid/main.js'></script>
    <!-- -->
    <script src='<?php echo base_url(); ?>assets/calendar/packages/timegrid/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/list/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/interaction/main.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/jquery/jquery.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/jqueryui/jqueryui.min.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/bootstrap/js/bootstrap.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/datepicker/datepicker.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/packages/colorpicker/bootstrap-colorpicker.min.js'></script>
    <script src='<?php echo base_url(); ?>assets/calendar/calendar3.js'></script>
    
    <script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular-route.min.js"></script>
    
    <style>
body {
  font-family: "Lato", sans-serif;
}

.sidenav {
  height: 100%;
  width: 160px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  padding-top: 20px;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.main {
  margin-left: 160px; /* Same as the width of the sidenav */
  font-size: 28px; /* Increased text to enable scrolling */
  padding: 0px 10px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
</head>

<body ng-app="assignation" ng-controller="myCtrl" class="hold-transition sidebar-mini">
  <div class="wrapper">
  <!-- Navbar -->
 <?php include('navbarheader.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include('menu.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
         </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-header">
                <h3 class="card-title">Agenda</h3>
                <div class="col-md-12" align="right">
                                       
                                    </div>
                                    
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <div class="container">





<div id="calendar"></div>
</div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('navbarfoot.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<script>
    var app = angular.module('assignation', []);
    app.controller('myCtrl', function($scope, $http, $location, $window) {
        $http({
            method: "GET",
            url: "<?php echo base_url('Api/Api/getparcour')?>"
        }).then(function mySuccess(response) {
            console.log(response);
            $scope.classe = response.data.data;
            //    $scope.response;


        }, function myError(response) {
            //   $scope.myWelcome = response.statusText;
        });
        $scope.getdetails1 = function() {
            $scope.classe2=[];
            $scope.classe1 = [];
            $scope.classe3=[];
            console.log($scope.parcour);
            $http({
                method: "POST",
                url: "<?php echo base_url('Api/Api/getparniveau')?>",
                data: {
                    idparcour: $scope.parcour
                }

            }).then(function(response) {
                console.log(response);
             //   $scope.classe2 = response.data.data;
                //  $scope.show = true;1
                
                $scope.classe2=[];
                $scope.classe3 = response.data.data;
                $scope.classe4 = [];
                //$scope.ec = []
            //    $scope.ue = response.data.Data;
                // console.log(response.data);
            });

        }
        $scope.getdetails3 = function() {
          $scope.classe1=[];
            console.log($scope.ue);
            if($scope.parcour!=null){
            $http({
                method: "POST",
                url: "<?php echo base_url('Api/Api/getec')?>",
                data: {
                    idue: $scope.ue,
                    idparcour: $scope.parcour
                }

            }).then(function(response) {
                console.log(response);
              //  $scope.classe4 = response.data.data;
                //  $scope.show = true;1
                $scope.classe1=response.data.data

               // $scope.ec = []
               // $scope.ue = response.data.Data;
                // console.log(response.data);
            });
            }
        }
        $scope.getdetails2 = function() {
            console.log($scope.userselected4);
            if($scope.parcour!=null){
            $http({
                method: "POST",
                url: "<?php echo base_url('Api/Api/getue')?>",
                data: {
                    idclasse: $scope.niveau,
                    idparcour: $scope.parcour
                }

            }).then(function(response) {
                console.log(response);
              //  $scope.classe4 = response.data.data;
                //  $scope.show = true;1
                $scope.classe2=response.data.data

              //  $scope.ec = []
             //   $scope.ue = response.data.Data;
                // console.log(response.data);
            });
            }
        }
    });
    </script>
</body>
</html>





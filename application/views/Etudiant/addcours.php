<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ajouter Travaux</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">

    <script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular-route.min.js"></script>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('navbarheader.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include('menu.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Travaux</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Travaux envoyés</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php echo isset($error) ? $error : ''; ?>
              <form method="post" ng-app="assignation" ng-controller="myCtrl" action="<?php echo site_url('Etudiant/ETController/do_upload'); ?>" enctype="multipart/form-data"> 
                <div class="card-body">

                <div class="form-group">
                    <label for="exampleInputEmail1">UE</label>
                    <select name="matiere" id="myselect"   ng-change="getdetails1()" ng-model="matiere" class="form-control">
                    <?php for($i=0;$i<count($matiere);$i++) {?>
                    <option value="<?php echo $matiere[$i]->idmatiere   ?>"><?php echo $matiere[$i]->nom_matiere ?></option>
                    <?php  } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">EC</label>
                    <select name="ec" id="classe3" class="custom-select"
                                                        ng-options="listeec.idec as listeec.nom_ec for listeec in listeec"
                                                        ng-model="ec">
                                                        <option value="">Choisir EC</option>
                                                    </select>
                  </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nom travaux</label>
                    <input type="text" name="nom" class="form-control" id="exampleInputEmail1" placeholder="Entre le nom du cours">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Fichier</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="profilePic" id="profilePic">
                        <label class="custom-file-label" for="exampleInputFile">Choisir votre piece jointe</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Télécharger</span>
                      </div>
                    </div>
                  </div>
                   </div>
                <!-- /.card-body -->

                <div class="card-footer">
                 <span title="envoyer"><button type="submit" class="btn btn-success pull-right"><i class="fa fa-paper-plane" aria-hidden="true"></i></button></span> 
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('navbarfoot.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="deleteform" name="delete" enctype="multipart/form-data" role="form">
				<input type="hidden" name="id_d" id="id_d" value="" />
					<div class="modal-header">						
						<h4 class="modal-title">Supprimer formateur</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
												<p class="text-warning"><small>Voulez-vous vraiment supprimer  définitivement ce formateur?</small></p>
</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Non">
						<input type="submit" class="btn btn-danger" value="Oui">
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/modal/modal/core/bootstrap-material-design.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- Page specific script -->
</script>
<script>
    var app = angular.module('assignation', []);
    app.controller('myCtrl', function($scope, $http, $location, $window) {
        $scope.getdetails1 = function() {
           
            console.log($scope.matiere);
            $http({
                method: "POST",
                url: "<?php echo base_url('Api/Api/getecetudiant')?>",
                data: {
                    idmatiere: $scope.matiere
                }

            }).then(function(response) {
                console.log(response);
                $scope.listeec = response.data.data;
                //  $scope.show = true;1
                
           
                //$scope.ec = []
            //    $scope.ue = response.data.Data;
                // console.log(response.data);
            });

        }
     
    });
    </script>
</body>
</html>

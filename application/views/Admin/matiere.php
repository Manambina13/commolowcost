<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Liste matiere</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">
	<script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/angularJS/angular-route.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/jquery.dataTables.min.css">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('navbarheader.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include('menu.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
         </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-header">
                <h3 class="card-title">Liste des matieres</h3>
                <div class="col-md-12" align="right">
                                        <span title="Ajout matière"> <a href="<?php echo base_url('Admin/Matiere/ajoutmatiere'); ?>"><button class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button></a></span>
                                    </div>
                                    
              </div>
							<!--   -->
							<div ng-app="assignation" ng-controller="myCtrl">
							<form action="<?php echo base_url('Admin/Matiere/recherche')   ?>" method="POST" >
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Parcours</label>
			<select name="userselected3" id="userselected3" class="custom-select" required="required" ng-options="classe.idparcour as classe.nom_parcour for classe in classe" ng-change="getdetails1()"  ng-model="userselected3">
            <option value="">Choisir Mention</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Niveau</label>
			<select name="listeniveau" id="listeniveau" class="custom-select" required="required" ng-options="classe6.idniveau as classe6.chiffre for classe6 in classe6"   ng-model="listeniveau">
            <option value="">Choisir Niveau</option>
      </select>
    </div>
  </div>
  <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
</div>
				



						  <!-- -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nom</th>
                    <th>Parcours</th>
                    <th>Niveau</th>
                    <th>Detail</th>
                    <th>Modifier</th>
                    <th>Supprimer</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                  <?php for($i=0;$i<count($result);$i++){?>
                    <td><?php echo $result[$i]->nom_matiere;?></td>
                    <td><?php echo $result[$i]->nom_parcour;?></td>
                    <td><?php echo $result[$i]->chiffre ?></td>
									<!--	<td style="text-align:center;"><a href="<?php echo base_url('Admin/Matiere/listeformateur/'.$result[$i]->idmatiere); ?>"><span title="Modifier"><button class="btn btn-warning btn-sm"><i class="fa fa-users"></i></button></span></a></td> -->
									<td style="text-align:center;"><a href="<?php echo base_url('Admin/Eccontroller/listeec/'.$result[$i]->idmatiere); ?>"><span title="Modifier"><button class="btn btn-warning btn-sm"><i class="fa fa-users"></i></button></span></a></td>
										<td style="text-align:center;"><a href="<?php echo base_url('Admin/Matiere/formupdate/'.$result[$i]->idmatiere); ?>"><span title="Modifier"><button class="btn btn-warning btn-sm"><i class="fa fa-pencil-square-o"></i></button></span></a></td>
                    <td style="text-align:center;"><a href="#deleteEmployeeModal" data-toggle="modal" data-id="<?php echo $result[$i]->idmatiere; ?>" ><span title="Supprimer"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa-lg"></i></button></span></a></td>
                    </tr>
                  <?php } ?>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('navbarfoot.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="deleteform" name="delete" enctype="multipart/form-data" role="form">
				<input type="hidden" name="id_d" id="id_d" value="" />
					<div class="modal-header">						
						<h4 class="modal-title">Supprimer EC</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
												<p class="text-warning"><small>Voulez-vous vraiment supprimer  définitivement cet EC?</small></p>
</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Non">
						<input type="submit" class="btn btn-danger" value="Oui">
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/modal/modal/core/bootstrap-material-design.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
	   $(document).ready(function() {
             
        $('#example1').DataTable({
            "bLengthChange": false,
            "language": {
         "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        }
        });
        });

</script>
<script>
$(document).ready(function(){
	// Activate tooltip
	$('[data-toggle="tooltip"]').tooltip();
	
	// Select/Deselect checkboxes
	var checkbox = $('table tbody input[type="checkbox"]');
	$("#selectAll").click(function(){
		if(this.checked){
			checkbox.each(function(){
				this.checked = true;                        
			});
		} else{
			checkbox.each(function(){
				this.checked = false;                        
			});
		} 
	});
	checkbox.click(function(){
		if(!this.checked){
			$("#selectAll").prop("checked", false);
		}
	});
	$('#editpicture').on('show.bs.modal', function(e) {
		var id=$(e.relatedTarget).data('id');
		console.log(id);
		$(e.currentTarget).find('input[name="id_u"]').val(id);
	});
	$('#editEmployeeModal').on('show.bs.modal', function(e) {
		var id=$(e.relatedTarget).data('id');
		var nom=$(e.relatedTarget).data('nom');
		var prenom=$(e.relatedTarget).data('prenom');
		var email=$(e.relatedTarget).data('email');
		$(e.currentTarget).find('input[name="id_e"]').val(id);
		$(e.currentTarget).find('input[name="prenom_e"]').val(prenom);
		$(e.currentTarget).find('input[name="name_e"]').val(nom);
		$(e.currentTarget).find('input[name="email_e"]').val(email);
	});
	$('#deleteEmployeeModal').on('show.bs.modal', function(e) {
		var id=$(e.relatedTarget).data('id');
		console.log(id);
		$(e.currentTarget).find('input[name="id_d"]').val(id);
	});
	$("#EditPictureform").submit(function(event){
        
		var name= $("#id_u").val();
		var fd = new FormData();
        var files = $('#file_e')[0].files;
		fd.append('file',files[0]);
		fd.append('name',name);
		console.log(fd);
		editpictureForm(fd);
		return false;
	})
	$("#contactForm").submit(function(event){
        
      
		var name= $("#nom").val();
		var prenom=$("#prenom").val();
		var email=$("#mail").val();
		console.log(name);
		console.log(prenom);
		console.log(email);
		var fd = new FormData();
        var files = $('#file')[0].files;
		fd.append('file',files[0]);
		fd.append('name',name);
		fd.append('prenom',prenom);
		fd.append('email',email)
		console.log(fd);
		submitForm(fd);
		return false;
	});
	
	$("#editForm").submit(function(event){
        var formData = {
			id:$("#id_e").val(),
            name: $("#name_e").val(),
			prenom: $("#prenom_e").val(),
			email: $("#email_e").val(),
        };
		console.log("ato bowo");
        submitEdit(formData);
		return false;
	});
	
	$("#deleteform").submit(function(event){
        var formData = {
			id:$("#id_d").val(),
		};
		console.log(formData)
        submitDelete(formData);
		return false;
	});
});

function submitDelete(formData){
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Admin/Matiere/submitdelete')?>",
	   cache:false,
	   data: formData,
	   dataType: 'json',
	   success: function(response){
		   console.log(response);    
		   if(response.status=="success"){
		   console.log("ato");
		   $("#deleteEmployeeModal").modal('hide');
		   var urls="<?php echo base_url('Admin/Matiere/listematiere')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
	   },
	   error: function(){
		   alert("Fako");
	   }
   });
}


function submitForm(formData){
    
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Employeecontrolleur/submitcreate')?>",
	   data: formData,
	   dataType: 'json',
	   contentType: false,
       processData: false,
	   success: function(response){
	
			console.log(response);
		
		   if(response.status=="success"){
		   console.log("ato");
		   $("#addEmployeeModal").modal('hide');
		   var urls="<?php echo base_url('Employeecontrolleur/list')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
		   
	   },
	   error: function(){
		   alert("Error");
	   }
   });
   
}

function submitEdit(formData){
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Employeecontrolleur/submitedit')?>",
	   cache:false,
	   data: formData,
	   dataType: 'json',
	   success: function(response){
		   console.log(response);    
		   if(response.status=="success"){
		   console.log("ato");
		   $("#editEmployeeModal").modal('hide');
		   var urls="<?php echo base_url('Employeecontrolleur/list')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
	   },
	   error: function(){
		   alert("Fako");
	   }
   });
}

function editpictureForm(formData){
    
	$.ajax({
	   type: "POST",
	   url: "<?php echo base_url('Employeecontrolleur/editpicture')?>",
	   data: formData,
	   dataType: 'json',
	   contentType: false,
       processData: false,
	   success: function(response){
		console.log(response);
		   
		
		if(response.status=="success"){
		   console.log("ato");
		   $("#editpicture").modal('hide');
		   var urls="<?php echo base_url('Employeecontrolleur/list')?>";
		   window.location.replace(urls);

		   }
		   else{
			   alert(response.message);
		   }
		   
	   },
	   error: function(){
		   alert("Error");
	   }
   });
}
</script>
</script>
<script>
var app = angular.module('assignation', []);
app.controller('myCtrl', function($scope,$http,$location,$window) {
	$http({
        method : "GET",
        url : "<?php echo base_url('Admin/Parcour/listeparcour')?>"
      }).then(function mySuccess(response) {
      console.log(response);
	  $scope.classe=response.data.data;
    //    $scope.response;
		 
		 
		  }, function myError(response) {
       //   $scope.myWelcome = response.statusText;
		});
		$scope.getdetails1=function(){
			$scope.classe6=[];
      console.log($scope.userselected3);
      $http({
        method : "POST",
        url : "<?php echo base_url('Admin/Matiere/listematiereapi')?>",
        data: {idclasse:$scope.userselected3}
      
    }).then(function(response) {
      console.log(response);
	  $scope.classe6=response.data.data;
      //  $scope.show = true;1
      $scope.classe3=[];
      $scope.classe4=[];
      $scope.ec=[]
        $scope.ue=response.data.Data;
        // console.log(response.data);
    });
	
    }
    $scope.getdetails2=function(){
        console.log("ato");
      console.log($scope.userselected2);
      $http({
        method : "POST",
        url : "<?php echo base_url('index.php/Information/getoption')?>",
        data: {idclasse:$scope.userselected2}
      
    }).then(function(response) {
      console.log(response);
	  
    $scope.classe3=response.data.data;
      //  $scope.show = true;1
      $scope.classe4=[];
      $scope.ec=[]
        $scope.ue=response.data.Data;
        // console.log(response.data);
    });
	
    }



});
  </script>
</body>
</html>

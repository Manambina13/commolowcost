<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="<?php echo base_url(); ?>assets/dist/img/1.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Commo</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
   
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo base_url('CalendrierControlleur/index'); ?>" class="nav-link">
            <i class="fa fa-calendar" aria-hidden="true"></i>
             <p>
                Agenda
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Admin/Etudiant/listeetudiant'); ?>" class="nav-link">
            <i class="fa fa-user-circle" aria-hidden="true"></i>  <p>
                Liste Etudiant
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Admin/Formateur/listeformateur'); ?>" class="nav-link">
            <i class="fa fa-users" aria-hidden="true"></i>  <p>
                Liste Formateur
              </p>
            </a>
          </li>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Admin/Matiere/listematiere'); ?>" class="nav-link">
            <i class="fa fa-book" aria-hidden="true"></i> <p>
                Liste UE et EC
              </p>
            </a>
          </li>
          <!--li class="nav-item">
            <a href="<--?php echo base_url('Admin/Inscription/listeetudiant'); ?>" class="nav-link">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i>  <p>
                Réinscription
              </p>
            </a>
          </li-->
          <li class="nav-item">
            <a href="<?php echo base_url('Admin/Anneecontroller/liste'); ?>" class="nav-link">
            <i class="fa fa-calendar-plus-o" aria-hidden="true"></i>   <p>
                Année Universitaire
              </p>
            </a>
          </li>
          
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

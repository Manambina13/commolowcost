<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Modifier Etudiant</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.css">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('navbarheader.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php include('menu.php');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Etudiant</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Modifier Etudiant</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php echo isset($error) ? $error : ''; ?> 
              <form action="<?php echo base_url('Admin/Etudiant/submitedit')  ?>" method="post">
                    <input type="hidden" name="idformateur" value="<?php echo $result[0]->idetudiant   ?>">
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Matricule</label>
                    <input type="text" name="matricule" value="<?php echo $result[0]->matricule ?>" class="form-control" placeholder="Matricule d'utilisateur">
                  </div>
                  
                <div class="form-group">
                    <label for="exampleInputEmail1">Nom</label>
                    <input type="text" name="nom" value="<?php echo $result[0]->nom ?>" class="form-control" placeholder="Nom">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Prénom(s)</label>
                    <input type="text"  name="prenom" value="<?php echo $result[0]->prenom ?>"  class="form-control" placeholder="Prénoms">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Numero</label>
                    <input type="text" class="form-control"  name="numero" value="<?php echo $result[0]->numero ?>"  id="exampleInputPassword1" placeholder="M1">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">E-mail</label>
                    <input type="text" class="form-control"  name="email" value="<?php echo $result[0]->email ?>"  id="exampleInputPassword1" placeholder="faniry@gmail.com">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Mot de passe</label>
                    <input type="password" class="form-control"  name="password" value="<?php echo $result[0]->mdp ?>"  id="exampleInputPassword1" placeholder="xxxxxxxxxxxxxxxxxxx">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Confirmer votre votre de passe</label>
                    <input type="password" class="form-control"  name="password2" value="<?php echo $result[0]->mdp ?>" id="exampleInputPassword1" placeholder="xxxxxxxxxxxxxxxxxxx">
                  </div>
                 
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Modifier</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('navbarfoot.php')?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
			<form id="deleteform" name="delete" enctype="multipart/form-data" role="form">
				<input type="hidden" name="id_d" id="id_d" value="" />
					<div class="modal-header">						
						<h4 class="modal-title">Supprimer Direction</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
												<p class="text-warning"><small>Voulez-vous vraiment supprimer  définitivement cette direction?</small></p>
</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Non">
						<input type="submit" class="btn btn-danger" value="Oui">
					</div>
				</form>
			</div>
		</div>
	</div>
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- Page specific script -->
</script>
</body>
</html>

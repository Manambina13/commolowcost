<?php
defined('BASEPATH') or exit('No direct script access allowed');
//@include_once('Base_Controller.php');
//class CalendrierControlleur extends Base_Controller
class CalendrierControlleur extends CI_Controller
{
    //jaune L1:#dbff00
    //Seconde L2:#bfbbbb
    //L3:#2600bf
    public function __construct()
    {
        parent::__construct();
        $this->load->model('DAOGen');
       // $this->load->model("CalendarModel");
       // $this->load->model("EmailModel");
        $this->load->database();
    }
    public function index(){

		$this->load->view('Admin/calendar2');
    }
    

    public function formateur(){

		$this->load->view('Prof/calendar2');
    }
    

    public function etudiant(){
        $this->load->view('Etudiant/calendar2');        
    }
    public function load(){
        /*
        $data = [];
        $result = $db->rows("SELECT * FROM events ORDER BY id");
        foreach($result as $row) {
            $data[] = [
                'id'              => $row->id,
                'title'           => $row->title,
                'start'           => $row->start_event,
                'end'             => $row->end_event,
                'backgroundColor' => $row->color,
                'textColor'       => $row->text_color
                ];
            }
         */
        $data=[];
        $result=$this->DAOGen->ReadGen("events","order by id");
        foreach($result as $row) {
            $data[] = [
                'id'              => $row->id,
                'title'           => $row->title,
                'start'           => $row->start_event,
                'end'             => $row->end_event,
                'backgroundColor' => $row->color,
                'textColor'       => $row->text_color
                ];
        }  
            echo json_encode($data);

    }
    public function getevent(){
        
        if (!empty($this->input->post('id'))) {
           // $row = $db->row("SELECT * FROM events where id=?", [$_POST['id']]);
           $row=$this->DAOGen->ReadGen("events","where id=".$this->input->post('id'));
           $data = [
                'id'        => $row[0]->id,
                'title'     => $row[0]->title,
                'start'     => date('d-m-Y H:i:s', strtotime($row[0]->start_event)),
                'end'       => date('d-m-Y H:i:s', strtotime($row[0]->end_event)),
                'color'     => $row[0]->color,
                'textColor' => $row[0]->text_color
            ];
        
            echo json_encode($data);
        }        
    }
    public function insert(){
       
        if (!empty($this->input->post('title'))) {

            //collect data
            $error      = null;
            $title      = $this->input->post('title');
            $start      = $this->input->post('startDate');
            $end        = $this->input->post('endDate');
            $color      = $this->input->post('color');
            $text_color = $this->input->post('text_color');
        
            //validation
            if ($title == '') {
                $error['title'] = 'Title is required';
            }
        
            if ($start == '') {
                $error['start'] = 'Start date is required';
            }
        
            if ($end == '') {
                $error['end'] = 'End date is required';
            }
        
            //if there are no errors, carry on
            if (! isset($error)) {
        
                //format date
                $start = date('Y-m-d H:i:s', strtotime($start));
                $end = date('Y-m-d H:i:s', strtotime($end));
                
                $data['success'] = true;
                $data['message'] = 'Success!';
        
                //store
                /*
                $insert = array(
                    'title'       => $title,
                    'start_event' => $start,
                    'end_event'   => $end,
                    'color'       => $color,
                    'text_color'  => $text_color
                );
                */

                $insert = array(
                    'title'       => $title,
                    'start_event' => $start,
                    'end_event'   => $end,
                    'color'       => "#009933",
                    'text_color'  => "#ffffff",
                );
                //$db->insert('events', $insert);
                $this->DAOGen->create($insert,null,null,"events");
            } else {
        
                $data['success'] = false;
                $data['errors'] = $error;
            }
        
            //echo json_encode($_POST);
        }
        echo json_encode($data);
        
    }
    public function update(){
        
        
            if (!empty($this->input->post('id'))) {

                //collect data
                $error      = null;
                $id         = $this->input->post('id');
                $start      = $this->input->post('start');
                $end        = $this->input->post('end');

                //optional fields
                $title      = !empty($this->input->post('title')) ? $this->input->post('title'): '';
                $color      =  !empty($this->input->post('color')) ? $this->input->post('color'): '';
                $text_color =  !empty($this->input->post('text_color')) ? $this->input->post('text_color'): '';

                //validation
                if ($start == '') {
                    $error['start'] = 'Start date is required';
                }

                if ($end == '') {
                    $error['end'] = 'End date is required';
                }

                //if there are no errors, carry on
                if (! isset($error)) {

                    //reformat date
                    $start = date('Y-m-d H:i:s', strtotime($start));
                    $end = date('Y-m-d H:i:s', strtotime($end));
                    
                    $data['success'] = true;
                    $data['message'] = 'Success!';

                    //set core update array
                    $update = [
                        'start_event' => date('Y-m-d H:i:s', strtotime($this->input->post('start'))),
                        'end_event' => date('Y-m-d H:i:s', strtotime($this->input->post('end')))
                    ];

                    //check for additional fields, and add to $update array if they exist
                    if ($title !='') {
                        $update['title'] = $title;
                    }

                    if ($color !='') {
                        $update['color'] = $color;
                    }

                    if ($text_color !='') {
                        $update['text_color'] = $text_color;
                    }

                    //set the where condition ie where id = 2
                    $where = ['id' => $this->input->post('id')];

                    //update database
                  //  $db->update('events', $update, $where);
                  $this->DAOGen->update($update,$where,'events');
                
                } else {

                    $data['success'] = false;
                    $data['errors'] = $error;
                }

                echo json_encode($data);
            }   
            }
    public function delete(){


        if(!empty($this->input->post('id'))) {
           // $db->deleteById('events',$_POST['id']);
           $delete=array(
            "id"=>$this->input->post('id')
           );
           $this->DAOGen->delete($delete,"events");
        }
        
    }
    ////Formateur////
    /*
        public function loademploi(){
        
        
        
            $final=[];
            $data=[];
            $result1=$this->DAOGen->ReadGen("events","order by id");
            foreach($result1 as $row) {
                $data[] = [
                    'id'              => $row->id,
                    'title'           => $row->title,
                    'start'           => $row->start_event,
                    'end'             => $row->end_event,
                    'backgroundColor' => $row->color,
                    'textColor'       => $row->text_color
                ];
            }
            $data2=[];
           // "select edt.*,matiere.nom_matiere,ec.nom_ec from ec join matiere on ec.idmatiere=matiere.idmatiere join edt on matiere.idmatiere=edt.idmatiere and ec.idec=edt.idec"
           // $result2=$this->DAOGen->ReadGen("edt",null);
           $result2=$this->DAOGen->RequeteFull("select edt.*,niveau.chiffre,matiere.nom_matiere,ec.nom_ec from ec join matiere on ec.idmatiere=matiere.idmatiere join edt on matiere.idmatiere=edt.idmatiere and ec.idec=edt.idec join niveau on edt.idniveau=niveau.idniveau"); 
           foreach($result2 as $row) {
                $data[] = [
                    'id'              => "E".$row->idedt,
                  //  'title'           =>"UE:".$row->nom_matiere." EC:".$row->nom_ec."(".$row->chiffre.")",
                  'title'           =>"EC:".$row->nom_ec."(".$row->chiffre.")",
                    'start'           => $row->debut,
                    'end'             => $row->fin,
                    'backgroundColor' => "#6453e9",
                    'textColor'       => "#ffffff"
                    ];
            }  
                //            echo json_encode($data);
          // array_push($data,$data2);    
        
     //   array_push($final,$data);
       // array_push($final,$data2);
        // echo json_encode($final);
        
        echo json_encode($data);
        }
        */
        public function getemploidutemps(){
            if (($this->input->post('id') != "")) {
      //    $row=  $this->CalendarModel->getAllById($this->input->post('id'));
             //   echo $this->input->post('id');
         //   $id=str_split($this->input->post('id'));
         //   echo $id[1];
         $pizza  = $this->input->post('id');
         $pieces = explode("A", $pizza);
       //  echo $pieces[1]
      
           $row=  $this->DAOGen->ReadGen("edt","where idedt=".$pieces[1]);
        //        echo "where idemploidutemps=".$pieces[1];
      $data = [
                    'id'        => $row[0]->idedt,
                    'start'     => date('d-m-Y H:i:s', strtotime($row[0]->debut)),
                    'end'       => date('d-m-Y H:i:s', strtotime($row[0]->fin)),
              
        //            'contact' => $row[0]->CONTACT
                ];
    
               echo json_encode($data);
            }
    
        }
        public function loademploi(){

            $data=[];
            $idformateur=$this->session->userdata('idutilisateur');
            $result1=$this->DAOGen->ReadGen("events","order by id");
            $emploitems=$this->DAOGen->ReadGen("edt",null);
            for($i=0;$i<count($emploitems);$i++) {
            $idprof2=$idformateur;
            if($emploitems[$i]->idformateur==$idprof2){
                $data2 = array(
                    'id'              => "A".$emploitems[$i]->idedt,
                    'title'           => "EC".$emploitems[$i]->idec,
                    'start'           => $emploitems[$i]->debut,
                    'end'             => $emploitems[$i]->fin,
                    'backgroundColor' => "#ff0000",
                    'textColor'       => "#ffffff",
                );
                array_push($data,$data2);
            }else{


            $data2 = array(
                'id'              => "M".$emploitems[$i]->idedt,
                'title'           => "EC".$emploitems[$i]->idec,
                'start'           => $emploitems[$i]->debut,
                'end'             => $emploitems[$i]->fin,
                'backgroundColor' => "#6453e9",
                'textColor'       => "#ffffff"
            );
            array_push($data,$data2);
            }
        }
        $result1=$this->DAOGen->ReadGen("events","order by id");
        foreach($result1 as $row) {
            $data[] = [
                'id'              => $row->id,
                'title'           => $row->title,
                'start'           => $row->start_event,
                'end'             => $row->end_event,
                'backgroundColor' => $row->color,
                'textColor'       => $row->text_color
            ];
        }
        echo json_encode($data);

    }
    public function deleteemploi(){

       // echo $this->input->post('id');
     //  echo json_encode($this->input->post()); 
           
        if(!empty($this->input->post('id'))) {
            // $db->deleteById('events',$_POST['id']);
            
            $pizza  = $this->input->post('id');
            $pieces = explode("A", $pizza);
            
            $delete=array(
             "idedt"=>$pieces[1]
            );
            $this->DAOGen->delete($delete,"edt");
         
        }
        
    







    }
    public function ajoutemploi(){
        
        
        
        
        try{
        if(empty($this->input->post('ec'))){
            throw new \Exception("Probleme ec");
        }
        if(empty($this->input->post('ue'))){
            throw new \Exception("Probleme ue");
        }
        if(empty($this->input->post('niveau'))){
            throw new \Exception("Probleme niveau");
        }
        if(empty($this->input->post('parcour'))){
            throw new \Exception("Probleme parcour");
        }
        if(empty($this->input->post('startDate'))){
            throw new \Exception("Probleme date");
        }
        if(empty($this->input->post('endDate'))){
            throw new \Exception("Probleme date fin");
        }
        $idec=$this->DAOGen->changement($this->input->post('ec'));
        $idue=$this->DAOGen->changement($this->input->post('ue'));
        $idniveau=$this->DAOGen->changement($this->input->post('niveau'));
        $idparcour=$this->DAOGen->changement($this->input->post('parcour'));
        $idutilisateur= $this->session->userdata('idutilisateur');
        $data = array( 
            'idniveau'=>$idniveau,
            'idparcour'=>$idparcour,
            'idformateur'=>$idutilisateur,
            'idmatiere'=>$idue,
            'idec'=>$idec,
            'debut'=> date('Y-m-d H:i:s', strtotime($this->input->post('startDate'))),
            'fin'=>date('Y-m-d H:i:s', strtotime($this->input->post('endDate'))),
         ); 
         $this->DAOGen->create($data,null,null,"edt");
         $data['success'] = true;
         $data['message'] = 'Success!';
         echo json_encode($data);
        }catch(\Exception $e){

            $data['success'] = false;
            $data['errors'] = $e->getMessage();
            echo json_encode($data);

        }
    }
        public function updatemploi(){
        //   echo json_encode($this->input->post());
           
           
            try{  
            
            if(empty($this->input->post('editec'))){
            throw new \Exception("Probleme ec");
        }
        if(empty($this->input->post('editue'))){
            throw new \Exception("Probleme ue");
            
        }
        if(empty($this->input->post('editniveau'))){
            throw new \Exception("Probleme niveau");
        }
        if(empty($this->input->post('editparcour'))){
            throw new \Exception("Probleme parcour");
        }
        if(empty($this->input->post('editdebut'))){
            throw new \Exception("Probleme date");
        }
        if(empty($this->input->post('editfin'))){
            throw new \Exception("Probleme date fin");
        }
        
        if(empty($this->input->post('id'))){
            throw new \Exception("Id probleme");
        }
        $idedt=$this->input->post('id');
        $idutilisateur= $this->session->userdata('idutilisateur');
       //$idutilisateur="2"; 
       $row=$this->DAOGen->ReadGen("edt","where idedt=".$idedt);
        if($row[0]->idformateur!=$idutilisateur){
            throw new \Exception("Formateur probleme");
        }
        $idec=$this->DAOGen->changement($this->input->post('editec'));
        $idue=$this->DAOGen->changement($this->input->post('editue'));
        $idniveau=$this->DAOGen->changement($this->input->post('editniveau'));
        $idparcour=$this->DAOGen->changement($this->input->post('editparcour'));
       
        $data = array( 
            'idniveau'=>$idniveau,
            'idparcour'=>$idparcour,
            'idmatiere'=>$idue,
            'idec'=>$idec,
            'debut'=> date('Y-m-d H:i:s', strtotime($this->input->post('editdebut'))),
            'fin'=>date('Y-m-d H:i:s', strtotime($this->input->post('editfin'))),
         ); 
         $update=array(
            "idedt"=>$idedt
         );
         $this->DAOGen->update($data,$update,"edt");
         $data['success'] = true;
         $data['message'] = 'Success!';
         echo json_encode($data);
        }catch(\Exception $e){

            $data['success'] = false;
            $data['errors'] = $e->getMessage();
            echo json_encode($data);

        }        
    
        }
        public function loadetudiant(){
            $final=[];
            $data=[];
            $result1=$this->DAOGen->ReadGen("events","order by id");
            foreach($result1 as $row) {
                $data[] = [
                    'id'              => $row->id,
                    'title'           => $row->title,
                    'start'           => $row->start_event,
                    'end'             => $row->end_event,
                    'backgroundColor' => $row->color,
                    'textColor'       => $row->text_color
                ];
            }
            $data2=[];
           // "select edt.*,matiere.nom_matiere,ec.nom_ec from ec join matiere on ec.idmatiere=matiere.idmatiere join edt on matiere.idmatiere=edt.idmatiere and ec.idec=edt.idec"
           // $result2=$this->DAOGen->ReadGen("edt",null);
           $result2=$this->DAOGen->RequeteFull("select edt.*,niveau.chiffre,matiere.nom_matiere,ec.nom_ec from ec join matiere on ec.idmatiere=matiere.idmatiere join edt on matiere.idmatiere=edt.idmatiere and ec.idec=edt.idec join niveau on edt.idniveau=niveau.idniveau"); 
           foreach($result2 as $row) {
                $data[] = [
                    'id'              => "E".$row->idedt,
                  //  'title'           =>"UE:".$row->nom_matiere." EC:".$row->nom_ec."(".$row->chiffre.")",
                  'title'           =>"EC:".$row->nom_ec."(".$row->chiffre.")",
                    'start'           => $row->debut,
                    'end'             => $row->fin,
                    'backgroundColor' => "#6453e9",
                    'textColor'       => "#ffffff"
                    ];
            }  
                //            echo json_encode($data);
          // array_push($data,$data2);    
        
     //   array_push($final,$data);
       // array_push($final,$data2);
        // echo json_encode($final);
        
        echo json_encode($data);





        }
    
}

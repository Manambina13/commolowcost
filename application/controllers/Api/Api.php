<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
      }
    public function listeparcour(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($_POST);  
    
        $data['result']=$this->DAOGen->ReadGen("parcour","where etat=1");
       // $data['result']=$this->DAOGen->RequeteFull("select distinct(nomparcour) as nomparcour,id_niveau,id_mention,id_parcour from parcour where id_mention=".$idmarque); 
       $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "ok";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);
    }
    //api///
    /////adddd cour formateur
    public function getparcour(){
        $idutilisateur=$this->session->userdata('idutilisateur');
        //"select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere from matiere join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec where assignationec.idformateur=1 and assignationec.etat=1";
       // "select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere,parcour.nom_parcour,parcour.idparcour from parcour join matiere on parcour.idparcour=matiere.idparcour join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec  where assignationec.etat=1"
        //$data['result']=$this->DAOGen->ReadGen("region","order by nom_region asc");
        $data['result']=$this->DAOGen->RequeteFull("select distinct idformateur,nom_parcour,idparcour from listeparcoursvue  WHERE idformateur='".$idutilisateur."'");
        $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "ok";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);    

    }
    public function getparniveau(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $idutilisateur=$this->session->userdata('idutilisateur');
        //"select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere from matiere join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec where assignationec.idformateur=1 and assignationec.etat=1";
       // "select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere,parcour.nom_parcour,parcour.idparcour from parcour join matiere on parcour.idparcour=matiere.idparcour join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec  where assignationec.etat=1"
        //$data['result']=$this->DAOGen->ReadGen("region","order by nom_region asc");
        $idparcour=$this->input->post('idparcour');
        $data['result']=$this->DAOGen->RequeteFull("select distinct idformateur,idniveau,chiffre,idparcour from listeparcoursvue  WHERE idparcour='".$idparcour."' and idformateur='".$idutilisateur."'");
        
        
        //select distinct idformateur,idniveau,chiffre,idparcour from listeparcoursvue  WHERE idparcour='1' and idformateur='2';
        $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "OK";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);    
    }
    public function getue(){

        $_POST = json_decode(file_get_contents('php://input'), true);
        $idutilisateur=$this->session->userdata('idutilisateur');
        //"select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere from matiere join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec where assignationec.idformateur=1 and assignationec.etat=1";
       // "select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere,parcour.nom_parcour,parcour.idparcour from parcour join matiere on parcour.idparcour=matiere.idparcour join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec  where assignationec.etat=1"
        //$data['result']=$this->DAOGen->ReadGen("region","order by nom_region asc");
        $idparcour=$this->input->post('idparcour');
        $idclasse=$this->input->post('idclasse');
        $data['result']=$this->DAOGen->RequeteFull("select distinct idformateur,idmatiere,nom_matiere,idparcour,idniveau from listeparcoursvue  WHERE idparcour='".$idparcour."'  and idniveau='".$idclasse."' and idformateur='".$idutilisateur."'");
        
        
        //select distinct idformateur,idniveau,chiffre,idparcour from listeparcoursvue  WHERE idparcour='1' and idformateur='2';
        $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "OK";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);    
    }
    public function getec(){

        $_POST = json_decode(file_get_contents('php://input'), true);
        $idutilisateur=$this->session->userdata('idutilisateur');
        //"select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere from matiere join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec where assignationec.idformateur=1 and assignationec.etat=1";
       // "select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere,parcour.nom_parcour,parcour.idparcour from parcour join matiere on parcour.idparcour=matiere.idparcour join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec  where assignationec.etat=1"
        //$data['result']=$this->DAOGen->ReadGen("region","order by nom_region asc");
        $idue=$this->input->post('idue');
        $data['result']=$this->DAOGen->RequeteFull("select distinct idformateur,idec,nom_ec,idmatiere from listeparcoursvue  WHERE  idmatiere='".$idue."' and idformateur='".$idutilisateur."'");
        
        
        //select distinct idformateur,idniveau,chiffre,idparcour from listeparcoursvue  WHERE idparcour='1' and idformateur='2';
        $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "OK";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);    
    }

    //////addd cours etudiant//////
    public function getecetudiant(){
        $_POST = json_decode(file_get_contents('php://input'), true);
    // $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
      //  $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
      //$id=$this->session->userdata('idutilisateur');
      //$getmaxetudiant=$this->DAOGen->RequeteFull("select idniveau,idetudiant,idparcour from inscription where idetudiant='".$id."' order by idniveau desc");  
      $idec=$this->input->post('idmatiere');
      $getec=$this->DAOGen->ReadGen("ec","where etat=1 and idmatiere=".$idec);
      $employee_object = new stdClass;
      $employee_object->status = "success";
      $employee_object->message = "OK";
      $employee_object->data = $getec;
      echo json_encode($employee_object); 
    }

    

}

?>
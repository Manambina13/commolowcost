<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\Jsend;
class MessageController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        $this->load->library("pagination");
        $this->load->library("session");
        $this->load->model("MetierMessage");
        $this->load->model('ExtensionsModel');
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function SearchUser(){
        
        $str=$this->input->post('city');
        if($str==null){
            $result2=null;
            $data['iddiscussionpossible']=null;
            $data['result']=$result2;
            $this->load->view('Prof/Message',$data);


        }
        else{
        $valeur=explode(" ",$str);
        $idprof=$this->DAOGen->ReadGen("utilisateur","where etat=1 and nom='".trim($valeur[0])."' and prenom='".trim($valeur[1])."'");
        redirect("MessageController/getView/".$idprof[0]->idutilisateur);
        }



    }
 //   
    public function SearchUserEtudiant(){
        
    $str=$this->input->post('city');
    if($str==null){
        $result2=null;
        $data['iddiscussionpossible']=null;
        $data['result']=$result2;
        $this->load->view('Prof/Message',$data);


    }
    else{
    $valeur=explode(" ",$str);
    $idprof=$this->DAOGen->ReadGen("utilisateur","where etat=1 and nom='".trim($valeur[0])."' and prenom='".trim($valeur[1])."'");
    redirect("MessageController/getViewEtudiant/".$idprof[0]->idutilisateur);
    }
}
    public function getViewEtudiant($iddiscussionpossible){
    $utilisateur=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$iddiscussionpossible);
    $nom=$utilisateur[0]->nom." ".$utilisateur[0]->prenom;
    $u_rec_id=$this->session->userdata('idutilisateur');
    $this->MetierMessage->updateVu($u_rec_id,$iddiscussionpossible);
    $data['iddiscussionpossible']=$iddiscussionpossible;
    $data['nom']=$nom;
    $this->load->view('Etudiant/Message',$data);
    }
    public function Discussion(){
       //     $data['idforum']=$idForum;
           /*
            $u_rec_id = $this->session->userdata('idutilisateur');
            $result=$this->DAOGen->RequeteFull("select * from discussionmessage where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id);
            */
            // "select distinct(idutilisateur) from discussionmessage where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id."";
            
            $result2=null;
            $data['iddiscussionpossible']=null;
            /*
            if(count($result)>0){
                if($result[0]->idutilisateur1!=$u_rec_id){
                $result2=$data['result']=$this->DAOGen->ReadGen("discussionmessage","where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$result[0]->idutilisateur2.") or  (idutilisateur1=".$result[0]->idutilisateur2." or idutilisateur2=".$u_rec_id.")");
                }else{
                    $result2=$data['result']=$this->DAOGen->ReadGen("discussionmessage","where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$result[0]->idutilisateur1.") or  (idutilisateur1=".$result[0]->idutilisateur1." or idutilisateur2=".$u_rec_id.")");    
                }
            }
           
           */
            // echo var_dump($result2);
            $data['nom']="";
            $data['result']=$result2;
            $this->load->view('Prof/Message',$data);
    }
    public function MessageControllerEtudiant(){
        //     $data['idforum']=$idForum;
            /*
             $u_rec_id = $this->session->userdata('idutilisateur');
             $result=$this->DAOGen->RequeteFull("select * from discussionmessage where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id);
             */
             // "select distinct(idutilisateur) from discussionmessage where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id."";
             
             $result2=null;
             $data['iddiscussionpossible']=null;
             /*
             if(count($result)>0){
                 if($result[0]->idutilisateur1!=$u_rec_id){
                 $result2=$data['result']=$this->DAOGen->ReadGen("discussionmessage","where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$result[0]->idutilisateur2.") or  (idutilisateur1=".$result[0]->idutilisateur2." or idutilisateur2=".$u_rec_id.")");
                 }else{
                     $result2=$data['result']=$this->DAOGen->ReadGen("discussionmessage","where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$result[0]->idutilisateur1.") or  (idutilisateur1=".$result[0]->idutilisateur1." or idutilisateur2=".$u_rec_id.")");    
                 }
             }
            
            */
             // echo var_dump($result2);
             $data['nom']="";
             $data['result']=$result2;
             $this->load->view('Etudiant/Message',$data);
     }
    public function getMessage(){
         /*
         $u_rec_id = $this->session->userdata('idutilisateur');
         $result=$this->DAOGen->RequeteFull("select distinct(idutilisateur),where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id);
         $test =new Jsend ("success","",$result);
         echo json_encode($test);
         */
        $u_rec_id = $this->session->userdata('idutilisateur');
        $getiduscissonpossible=$this->DAOGen->ReadGen("discussionpossible","where idutilisateur1=".$u_rec_id." or idutilisateur2=".$u_rec_id);
        if(count($getiduscissonpossible)>0){
        $tableaudiscussionpossible=array();
        $getusers=array();
        for($i=0;$i<count($getiduscissonpossible);$i++){
            if($getiduscissonpossible[$i]->idutilisateur1==$u_rec_id){
                array_push($getusers,$getiduscissonpossible[$i]->idutilisateur2);
                
            }else{
                array_push($getusers,$getiduscissonpossible[$i]->idutilisateur1);
            }
            array_push($tableaudiscussionpossible,$getiduscissonpossible[$i]->iddiscussionpossible);
        }
        $requete=$this->DAOGen->FonctiongetCreateOR($getusers,"idutilisateur");
     //   echo var_dump($requete);
        $requeteUsers=$this->DAOGen->FonctiongetCreateOR($tableaudiscussionpossible,"iddiscussionpossible");
        $listeutilisateur=$this->DAOGen->ReadGen("utilisateur","where ".$requete);
//        $x=array_filter($listeutilisateur,function($a){return $a->idutilisateur=='2';});

        $getid=$this->DAOGen->RequeteFull("select distinct (iddiscussionpossible),date,max(idmessage) as idmessage from message where (".$this->DAOGen->FonctiongetCreateOR($tableaudiscussionpossible,"iddiscussionpossible").") and date=(select max(date))  group by iddiscussionpossible");
      //  echo "select distinct (iddiscussionpossible),date,max(idmessage) as idmessage from message where (".$this->DAOGen->FonctiongetCreateOR($tableaudiscussionpossible,"iddiscussionpossible").") and date=(select max(date))  group by iddiscussionpossible";
      //  echo var_dump($getid);
        $idmessage=array();
        for($i=0;$i<count($getid);$i++){
            array_push($idmessage,$getid[$i]->idmessage);
        }
        if(count($getid)>0){
        $listeMessage=$this->DAOGen->ReadGen("discussionmessage","where (".$this->DAOGen->FonctiongetCreateOR($idmessage,"idmessage").")and idutilisateur=".$u_rec_id." order by date desc");
        for($i=0;$i<count($listeMessage);$i++){
            //
         
        
            
            
            
            //
            if($listeMessage[$i]->idutilisateur1!=$u_rec_id){
                $id=$listeMessage[$i]->idutilisateur1;
                $x=array_filter($listeutilisateur,function($a) use ($id){return $a->idutilisateur==$id;});
                foreach($x as $util){
               // $listeMessage[$i]->idutilisateur=$util->nom." ".$util->prenom;
                $listeMessage[$i]->idutilisateur=$util->nom;    
                }
                $listeMessage[$i]->idutilisateur2=$listeMessage[$i]->idutilisateur1;
                $listeMessage[$i]->idutilisateur1= $u_rec_id;
                if($listeMessage[$i]->envoyeur==$u_rec_id){
                    $listeMessage[$i]->texte="Vous:".$listeMessage[$i]->texte;

                }
            }
            else{
                $id=$listeMessage[$i]->idutilisateur2;
                $x=array_filter($listeutilisateur,function($a) use ($id){return $a->idutilisateur==$id;});
                foreach($x as $util){
                   // $listeMessage[$i]->idutilisateur=$util->nom." ".$util->prenom;
                    $listeMessage[$i]->idutilisateur=$util->nom;
                    }
                    if($listeMessage[$i]->envoyeur==$u_rec_id){
                        $listeMessage[$i]->texte="Vous:".$listeMessage[$i]->texte;
    
                    }

            }
            if($listeMessage[$i]->etat==0){
                $listeMessage[$i]->etat="fa fa-circle";
              //  $listeMessage[$i]->texte="<strong>".$listeMessage[$i]->texte."</strong>";
            }
            else{
                $listeMessage[$i]->etat="";
            }
        }
    
        //      echo "where ".$this->DAOGen->FonctiongetCreateOR($idmessage,"idmessage")." )and idutilisateur=".$u_rec_id;
        $test =new Jsend ("success","OK",$listeMessage);        
        echo json_encode($test);
        }else{
            $test =new Jsend ("success","OK",null);        
            echo json_encode($test);


        }
    }else{
        $test =new Jsend ("success","OK",null);        
        echo json_encode($test);


    }

}
    public function getView($iddiscussionpossible){
        $utilisateur=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$iddiscussionpossible);
        $nom=$utilisateur[0]->nom." ".$utilisateur[0]->prenom;
        $u_rec_id=$this->session->userdata('idutilisateur');
        $this->MetierMessage->updateVu($u_rec_id,$iddiscussionpossible);
        $data['iddiscussionpossible']=$iddiscussionpossible;
        $data['nom']=$nom;
        $this->load->view('Prof/Message',$data);
    }
    public function getMessageWithPerson(){
      //  $u_rec_id = $this->session->userdata('idutilisateur');
     //   $data['result']=$this->DAOGen->ReadGen("discussionmessage","where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$idutilisateur.") or  (idutilisateur1=".$idutilisateur." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id);
     //   echo var_dump("where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$idutilisateur.") or  (idutilisateur1=".$idutilisateur." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id);
     //   echo var_dump($data['result']);
        //   $this->load->view('Prof/Message',$data);
     //   $data['result']=$this->DAOGen->ReadGen("discussionmessage","where iddiscussionpossible=".$idutilisateur." and idutilisateur=".$u_rec_id);
     //   echo json_encode($data['result']);
     $u_rec_id = $this->session->userdata('idutilisateur');
     $_POST = json_decode(file_get_contents('php://input'), true);
    if($this->input->post('iddiscussion')==null){
        $test =new Jsend ("success","OK",null);   
        echo json_encode($test);
    }
    else{
     $iddiscussionpossible=$this->MetierMessage->AvoirDiscussionid($u_rec_id,$this->input->post('iddiscussion'));
    
       $data['result']=$this->DAOGen->ReadGen("discussionmessage","where iddiscussionpossible=".$iddiscussionpossible." and idutilisateur=".$u_rec_id);
  //$data['result']=$this->DAOGen->ReadGen("discussionmessage","where (idutilisateur1=".$u_rec_id." or idutilisateur2=".$this->input->post('iddiscussion').") or  (idutilisateur1=".$this->input->post('iddiscussion')." or idutilisateur2=".$u_rec_id.") and idutilisateur=".$u_rec_id);
  for($i=0;$i<count($data['result']);$i++){
            
            if($data['result'][$i]->envoyeur==$u_rec_id){
                $data['result'][$i]->etat="msg-right";
            }else{
                $data['result'][$i]->etat="msg-left";
            }
            //
            if($data['result'][$i]->idfichier!=null){
                $fich=$this->DAOGen->ReadGen("fichier","where idfichier='".$data['result'][$i]->idfichier."'");
                $data['result'][$i]->idmessage=$fich[0]->idtypemessage;
                $data['result'][$i]->texte=$fich[0]->lien;
                $data['result'][$i]->idfihier=$fich[0]->lien;
            }
            /*
               if($data['result'][$i]->idtypemessage=="2"){
                $fich=$this->DAOGen->ReadGen("fichier","where idfichier='".$data['result'][$i]->idfichier."'");
                
              //  $listeMessage[$i]->Texte=$listeMessage[$i]->texte."<img src='".$fich[0]->lien."' alt='Girl in a jacket' width='300' height='300'>";
                  $data['result'][$i]->idmessage =$fich[0]->lien;
                  $data['result'][$i]->idfichier=$fich[0]->nom;
                  $split=str_split($listeMessage[$i]->nom);
      
    
            }
            else if($data['result'][$i]->idtypemessage=="3"){
                $fich=$this->DAOGen->ReadGen("fichier","where idfichier='".$data['result'][$i]->idfichier."'");
              //  $listeMessage[$i]->Texte=$listeMessage[$i]->texte."<a href='".$fich[0]->lien."'>".$fich[0]->nom."</a>";
              $data['result'][$i]->idmessage =$fich[0]->lien;
              $data['result'][$i]->idfichier=$fich[0]->nom;
              
            }
            else if($data['result'][$i]->idtypemessage=="4"){
                $fich=$this->DAOGen->ReadGen("fichier","where idfichier='".$data['result'][$i]."'");
              //  $listeMessage[$i]->Texte=$listeMessage[$i]->texte."<a href='".$fich[0]->lien."'>".$fich[0]->nom."</a>";
              $data['result'][$i]->idmessage =$fich[0]->lien;
              $data['result'][$i]->idfichier=$fich[0]->nom;
          
            }
            else if($data['result'][$i]->idtypemessage=="1"){
            
          
            }
            */



            //


     }
     $test =new Jsend ("success","OK",$data['result']);   
     echo json_encode($test);
    }
    }
    public function EcrireMessage(){
   //     echo json_encode($this->input->post('idforum')); 
    
        $utilisateur2=$this->input->post('idforum');
        $u_rec_id = $this->session->userdata('idutilisateur');
        $controleprof=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$utilisateur2." or idutilisateur=".$u_rec_id);
     //   echo json_encode($this->input->post()); 
        if($controleprof[0]->idprofile==2 && $controleprof[1]->idprofile==2){
            $test =new Jsend ("success","ok",null);   
            echo json_encode($test);     

        }
        else{
        if(isset($_FILES["file"]["name"]))  
        {
            echo $_FILES["file"]["name"];
            $typeMessage=$this->ExtensionsModel->getExtension($_FILES["file"]["name"]);
            $path=$this->ExtensionsModel->getPath($_FILES["file"]["name"]);
            $config['upload_path']=$path;
            $config['allowed_types']='gif|jpg|png|pdf|docx|zip';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload',$config);
      //    $this->upload->initialize($config);
     if(!$this->upload->do_upload('file'))  
     {  
     echo $this->upload->display_errors();  
     }  
     else  
     {
     
     $data = array('upload_data' => $this->upload->data());
     $title= $this->input->post('title');
     $image= $data['upload_data']['file_name']; 
     
     //$result= $this->upload_model->save_upload($title,$image);
    
   //  $this->DAOUsers->create($url,null,null,"lien");
  //   echo '<img src="'.base_url().'assets/images/'.$image.'" width="300" height="225" class="img-thumbnail" />';  
  if($this->input->post('texte')!=null){
    $u_rec_id = $this->session->userdata('idutilisateur');
    $idtypefichier="1";
    $text=$this->input->post('texte');
    $utilisateur2=$this->input->post('idforum');
    $idiscussionforum=$this->DAOGen->getSequence("message","message");
    $getiduscissonpossible=$this->MetierMessage->CheckDiscussionId($utilisateur2,$u_rec_id);
    $datetime = new \DateTime("now"); 
    $idfichier=$this->DAOGen->getSequence("idfichier","fichier");
    if($text=="undefined"){
        $text="";
    }
    
    
    $discussionforum=array(
        "idmessage"=>$idiscussionforum->sequence,
        "idtypemessage"=>$typeMessage[0]->idtypemessage, 
        "envoyeur"=>$u_rec_id,
        "etat"=>1,
        "texte"=>$text,
        "idtypemessage"=>$typeMessage[0]->idtypemessage, 
        "recepteur"=>$utilisateur2,
        "iddiscussionpossible"=>$getiduscissonpossible,
        "date"=>$datetime->format('Y-m-d H:i:s'),  
        "idfichier"=>$idfichier->sequence
    );
    $datafichier=array(
        "idfichier"=>$idfichier->sequence,
        "idtypemessage"=>$typeMessage[0]->idtypemessage,
        "nom"=>$_FILES["file"]["name"],
        "lien"=>$this->ExtensionsModel->reglagepath($path,$image)
    );
    $this->DAOGen->create($datafichier,null,null,"fichier");
    $this->DAOGen->create($discussionforum,null,null,"message");
    
   
    $consulationforum2=array(
        "idutilisateur"=>$u_rec_id,
         "idmessage"=>$idiscussionforum->sequence,
         "etat"=>1
    );
    $this->DAOGen->create($consulationforum2,null,null,"consultationmessage");
    
    

        $consulationforum=array(
            "idutilisateur"=>$utilisateur2,
             "idmessage"=>$idiscussionforum->sequence,
             "etat"=>0
        );
        $this->DAOGen->create($consulationforum,null,null,"consultationmessage");
    
    
    $test =new Jsend ("success","ok",null);   
        echo json_encode($test);     



    }else{
        $test =new Jsend ("failed","Veuillez taper Message",null);   
        echo json_encode($test);       
        ///erreur


    }
    


    }  
    }else{
        if($this->input->post('texte')!=null){
       // $u_rec_id = $this->session->userdata('idutilisateur');
        $idtypefichier="1";
        $text=$this->input->post('texte');
        $utilisateur2=$this->input->post('idforum');
        $idiscussionforum=$this->DAOGen->getSequence("message","message");
        $getiduscissonpossible=$this->MetierMessage->CheckDiscussionId($utilisateur2,$u_rec_id);
        $datetime = new \DateTime("now"); 
        $discussionforum=array(
            "iddiscussionpossible"=>$getiduscissonpossible,
            "envoyeur"=>$u_rec_id,
            "idtypemessage"=>1,
            "recepteur"=>$utilisateur2,
            "texte"=>$text,
            "date"=>$datetime->format('Y-m-d H:i:s'),
            "etat"=>1   
        );
        $this->DAOGen->create($discussionforum,null,null,"message");
     //   $membreForum=$this->DAOGen->ReadGen("participant","where idforum='".$utilisateur2."' and idutilisateur!='".$u_rec_id."'");
  /*      
     +-----------------------+---------+------+-----+---------+----------------+
| Field                 | Type    | Null | Key | Default | Extra          |
+-----------------------+---------+------+-----+---------+----------------+
| idconsultationmessage | int(11) | NO   | PRI | NULL    | auto_increment |
| idutilisateur         | int(11) | NO   | MUL | NULL    |                |
| idmessage             | int(11) | NO   | MUL | NULL    |                |
| etat                  | int(11) | YES  |     | NULL    |                |
+-----------------------+---------+------+-----+---------+----------------+
    */ 
        
        $consulationforum2=array(
        
            "idutilisateur"=>$u_rec_id,
             "idmessage"=>$idiscussionforum->sequence,
             "etat"=>1
        );
        $this->DAOGen->create($consulationforum2,null,null,"consultationmessage");
        
        
        
     
            $consulationforum=array(
                
                "idutilisateur"=>$utilisateur2,
                 "idmessage"=>$idiscussionforum->sequence,
                 "etat"=>0
            );
            $this->DAOGen->create($consulationforum,null,null,"consultationmessage");
        
        
        $test =new Jsend ("success","ok",null);   
            echo json_encode($test);     

    
    
        }else{
            $test =new Jsend ("failed","Veuillez taper Message",null);   
            echo json_encode($test);       
            ///erreur


        }
    
    }


    }
    
    
    
    }
}
    


    
<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
class AppelController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function ListeAppel(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['result']=$this->DAOGen->ReadGen("listeappel","where idutilisateur=".$u_rec_id);
        $this->load->view('Prof/listeappel',$data);
    }
}


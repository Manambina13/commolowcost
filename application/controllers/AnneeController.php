<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
class AnneeController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function Liste(){
        $where="where etat=1";
        $aneee=$this->DAOGen->ReadGen("anneescolaire",$where);
        for($i=0;$i<count($aneee);$i++){
            $aneee[$i]->etat=$aneee[$i]->debut."-".$aneee[$i]->fin;
        }
        $data['result']=$aneee;
        $this->load->view('admin/ListeAnnee',$data);
    }
    public function Terminer($idAnneee){
        echo $idAnneee;
        $where=array(
            "idanneescolaire"=>$idAnneee
        );
        $value=array(
            "etat"=>10
        );
        $this->DAOGen->update($value,$where,'anneescolaire');
        redirect("AnneeController/Liste");
    }
    //Promotion
    public function ModifierProm($idAnneee){
        $where="order by fin desc";
        $result=$this->DAOGen->ReadGen("promotion","where idpromotion='".$idAnneee."'");
        $year = \date("Y");  
        $debut=array();
        for($i=$year;$i<$year+10;$i++){
            array_push($debut,$i);
        }
        $data['result']=$result;
        $data["date"]=$debut;
        $this->load->view('admin/FicheModifProm',$data);
    }
    public function SubmitModifProm(){
        $str = $this->input->post('nom');
		$pattern = "/[a-zA-Z]{1,}/";
		$x=preg_match($pattern, $str);
        try{
        if($x==0){
			throw new \Exception("Veuillez entre un nom Exacte");
        }
        $where=array(
            "idpromotion"=>$this->input->post('idpromotion')
        );
        $data=array(
            "nom"=>$str
        );
        $this->DAOGen->update($data,$where,"promotion");
        redirect("PromotionController/ListePromotion");
        }catch(\Exception $e){
            $result=$this->DAOGen->ReadGen("promotion","where idpromotion='".$this->input->post('idpromotion')."'");
            $data['result']=$result;
            $data['error']=$e->getMessage();
            $this->load->view('admin/FicheModifProm',$data);
        }
    }
    //   
    public function Ajout(){
        $where="order by fin desc";
        $result=$this->DAOGen->ReadGen("anneescolaire",$where);
        $year = \date("Y");  
        $debut=array();
        for($i=$year;$i<$year+10;$i++){
            array_push($debut,$i);
        }
        $data["date"]=$debut;
        $this->load->view('admin/CreateDate',$data);
    }
    public function InsertionDate(){
        $debuts=$this->input->post('debut');
        $fin=$this->input->post('fin');
        
        if($fin==$debuts){
            $where="order by fin desc";
            $result=$this->DAOGen->ReadGen("anneescolaire",$where);
            $debut=array();
            /// 
            if(count($result)==0){
                $year = \date("Y");
                for($i=$year;$i<$year+10;$i++){
                    array_push($debut,$i);
                }
            }else{  
            for($i=$result[0]->fin;$i<$result[0]->fin+10;$i++){
                array_push($debut,$i);
            }
        }
            $data["date"]=$debut;
            $data["error"]="Date semblable";
            $this->load->view('admin/CreateDate',$data);

        }
        else if($fin<$debuts){
            $where="order by fin desc";
            $result=$this->DAOGen->ReadGen("anneescolaire",$where);
            $debut=array();
            if(count($result)>0){
            for($i=$result[0]->fin;$i<$result[0]->fin+10;$i++){
                array_push($debut,$i);
                }
            }else{
                $year = \date("Y");
                for($i=$year;$i<$year+10;$i++){
                    array_push($debut,$i);
                }

            }
            $data["date"]=$debut;
            $data["error"]="Date debut superieure a Fin";
            $this->load->view('admin/CreateDate',$data);
        }
        else if($fin>$debuts){
            $where="order by fin desc";
            $result=$this->DAOGen->ReadGen("anneescolaire",$where);
            if(count($result)==0){
                $x=array(
                    "debut"=>$debuts,
                    "fin"=>$fin,
                    "etat"=>1
                );
                $this->DAOGen->create($x,null,null,"anneescolaire");
                redirect("AnneeController/Liste");


            }else{
            $debut=array();
            for($i=$result[0]->fin;$i<$result[0]->fin+10;$i++){
                array_push($debut,$i);
                }
            if($debuts+1==$fin){
                $x=array(
                    "debut"=>$debuts,
                    "fin"=>$fin,
                    "etat"=>1
                );
                $where="where debut='".$debuts."' and fin='".$fin."'";
                $tableau=$this->DAOGen->ReadGen("anneescolaire",$where);
                if(count($tableau)>0){
                    
                $data["date"]=$debut;
                $data["error"]="date deja existant";
                $this->load->view('admin/CreateDate',$data);
                }
                else{
                $this->DAOGen->create($x,null,null,"anneescolaire");
                redirect("AnneeController/Liste");
                }
            }
            else{

                $data["date"]=$debut;
                $data["error"]="date Anormale";
                $this->load->view('admin/CreateDate',$data);
            }

            }
        }
    }
    public function MatiereParSemestre($idAnneee){
        $result=$this->DAOGen->ReadGen("classe",null);
        $data['classe']=$result;
        $this->load->view('admin/AssignerSemestre',$data);   
    }
    public function AnneeParSemestre(){
        $result=$this->DAOGen->ReadGen("classe",null);
        $data['classehidden']=$this->input->post('choix');
        $data['classe']=$result;
        $where="where idclasse='".$this->input->post('choix')."'";
        $data['semestre']=$this->DAOGen->ReadGen("semestre","where idclasse='".$this->input->post("choix")."'");
        $data['ue']=$this->DAOGen->ReadGen("ue","where idclasse='".$this->input->post("choix")."'");
        $this->load->view('admin/SuggestionSemetre',$data);
    }
    public function AnneeParSemestreTableau($idclasse){
        $result=$this->DAOGen->ReadGen("classe",null);
        $data['classehidden']=$idclasse;
        $data['classe']=$result;
        $where="where idclasse='".$idclasse."'";
        $data['semestre']=$this->DAOGen->ReadGen("semestre","where idclasse='".$idclasse."'");
        $data['ue']=$this->DAOGen->ReadGen("ue","where idclasse='".$idclasse."'");
        $this->load->view('admin/SuggestionSemetre',$data);
    }
    /*
    public function SemestreAnnee($idclasse){
        
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $data=$this->DAOGen->ReadGen("semestre","where idclasse='".$this->input->post("classehidden")."'");

        $premier=$this->input->post($data[0]->idsemestre);
        $suivant=$this->input->post($data[1]->idsemestre);
        $verification=$this->DAOGen->ReadGen("semestreannee","where idsemestre='".$data[0]->idsemestre."' and idsemestre='".$data[0]->idsemestre."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $whier="where idsemestre='".$data[0]->idsemestre."' and idsemestre='".$data[0]->idsemestre."' and idanneescolaire='".$annee[0]->idanneescolaire."'";
       // echo var_dump($verification);
        try{
        if(count($verification)>0){
            throw new Exception("Semestre deja Assigner");
        }
      
        echo var_dump($premier);
       
     if(count($premier)>$suivant){
            for($i=0;$i<count($premier);$i++){
              for($j=0;$j<count($suivant);$j++){
                  if($premier[$i]==$suivant[$j]){
                    throw new Exception("Même matière sélectionner");
                  }
              }  
            }
        }
        else{
            for($i=0;$i<count($suivant);$i++){
                for($j=0;$j<count($premier);$j++){
                    if($premier[$j]==$suivant[$i]){
                        throw new Exception("Même matière sélectionner");
                    }
                }  
              }
          }
          $idsequence=$this->DAOGen->getSequence("idsemestreannee","semestreannee");
          $data0=array(
            "idsemestreannee"=>$idsequence->sequence,
            "idanneescolaire"=>$annee[0]->idanneescolaire,
            "idsemestre"=>$data[0]->idsemestre,
            "idclasse"=>$this->input->post("classehidden"),
            "rang"=>1,
            "etat"=>1
        );
        $this->DAOGen->create($data0,null,null,"semestreannee");
        for($i=0;$i<count($premier);$i++){
            $idueannee=$this->DAOGen->getSequence("idueannee","ueannee");
            echo var_dump($idueannee);
            $data1=array(
                "idueannee"=>$idueannee->sequence,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idsemestreannee"=>$idsequence->sequence,
                "idue"=>$premier[$i],
                "etat"=>1
            );
            $rattrapageueannee=array(
                "idueannee"=>$idueannee->sequence,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "etat"=>0
            );
            $this->DAOGen->create($data1,null,null,"ueannee");
            $this->DAOGen->create($rattrapageueannee,null,null,"rattrapageueannee");
            $ec=$this->DAOGen->ReadGen("ec","where idue='".$premier[$i]."'");
            for($j=0;$j<count($ec);$j++){
                $idecannee=$this->DAOGen->getSequence("idecannee","ecannee");
                $data2=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idec"=>$ec[$j]->idec,
                "idue"=>$premier[$i],
                
                "etat"=>1
            );
            $this->DAOGen->create($data2,null,null,"ecannee");
           
            $rattrapageecannee=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idec"=>$ec[$j]->idec,
                "idecannee"=>$idecannee->sequence,
                "idueannee"=>$idueannee->sequence
            ); 
            $this->DAOGen->create($rattrapageecannee,null,null,"rattrapageecannee");
        }
           
           
        }
        $idsequence2=$this->DAOGen->getSequence("idsemestreannee","semestreannee");
        $data2=array(
          "idsemestreannee"=>$idsequence2->sequence,
          "idanneescolaire"=>$annee[0]->idanneescolaire,
          "idsemestre"=>$data[1]->idsemestre,
           "idclasse"=>$this->input->post("classehidden"),
            "rang"=>2,
          "etat"=>0
      );
      $this->DAOGen->create($data2,null,null,"semestreannee");
      echo var_dump($suivant);
      for($ii=0;$ii<count($suivant);$ii++){
        $idueannee=$this->DAOGen->getSequence("idueannee","ueannee");
        $data1=array(
            "idueannee"=>$idueannee->sequence,
            "idanneescolaire"=>$annee[0]->idanneescolaire,
            "idue"=>$suivant[$ii],
            "idsemestreannee"=>$idsequence2->sequence,
            "etat"=>0
        );
        $rattrapageueannee=array(
            "idueannee"=>$idueannee->sequence,
            "idanneescolaire"=>$annee[0]->idanneescolaire,
            "etat"=>0
        );
        $this->DAOGen->create($data1,null,null,"ueannee");
        $this->DAOGen->create($rattrapageueannee,null,null,"rattrapageueannee");
        $ec=$this->DAOGen->ReadGen("ec","where idue='".$suivant[$ii]."'");
        for($j=0;$j<count($ec);$j++){
            $idecannee=$this->DAOGen->getSequence("idecannee","ecannee");
            $data2=array(
            "idanneescolaire"=>$annee[0]->idanneescolaire,
            "idec"=>$ec[$j]->idec,
            "idue"=>$suivant[$ii],
            "etat"=>1
        );
        $this->DAOGen->Create($data2,null,null,"ecannee");
        $rattrapageecannee=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idec"=>$ec[$j]->idec,
                "idecannee"=>$idecannee->sequence,
                "idueannee"=>$idueannee->sequence
            ); 
            $this->DAOGen->create($rattrapageecannee,null,null,"rattrapageecannee");
        }   
       
    }
    redirect("AnneeController/Liste");
        }catch(\Exception $e){
            $result=$this->DAOGen->ReadGen("classe",null);
            $data['error']=$e->getMessage();
            $data['classehidden']=$this->input->post('classehidden');
            $data['classe']=$result;
            $where="where idclasse='".$this->input->post('classehidden')."'";
            $data['semestre']=$this->DAOGen->ReadGen("semestre","where idclasse='".$this->input->post("classehidden")."'");
            $data['ue']=$this->DAOGen->ReadGen("ue","where idclasse='".$this->input->post("classehidden")."'");
            $this->load->view('admin/SuggestionSemetre',$data);
            



    }
    }
   */

    public function DebutMatiere($idclasse){
        try{
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $ue=$this->DAOGen->ReadGen("ue","where idclasse=".$idclasse);
        $ec=$this->DAOGen->ReadGen("ec",null);
        $classe=$this->DAOGen->ReadGen("classe","where idclasse=".$idclasse);
        $iduearray=array();
        for($i=0;$i<count($ue);$i++){
            array_push($iduearray,$ue[$i]->idue);
        };
        $checkue=$this->DAOGen->ReadGen("ueannee","where (".$this->DAOGen->FonctiongetCreateOR($iduearray,"idue").") and idanneescolaire=".$annee[0]->idanneescolaire);
       // echo var_dump("select*from ueannee where (".$this->DAOGen->FonctiongetCreateOR($iduearray,"idue").") and idanneescolaire=".$annee[0]->idanneescolaire);
        if(count($checkue)>0){
      //      echo var_dump($checkue);
            throw new \Exception("Le niveau ".$classe[0]->classe." a déjà débuté.");
        }        
       // echo var_dump($ue);
        for($i=0;$i<count($ue);$i++){
            echo $i;
            $idue=$ue[$i]->idue;
            $listeEC=array_values(array_filter($ec,function($a) use ($idue){return $a->idue==$idue;}));
            $idueannee=$this->DAOGen->getSequence("idueannee","ueannee");
            $data1=array(
                "idueannee"=>$idueannee->sequence,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idue"=>$ue[$i]->idue,
                "etat"=>1
            );
            $rattrapageueannee=array(
                "idueannee"=>$idueannee->sequence,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "etat"=>0
            );
            $examensn=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idue"=>$ue[$i]->idue,
                "idueannee"=>$idueannee->sequence,
                "etat"=>0
            );
            $this->DAOGen->create($data1,null,null,"ueannee");
           $this->DAOGen->create($rattrapageueannee,null,null,"rattrapageueannee");
            $this->DAOGen->create($examensn,null,null,"examensn");
            for($j=0;$j<count($listeEC);$j++){
                $idecannee=$this->DAOGen->getSequence("idecannee","ecannee");
                $data2=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idec"=>$listeEC[$j]->idec,
                "idue"=>$ue[$i]->idue,
                "etat"=>1
            );
            $this->DAOGen->create($data2,null,null,"ecannee");
            $rattrapageecannee=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idec"=>$listeEC[$j]->idec,
                "idecannee"=>$idecannee->sequence,
                "idueannee"=>$idueannee->sequence
            );
            $ecanneesn=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idec"=>$listeEC[$j]->idec,
                "idecannee"=>$idecannee->sequence,
                "idueannee"=>$idueannee->sequence
            );
            $this->DAOGen->create($ecanneesn,null,null,"ecanneesn");
            $this->DAOGen->create($rattrapageecannee,null,null,"rattrapageecannee");
            }
        }
        redirect("AnneeController/Liste");
    }catch(\Exception $e){
        echo $e->getMessage();
      //  redirect('CommoStagiairemysql/AnneeController/Detail'); 
       $data['classe']=$this->DAOGen->ReadGen("classe",null);
       $data['error']=$e->getMessage(); 
       $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
       $annee[0]->debut=$annee[0]->debut."-".$annee[0]->fin;
       $data['annee']=$annee;
       $this->load->view('admin/DetailAnnee',$data);
    }
} 
    public function Detail(){
        $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");      
       $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $annee[0]->debut=$annee[0]->debut."-".$annee[0]->fin;
        $data['annee']=$annee;
        $this->load->view('admin/DetailAnnee',$data);
    }
    public function ValidationRattrapage($idclasse,$idannee){
        $this->load->model("MetierEC");
        $this->MetierEC->FinirRattrapage($idannee,$idclasse);

    }
    }


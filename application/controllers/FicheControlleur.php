<?php defined('BASEPATH') OR exit('No direct script access allowed');
class FicheControlleur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        $this->load->model('Fichemodel');
        $this->load->library("pagination");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function Create(){
        $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");
        $data['anneescolaire']=$this->DAOGen->RequeteFull(" select  CONCAT(`debut`, '-', `fin`) as annee,idanneescolaire FROM anneescolaire");
        $this->load->view('admin/ajoutfiche',$data);

    }
    public function Modification($id){
        $data["fiche"]=$this->DAOGen->ReadGen("fichepedagogique","where idfichepedagogique =".$id);
        $this->load->view('admin/modifierfiche',$data);



    }
    public function Submitmodification($id){
        try{    
            if($this->input->post('affichage')!="" || $this->input->post('affichage')!=null){
                $count=$this->DAOGen->ReadGen("fichepedagogique","where (lienuser='".$this->input->post('affichage')."' or lienuser='".$this->input->post('affichage')."') or (lienedit='".$this->input->post('affichage')."' or lienedit='".$this->input->post('affichage')."') and idfichepedagogique!=".$id);
                if(count($count)>0){
                throw new \Exception("Ce lien est déjà utilisé");
                }
            }
            if($this->input->post('modification')!="" || $this->input->post('modification')!=null){
                $count=$this->DAOGen->ReadGen("fichepedagogique","where (lienuser='".$this->input->post('modification')."' or lienuser='".$this->input->post('modification')."') or (lienedit='".$this->input->post('modification')."' or lienedit='".$this->input->post('modification')."') and idfichepedagogique!=".$id);
                if(count($count)>0){
                throw new \Exception("Ce lien est déjà utilisé");
                }
            }
            $modifier=array(
                "lienuser"=>$this->input->post('affichage'),
                "lienedit"=>$this->input->post('modification')
            );
            $idfiche=array(
                "idfichepedagogique"=>$id
            );
            $this->DAOGen->update($modifier,$idfiche,"fichepedagogique");
            $data['result']=$this->DAOGen->ReadGen("fichefull",null);
            for($i=0;$i<count($data['result']);$i++){
                $data['result'][$i]->debut=$data['result'][$i]->debut."-".$data['result'][$i]->fin;
            }
            $this->load->view('admin/Listefiche',$data);   
            
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data["fiche"]=$this->DAOGen->ReadGen("fichepedagogique","where idfichepedagogique =".$id);
            $this->load->view('admin/modifierfiche',$data);
        }
    }
    public function Submit(){
        try{
        $count=$this->DAOGen->ReadGen("fichepedagogique","where idclasse=".$this->input->post('debut')." and idanneescolaire =".$this->input->post('fin'));
        if(count($count)>0){
            
            throw new Exception('Ce fiche Existe deja');

        }else{
            if($this->input->post('affichage')!="" || $this->input->post('affichage')!=null){
                $count=$this->DAOGen->ReadGen("fichepedagogique","where (lienuser='".$this->input->post('affichage')."' or lienuser='".$this->input->post('affichage')."') or (lienedit='".$this->input->post('affichage')."' or lienedit='".$this->input->post('affichage')."')");
                if(count($count)>0){
                throw new \Exception("Ce lien est déjà utilisé.");
                }
            }
            if($this->input->post('modification')!="" || $this->input->post('modification')!=null){
                $count=$this->DAOGen->ReadGen("fichepedagogique","where (lienuser='".$this->input->post('modification')."' or lienuser='".$this->input->post('modification')."') or (lienedit='".$this->input->post('modification')."' or lienedit='".$this->input->post('modification')."')");
                if(count($count)>0){
                throw new \Exception("Ce lien est déjà utilisé");
                }
            }
                    $insert=array(
                    "idclasse"=>$this->input->post('debut'),
                    "idanneescolaire"=> $this->input->post('fin'),
                    "lienuser"=>$this->input->post('affichage'),
                    "lienedit"=> $this->input->post('modification') 
                    );
                    $this->DAOGen->create($insert,null,null,"fichepedagogique");
                    redirect('FicheControlleur/ListeFichePedagogique');
                    
                }
            }catch(\Exception $e){
                $data['error']=$e->getMessage();
                $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");
                $data['anneescolaire']=$this->DAOGen->RequeteFull(" select  CONCAT(`debut`, '-', `fin`) as annee,idanneescolaire FROM anneescolaire");
                $this->load->view('admin/ajoutfiche',$data);
            }

    }
    public function ListeFichePedagogique()
    {
        $data['anneescolaire']=$this->DAOGen->RequeteFull(" select  CONCAT(`debut`, '-', `fin`) as annee,idanneescolaire FROM anneescolaire");
        //$data['result']=$this->DAOGen->ReadGen("fichefull",null);
       // for($i=0;$i<count($data['result']);$i++){
         //   $data['result'][$i]->debut=$data['result'][$i]->debut."-".$data['result'][$i]->fin;

       // }
       $x=$this->DAOGen->ReadGen("fichefull",null);
       $taille=count($x);
       $config["base_url"] = base_url() . "FicheControlleur/ListeFichePedagogique";
       $config["total_rows"] = $taille;
       $config["per_page"] = 10;
       $config["uri_segment"] = 3;
       $this->pagination->initialize($config);
       $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["links"] = $this->pagination->create_links();
       $data['result'] = $this->Fichemodel->LimitProf($config["per_page"], $page);
       $this->load->view('admin/Listefiche',$data);   
    }
    public function Recherche(){
        $data['anneescolaire']=$this->DAOGen->RequeteFull(" select  CONCAT(`debut`, '-', `fin`) as annee,idanneescolaire FROM anneescolaire");
        $data['result']=$this->DAOGen->ReadGen("fichefull","where idanneescolaire=".$this->input->post('fin'));
        for($i=0;$i<count($data['result']);$i++){
            $data['result'][$i]->debut=$data['result'][$i]->debut."-".$data['result'][$i]->fin;

        }
        $data['links']='';
        $this->load->view('admin/Listefiche',$data);   
    }
    public function AfficherListe($id){
        $data['result']=$this->DAOGen->ReadGen("fichepedagogique","where idfichepedagogique=".$id);
        $this->load->view('admin/Fichedetail',$data);   
    }
    public function Ficheleve(){
        $idutilisateur=$this->session->userdata('idutilisateur');
        $result=$this->DAOGen->RequeteFull("select idinscription,idutilisateur,idclasse,idanneescolaire from inscription where  idutilisateur=".$idutilisateur."  order by idinscription desc");
        $fichepedagoqique=$this->DAOGen->ReadGen("fichepedagogique","where idclasse=".$result[0]->idclasse);
       // $fichepedagoqique[0]->lienuser ="https://docs.google.com/forms/d/e/1FAIpQLSdptk8OxSBf7-4QXMvmMB_1QHxjAoE5tQWfa04tyPg8DVkPqA/viewform";
        $data['result']=$fichepedagoqique;
        $this->load->view('Etudiant/fichepedagogique',$data);
               
        

    }
}
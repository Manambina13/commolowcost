<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ETController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function index(){
        $id=$this->session->userdata('idutilisateur');
        $getmaxetudiant=$this->DAOGen->RequeteFull('select max(idniveau),idetudiant as idniveau from inscription where idetudiant='.$id);
     //   $data['result']=$this->DAOGen->ReadGen('fichierformateur',"where etat=1 and idniveau<=".$getmaxetudiant[0]->idniveau);
     $data['result']=$this->DAOGen->RequeteFull(" select fichierformateur.*,matiere.nom_matiere,ec.nom_ec from ec join matiere on ec.idmatiere=matiere.idmatiere join fichierformateur on ec.idec=fichierformateur.idec and matiere.idmatiere=fichierformateur.idmatiere where fichierformateur.etat=1 and fichierformateur.idniveau<=".$getmaxetudiant[0]->idniveau);
     $this->load->view('Etudiant/listecours',$data);
    }
	public function agenda(){
		
		
		
    }
    public function addcours(){
        $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
      //  $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
      $id=$this->session->userdata('idutilisateur');
      $getmaxetudiant=$this->DAOGen->RequeteFull("select idniveau,idetudiant,idparcour from inscription where idetudiant='".$id."' order by idniveau desc");  
      if(count($getmaxetudiant)==0){
        $data['matiere']=[];
        $this->load->view('Etudiant/addcours',$data);
      }else{
      $data['matiere']=$this->DAOGen->ReadGen("matiere","where idparcour='".$getmaxetudiant[0]->idparcour."' and idniveau='".$getmaxetudiant[0]->idniveau."'");
      $this->load->view('Etudiant/addcours',$data);
      }
    }
    public function listefichier(){
        $id=$this->session->userdata('idutilisateur');
       // select fichieretudiant.*,parcour.nom_parcour,matiere.nom_matiere from matiere inner join fichieretudiant on matiere.idmatiere=fichieretudiant.idmatiere inner join parcour on fichieretudiant.idparcour=parcour.idparcour;
      //  $data['result']=$this->DAOGen->RequeteFull("select fichieretudiant.*,parcour.nom_parcour,matiere.nom_matiere from matiere inner join fichieretudiant on matiere.idmatiere=fichieretudiant.idmatiere inner join parcour on fichieretudiant.idparcour=parcour.idparcour where fichieretudiant.idetudiant=".$id);
      $data['result']=$this->DAOGen->RequeteFull("select fichieretudiant.*,parcour.nom_parcour,matiere.nom_matiere,ec.nom_ec from ec join matiere on ec.idmatiere=matiere.idmatiere  inner join fichieretudiant on matiere.idmatiere=fichieretudiant.idmatiere inner join parcour on fichieretudiant.idparcour=parcour.idparcour where fichieretudiant.idetudiant=".$id);
        $this->load->view('Etudiant/listecoursetudiant',$data);
    }
    public function do_upload(){


        try{
        $config['upload_path'] = 'upload/';
        $config['allowed_types']        = 'pdf|docx|xlsv';
    //	$config['max_size']             = 100;
//		$config['max_width']            = 1024;
//		$config['max_height']           = 768;

        $this->load->library('upload', $config);

// Alternately you can set preferences by calling the ``initialize()`` method. Useful if you auto-load the class:
$this->upload->initialize($config);

            if($this->input->post('matiere')==NULL){

                throw new \Exception("Veuillez completer la case matiere");
            }
        if ($this->upload->do_upload('profilePic')){
            $idec=$this->DAOGen->Changement($this->input->post('ec'));   
            $createdby  =   $this->session->userdata('idRestaurant');
            $data = $this->upload->data();
                $picture = array(
                    'photoPath' => $this->upload->data('full_path').$data['file_name']
                );
                //echo 'upload/'.$data['file_name'];

                $idutilisateur= $this->session->userdata('idutilisateur');
                $getannee=$this->DAOGen->ReadGen('anneeuniversitaire','where etat=5');
                if(count($getannee)==0){
                    throw new Exception("annee deja terminer");
                }
                $getinformation=$this->DAOGen->ReadGen('inscription','where idanneeuniversitaire='.$getannee[0]->idanneeuniversitaire);
                $fako='upload/'.$data['file_name'];
                $data = array( 
                    'idniveau'=>$getinformation[0]->idniveau,
                    'idparcour'=>$getinformation[0]->idparcour,
                    'idetudiant'=>$idutilisateur,
                    'idtypefichier'=>'1',
                    "idec"=>$idec,
                    'idmatiere'=>$this->input->post('matiere'),
                    'nom'=>$this->input->post('nom'),
                    'idanneeuniversitaire'=>$getinformation[0]->idanneeuniversitaire,
                    'lien'=>$fako
                 ); 
                 $this->DAOGen->Create($data,null,null,"fichieretudiant");
                 redirect('Etudiant/ETController/listefichier');  
                }
              
        
        else{
             $data['error']=$this->upload->display_errors();
             $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
             //  $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
             $id=$this->session->userdata('idutilisateur');
             $getmaxetudiant=$this->DAOGen->RequeteFull("select idniveau,idetudiant,idparcour from inscription where idetudiant='".$id."' order by idniveau desc");  
             if(count($getmaxetudiant)==0){
               $data['matiere']=[];
               $this->load->view('Etudiant/addcours',$data);
             }else{
             $data['matiere']=$this->DAOGen->ReadGen("matiere","where idparcour='".$getmaxetudiant[0]->idparcour."' and idniveau='".$getmaxetudiant[0]->idniveau."'");
             $this->load->view('Etudiant/addcours',$data);
             }
        }
        }catch(Exception $e){
         
            $data['parcour']=$e->getMessage();;
            //  $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
            $id=$this->session->userdata('idutilisateur');
            $getmaxetudiant=$this->DAOGen->RequeteFull("select idniveau,idetudiant,idparcour from inscription where idetudiant='".$id."' order by idniveau desc");  
            if(count($getmaxetudiant)==0){
              $data['matiere']=[];
              $this->load->view('Etudiant/addcours',$data);
            }else{
            $data['matiere']=$this->DAOGen->ReadGen("matiere","where idparcour='".$getmaxetudiant[0]->idparcour."' and idniveau='".$getmaxetudiant[0]->idniveau."'");
            $this->load->view('Etudiant/addcours',$data);
            }

        }   
    }
        
}

?>
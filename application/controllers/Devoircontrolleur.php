<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\Jsend;
class Devoircontrolleur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form', 'url'));
        $this->load->model("DAOGen");
        $this->load->model("MetierDevoir");
        $this->load->model('ExtensionsModel');
        $this->load->library("pagination");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function getNombreDevoirNonFini(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $ewault=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");
        $requete="select *from inscriptionClasse where idinscription=".$ewault[0]->idinscription;
        $getinfo=$this->DAOGen->RequeteFull($requete);
        $result2=$this->DAOGen->ReadGen("participantdevoircomplet","where idanneescolaire=".$getinfo[0]->idanneescolaire."  and idutilisateur=".$u_rec_id." and etat=1");
        $result=count($result2);
        $employee_object = new stdClass;
        $employee_object->nombre=$result;
        echo json_encode($employee_object);
    }
    public function Create(){
        $idutilisateur=$this->session->userdata('idutilisateur');
        $ec=$this->DAOGen->ReadGen("ec","where idutilisateur='".$idutilisateur."'");
        $data["ec"]=$ec;
        $this->load->view("Prof/CreateDevoir",$data);  
    }
    public function ListeDiscussion($idenvoyeur){
        $liste=$this->DAOGen->ReadGen("participantdevoircomplet","where iddevoir='".$idenvoyeur."'");
        for($i=0;$i<count($liste);$i++){
           if($liste[$i]->etat==0){
            $liste[$i]->etat="X";
           }else{
            $liste[$i]->etat="V";
           }
        }
        $data['data']=$liste;
  //     echo var_dump($liste);
        $this->load->view("Prof/ListeParDevoir",$data);
    }
    public function NewListeDiscussion($idenvoyeur){
        $liste=$this->DAOGen->ReadGen("participantdevoircomplet","where iddevoir='".$idenvoyeur."'");
        for($i=0;$i<count($liste);$i++){
           if($liste[$i]->etat==0){
            //$liste[$i]->etat="X";
          //  $liste[$i]->idfichier="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
           // $liste[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'></i>";
         
           //  $liste[$i]->etat="X";
         //  $liste[$i]->idfichier="X";
        
           $liste[$i]->etat="<i data-feather='x-circle'></i>";  
           $liste[$i]->idfichier="<i data-feather='x-circle'></i>";     
        }else{
                if($liste[$i]->idfichier==NULL){
           //         echo var_dump("ato");
               //     $liste[$i]->etat="X";
               //     $liste[$i]->idfichier="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
                //    $liste[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'></i>";
              //  $liste[$i]->etat="X";
               // $liste[$i]->idfichier="X";
                
               $liste[$i]->etat="<i data-feather='x-circle'></i>";  
                $liste[$i]->idfichier="<i data-feather='x-circle'></i>";  

                }else{
                $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$liste[$i]->idfichier);
                
                
                //$liste[$i]->idfichier="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom;
                $liste[$i]->idfichier="<a href='".$fichier[0]->lien."'><i data-feather='download'></i>"; 
          //      $liste[$i]->etat="V";
          $liste[$i]->etat="<i data-feather='check-circle'></i>"; 
            }
        //  $liste[$i]->etat="<i class='fa fa-check-circle fa-lg' aria-hidden='true'></i>";
           }
        }
        $data['result']=$liste;
   //     echo var_dump($data['result']);
   // echo var_dump($liste);
        $this->load->view("Prof/NewListeParDevoir",$data);
    }
    public function getEleveDevoir($idevoir){
        $liste=$this->DAOGen->ReadGen("participantdevoircomplet","where iddevoir='".$idevoir."'");
            for($i=0;$i<count($liste);$i++){
                if($liste[$i]->notes==NULL){
                    $liste[$i]->notes="<input type='text' name='note' class='form-control' id='basicInput'  required>";
                }
                else{
                    $liste[$i]->notes;    

                }
                
                
                
                
                
                
                if($liste[$i]->etat==0){
                 //$liste[$i]->etat="X";
               //  $liste[$i]->idfichier="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
                // $liste[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'></i>";
              //  $liste[$i]->idfichier="X";
                $liste[$i]->idfichier="<i data-feather='x-circle'></i>";       
            }else{
                 if($liste[$i]->idfichier==null){

                    $liste[$i]->idfichier="<i data-feather='x-circle'></i>";    
                 }else{
                 $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$liste[$i]->idfichier);
                $liste[$i]->idfichier="<a href='".$fichier[0]->lien."'><i data-feather='download' download></i>";
                 }
                }
             }
        $data['iddevoir']=$idevoir;
        $data['listeEleve']=$liste;
        $this->load->view("Prof/NoteDevoir",$data);
    }
    public function FaireUneDevoir($idec){
        $idutilisateur=$this->session->userdata('idutilisateur');
        $ec=$this->DAOGen->ReadGen("detailec","where idec=".$idec." and idutilisateur=".$idutilisateur);
        $data['idchoice']=$ec[0]->choix;
        $data['idec']=$idec;
        $data['idclasse']=$ec[0]->idclasse;
        $this->load->view("Prof/newcreatedevoir2",$data);
    }
    public function SubmitFaireUneDevoir(){
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $this->load->model('MetierDevoir');
        try{
        $this->MetierDevoir->Examen($this->input->post("nom"),$this->input->post('date1'),$this->input->post('date2'));
        $this->MetierDevoir->TraitementTotalCreate($anneescolaire,$this->input->post("nom"),$this->input->post('date1'),$this->input->post('date2'),$this->input->post('idec'),$this->input->post('idchoice'));
        $edetail=$this->DAOGen->ReadGen("detailec","where idec='".$this->input->post('idec')."'");
        $idutilisateur=$this->session->userdata('idutilisateur');
        $data['devoir']=$this->DAOGen->ReadGen("devoir","where idutilisateur='".$idutilisateur."' and idanneescolaire='".$anneescolaire[0]->idanneescolaire."'");
        redirect('Devoircontrolleur/parclasse/'.$edetail[0]->idclasse);
        }catch(\Exception $e){
            $liste=$this->DAOGen->ReadGen("detailec","where idec='".$this->input->post('idec')."'");
            $idclasse=$liste[0]->idclasse;
            $data['error']=$e->getMessage();
            $u_rec_id = $this->session->userdata('idutilisateur');
            $data['ec']=$this->DAOGen->ReadGen("detailec","where idutilisateur='".$u_rec_id."' and idclasse='".$idclasse."'");
            $this->load->view("Prof/newFicheDevoir",$data);
        }
    }
    public function newsubmitDevoir(){
        $liste=$this->DAOGen->ReadGen("participantdevoircomplet","where iddevoir=".$this->input->post('iddevoir'));
        $idec=$this->DAOGen->ReadGen("devoir","where iddevoir=".$this->input->post('iddevoir'));
        $dclasse=$this->DAOGen->ReadGen("ue","where idue=".$idec[0]->idue);
        for($i=0;$i<count($liste);$i++){
            $str=$this->input->post($liste[$i]->idutilisateur);
            $pattern = "/^(?:1?\d(?:\.\d{1,2})?|20(?:\.0?0?)?)$/i";
            $value=preg_match_all($pattern, $str); 
            if($value==0){
               throw new \Exception("Veuillez Bien Taper Le Note");
            }
        }
        for($i=0;$i<count($liste);$i++){
            $note=$this->input->post($liste[$i]->idutilisateur);
            $update=array(
            "notes"=>$note,
            "etat"=>10
            );
            $where=array(
                "idremisedevoir"=>$liste[$i]->idremisedevoir
            );
            $this->DAOGen->update($update,$where,"remisedevoir");

        }
        $update2=array(
            "etat"=>10
        );
        $where2=array(
            "iddevoir"=>$this->input->post('iddevoir')
        );
        $this->DAOGen->update($update2,$where2,"devoir");
        redirect("Devoircontrolleur/parclasse/".$dclasse[0]->idclasse);
    }
    public function ValidationDevoirOnly($iddevoir){
        $devoir=$this->DAOGen->ReadGen("devoir","where iddevoir=".$iddevoir);
        try{
        
        if($devoir[0]->etat==10){
            throw new \Exception("Devoir déjà valider");
        }else if($devoir[0]->etat==5){
            $remisedevoir=$this->DAOGen->ReadGen("remisedevoir","where notes is NULL and iddevoir=".$iddevoir);
            if(count($remisedevoir)>0){
                throw new \Exception("veuillez Notez tous les élèves");
            }
            else{
                $update=array(
                    "etat"=>10
                );
                $where=array(
                    "iddevoir"=>$iddevoir
                );
            $this->DAOGen->update($update,$where,"devoir");

            }

        }
        else if($devoir[0]->etat==0){
            throw new \Exception("Les eleves n'ont pas encore recu le devoir");
        }
        else if($devoir[0]->etat==1){
            throw new \Exception("Travaux pas encore fini");

        }
        redirect('Devoircontrolleur/getEleveDevoir/'.$iddevoir);


        }catch(\Exception $e){
            $idevoir=$iddevoir;
         //   echo var_dump($idevoir);
            $liste=$this->DAOGen->ReadGen("participantdevoircomplet","where iddevoir='".$idevoir."'");
            for($i=0;$i<count($liste);$i++){
                if($liste[$i]->notes==NULL){
                    $liste[$i]->notes="<input type='text' name='note' class='form-control' id='basicInput'  required>";
                }
                else{
                    $liste[$i]->notes;    

                }
                
                
                
                
                
                
                if($liste[$i]->etat==0){
             
                $liste[$i]->idfichier="X";
                 }else{
                 if($liste[$i]->idfichier==null){

                    $liste[$i]->idfichier="X";  
                 }else{
                 $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$liste[$i]->idfichier);
                 $liste[$i]->idfichier="<a href='".$fichier[0]->lien."'><i data-feather='download' download></i>";
                 }
                }
             }

        $data['error']=$e->getMessage();
        $data['iddevoir']=$idevoir;
        $data['listeEleve']=$liste;
        $this->load->view("Prof/NoteDevoir",$data);
       //      echo $e->getMessage();


        }

    }
    public function newsubmitDevoirSeul(){
        try{
        $str=$this->input->post("note");
        $pattern = "/^(?:1?\d(?:\.\d{1,2})?|20(?:\.0?0?)?)$/i";
        $value=preg_match_all($pattern, $str); 
        $idremisedevoir=$this->input->post('idremisedevoir');
        $remisedevoir=$this->DAOGen->ReadGen("remisedevoir","where idremisedevoir=".$idremisedevoir);
        $devoir=$this->DAOGen->ReadGen("devoir","where iddevoir=".$this->input->post('iddevoir'));
        if($devoir[0]->etat==10){
            throw new \Exception("Devoir déjà valider");
        }
        if($remisedevoir[0]->notes!=NULL){
            throw new \Exception("Devoir déjà note");
        }
        if($value==0){
            throw new \Exception("Veuillez Bien Taper Le Note");
        }
      
        $update=array(
            "notes"=>$str,
            "etat"=>10
            );
            $where=array(
                "idremisedevoir"=>$idremisedevoir
            );
            $this->DAOGen->update($update,$where,"remisedevoir");
        $redirect="Devoircontrolleur/getEleveDevoir/".$remisedevoir[0]->iddevoir;
        redirect($redirect);
        }catch(\Exception $e){
            $idevoir=$this->input->post('iddevoir');
            $liste=$this->DAOGen->ReadGen("participantdevoircomplet","where iddevoir='".$idevoir."'");
            $data['error']=$e->getMessage();
            for($i=0;$i<count($liste);$i++){
                if($liste[$i]->notes==NULL){
                    $liste[$i]->notes="<input type='text' name='note' class='form-control' id='basicInput'  required>";
                }
                else{
                    $liste[$i]->notes;    

                }            
                if($liste[$i]->etat==0){
                 //$liste[$i]->etat="X";
               //  $liste[$i]->idfichier="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
                // $liste[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'></i>";
                $liste[$i]->idfichier="X";
                 }else{
                 if($liste[$i]->idfichier==null){

                    $liste[$i]->idfichier="X";  
                 }else{
                 $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$liste[$i]->idfichier);
                 $liste[$i]->idfichier="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom;
             //  $liste[$i]->etat="<i class='fa fa-check-circle fa-lg' aria-hidden='true'></i>";
                 }
                }
             }
        $data['iddevoir']=$idevoir;
        $data['listeEleve']=$liste;
        $this->load->view("Prof/NoteDevoir",$data);
        }
    }
    public function getDiscussion(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $u_rec_id = $this->session->userdata('idutilisateur');
       //    echo "ato";
    //   echo json_encode($this->input->post()); 
           // $test =new Jsend ("success","ok",null);   
        
        //  echo json_encode($_POST);
         // echo $this->input->post('idforum');
      //  echo  $requete="where iddevoir='".$this->input->post('iddevoir')."' and idutilisateur='".$this->inpu order by date asc";
 //     idutilisateur='UT9'   
  //  echo $requete= "where iddevoir='".$this->input->post('iddevoir')."' and idutilisateur='".$this->input->post('ideleve')."' or  idutilisateur='".trim($u_rec_id)."'    order by date asc";
    
//  $req="where iddevoir='".$this->input->post('iddevoir')."' and idutilisateur='".$this->input->post('ideleve')."' or  idutilisateur='".trim($u_rec_id)."'    order by date asc";
    //    echo json_encode($_POST);
  // echo $req;
        if($u_rec_id==null){
            echo "ressource pas disponible";
        }
    
    
  
    $idiscussionforum=$this->DAOGen->ReadGen("discussionpossibledevoir","where iddevoir='".$this->input->post('iddevoir')."' and ideleve='".$this->input->post('ideleve')."'");
  //  echo "select*from discussionpossibledevoir where iddevoir='".$this->input->post('iddevoir')."' and ideleve='".$this->input->post('ideleve')."'";
    $data=$this->DAOGen->ReadGen("discussiondevoircomplet ","where iddiscussionpossibledevoir='".$idiscussionforum[0]->iddiscussionpossibledevoir."' and iddevoir='".$this->input->post('iddevoir')."' order by date asc");
   // echo "where iddiscussionpossibledevoir='".$idiscussionforum[0]->iddiscussionpossibledevoir."' and iddevoir='".$this->input->post('iddevoir')."' order by date asc";
         //  $data=$this->DAOGen->ReadGen("discussionforum","where idforum='FO13' order by date asc");
      
        for($i=0;$i<count($data);$i++){
                  if($data[$i]->idtypemessage=="2"){
                      $fich=$this->DAOGen->ReadGen("fichierMessage","where idfichier='".$data[$i]->idfichier."'");
                      
                    //  $data[$i]->Texte=$data[$i]->texte."<img src='".$fich[0]->lien."' alt='Girl in a jacket' width='300' height='300'>";
                        $data[$i]->idforum =$fich[0]->lien;
                        $data[$i]->idfichier=$fich[0]->nom;
                        $split=str_split($data[$i]->nom);
                        //   $data[$i]->idutilisateur=strtolower($split[0]);
                           $data[$i]->iddiscussiondevoir= "#icon-ava-".strtolower($split[0]);
                      
          
                  }
                  else if($data[$i]->idtypemessage=="3"){
                      $fich=$this->DAOGen->ReadGen("fichierMessage","where idfichier='".$data[$i]->idfichier."'");
                    //  $data[$i]->Texte=$data[$i]->texte."<a href='".$fich[0]->lien."'>".$fich[0]->nom."</a>";
                    $data[$i]->idforum =$fich[0]->lien;
                    $data[$i]->idfichier=$fich[0]->nom;
                    $split=str_split($data[$i]->nom);
                    //   $data[$i]->idutilisateur=strtolower($split[0]);
                       $data[$i]->iddiscussiondevoir= "#icon-ava-".strtolower($split[0]);
                  }
                  else if($data[$i]->idtypemessage=="4"){
                      $fich=$this->DAOGen->ReadGen("fichierMessage","where idfichier='".$data[$i]->idfichier."'");
                    //  $data[$i]->Texte=$data[$i]->texte."<a href='".$fich[0]->lien."'>".$fich[0]->nom."</a>";
                    $data[$i]->idforum =$fich[0]->lien;
                    $data[$i]->idfichier=$fich[0]->nom;
                    $split=str_split($data[$i]->nom);
                    //   $data[$i]->idutilisateur=strtolower($split[0]);
                       $data[$i]->iddiscussiondevoir= "#icon-ava-".strtolower($split[0]);
                  }
                  else if($data[$i]->idtypemessage=="TY1"){
                    $split=str_split($data[$i]->nom);
                    //   $data[$i]->idutilisateur=strtolower($split[0]);
                       $data[$i]->iddiscussiondevoir= "#icon-ava-".strtolower($split[0]);
  
                  }
  
  
          }
      //    echo json_encode($data);
        //  $x="where iddevoir='".$this->input->post('iddevoir')."' and idutilisateur='".$this->input->post('ideleve')."' order by date asc";
       //   echo json_encode($x);
          $test =new Jsend ("success","ok",$data);   
          echo json_encode($test);
          
          
    }
    public function Discussion($idevoir,$ideleve){
        $data['iddevoir']=$idevoir;
        $data['ideleve']=$ideleve;
        $fichier=$this->DAOGen->ReadGen("discussionpossibledevoir","where ideleve='".trim($ideleve)."'");
      
        $data['idpossible']=$fichier[0]->iddiscussionpossibledevoir;
    //    echo var_dump($data['idpossible']);
        $this->load->view('Prof/MessageDevoir',$data);
    }
    public function NewDiscussion($idevoir,$ideleve){
        $data['iddevoir']=$idevoir;
        $data['ideleve']=$ideleve;
        $fichier=$this->DAOGen->ReadGen("discussionpossibledevoir","where ideleve='".trim($ideleve)."' and iddevoir='".$idevoir."'");
      
        $data['idpossible']=$fichier[0]->iddiscussionpossibledevoir;
    //    echo var_dump($data['idpossible']);
        $this->load->view('Prof/NewMessageDevoir',$data);
    }
    public function CreateSubmit(){
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $this->load->model('MetierDevoir');
        try{
        $this->MetierDevoir->Examen($this->input->post("nom"),$this->input->post('date1'),$this->input->post('date2'));
        $this->MetierDevoir->TraitementTotalCreate($anneescolaire,$this->input->post("nom"),$this->input->post('date1'),$this->input->post('date2'),$this->input->post('idec'));
        
        $edetail=$this->DAOGen->ReadGen("detailec","where idec='".$this->input->post('idec')."'");
        $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$edetail[0]->idue."' and idanneescolaire='".$anneescolaire[0]->idanneescolaire."'");
      //  $this->MetierDevoir->Traitement($ue,) 
        //      $this->DAOGen->create($data,null,null,"Devoir");
        $idutilisateur=$this->session->userdata('idutilisateur');
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $idutilisateur = $this->session->userdata('idutilisateur');
        $data['devoir']=$this->DAOGen->ReadGen("devoir","where idutilisateur='".$idutilisateur."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $this->load->view('Prof/Devoir',$data);
        

        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $idutilisateur=$this->session->userdata('idutilisateur');
            $ec=$this->DAOGen->ReadGen("ec","where idutilisateur='".$idutilisateur."'");
            $data["ec"]=$ec;
            $this->load->view("Prof/CreateDevoir",$data);  


        }
    }
    public function NewCreateSubmit(){
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $this->load->model('MetierDevoir');
        try{
        if(count($anneescolaire)==0){
            throw new \Exception("L'année n'a pas encore commencé");

        }
        $this->MetierDevoir->Examen($this->input->post("nom"),$this->input->post('date1'),$this->input->post('date2'));
        $this->MetierDevoir->TraitementTotalCreate($anneescolaire,$this->input->post("nom"),$this->input->post('date1'),$this->input->post('date2'),$this->input->post('idec'),$this->input->post('idchoice'));
        
        $edetail=$this->DAOGen->ReadGen("detailec","where idec='".$this->input->post('idec')."'");
        $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$edetail[0]->idue."' and idanneescolaire='".$anneescolaire[0]->idanneescolaire."'");
      //  $this->MetierDevoir->Traitement($ue,) 
        //      $this->DAOGen->create($data,null,null,"Devoir");
        $idutilisateur=$this->session->userdata('idutilisateur');
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $idutilisateur = $this->session->userdata('idutilisateur');
        $data['devoir']=$this->DAOGen->ReadGen("devoir","where idutilisateur='".$idutilisateur."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
    //    $this->load->view('Prof/Devoir',$data);    
     //   $redirect='Devoircontrolleur/parclasse/'.$edetail[0]->idclasse;
       // echo $redirect;
        redirect('Devoircontrolleur/parclasse/'.$edetail[0]->idclasse);

        }catch(\Exception $e){
            $liste=$this->DAOGen->ReadGen("detailec","where idec='".$this->input->post('idec')."'");
            $idclasse=$liste[0]->idclasse;
            $data['error']=$e->getMessage();
            $u_rec_id = $this->session->userdata('idutilisateur');
            $data['ec']=$this->DAOGen->ReadGen("detailec","where idutilisateur='".$u_rec_id."' and idclasse='".$idclasse."'");
            $this->load->view("Prof/newFicheDevoir",$data);


        }



    }
    public function Examenparclasse($idclasse){



        
    }
    public function fichenew($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $result['ec']=$this->DAOGen->ReadGen("detailec","where idutilisateur='".$u_rec_id."' and idclasse='".$idclasse."'");
        try{
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        if(count($annee)==0){
            throw new \Exception("L'année n'a pas encore commencé");
        }
        $this->load->view("Prof/newFicheDevoir",$result);
        }catch(\Exception $e){
            $u_rec_id = $this->session->userdata('idutilisateur');
            $this->load->model('MetierDevoir');
            $config = array();
            $config["base_url"] = base_url() . "Devoircontrolleur/parclasse/".$idclasse;
            $config["total_rows"] = $this->MetierDevoir->countgetallnewDevoir($idclasse,$u_rec_id);
            $config["per_page"] = 5;
            $config["uri_segment"] = 4;
           // echo  var_dump($this->uri->segment(3));
            $this->pagination->initialize($config);
           
    
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
    
            $data["links"] = $this->pagination->create_links();
            $result=$this->MetierDevoir->limitnewdevoir($idclasse,$u_rec_id,$config["per_page"], $page);
            for($i=0;$i<count($result);$i++){
            $devoirpar=$this->DAOGen->ReadGen("remisedevoir","where iddevoir=".$result[$i]->iddevoir);
            $getfini=array_values(array_filter($devoirpar,function($a){return $a->idfichier!=NULL;}));
             $result[$i]->idue=count($getfini)."/".count($devoirpar);
            }
    
    
            $data['error']=$e->getMessage();
            $data['idclasse']=$idclasse;
            $data['result'] = $result;
        //   $this->load->view('authors/index', $data);
      //  echo var_dump($this->uri->segment(4)); 
        $this->load->view('Prof/NewDevoir',$data);
            


        }



    }
    public function parclasse($idclasse){
  
        $u_rec_id = $this->session->userdata('idutilisateur');
        $this->load->model('MetierDevoir');
        $config = array();
        $config["base_url"] = base_url() . "Devoircontrolleur/parclasse/".$idclasse;
        $config["total_rows"] = $this->MetierDevoir->countgetallnewDevoir($idclasse,$u_rec_id);
        $config["per_page"] = 5;
        $config["uri_segment"] = 4;
       // echo  var_dump($this->uri->segment(3));
        $this->pagination->initialize($config);
       

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data["links"] = $this->pagination->create_links();
        $result=$this->MetierDevoir->limitnewdevoir($idclasse,$u_rec_id,$config["per_page"], $page);
        for($i=0;$i<count($result);$i++){
        $devoirpar=$this->DAOGen->ReadGen("remisedevoir","where iddevoir=".$result[$i]->iddevoir);
        $getfini=array_values(array_filter($devoirpar,function($a){return $a->idfichier!=NULL;}));
         $result[$i]->idue=count($getfini)."/".count($devoirpar);
        }



        $data['idclasse']=$idclasse;
        $data['result'] = $result;
    //   $this->load->view('authors/index', $data);
  //  echo var_dump($this->uri->segment(4)); 
    $this->load->view('Prof/NewDevoir',$data);
    }
    




    //x//Cote eleve
    public function parclasseEleve($idue){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $classe=$this->DAOGen->ReadGen("ue","where idue=".$idue);
       // $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
       // echo $idclasse;
       // $data['result']=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and idue='".$idue."' and eleve=".$u_rec_id);
       $ewault=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");
        $requete="select *from inscriptionClasse where idinscription=".$ewault[0]->idinscription;
        $anneescolaire=$this->DAOGen->RequeteFull($requete); 
       $result=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and idue='".$idue."' and eleve=".$u_rec_id." and etat>0");
        for($i=0;$i<count($result);$i++){
            //echo $result[$i]->etat;
            if($result[$i]->etat==5){
            if($result[$i]->etatremise>=5){
            //    echo var_dump("ato");
                $result[$i]->etatremise="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>";
                if($result[$i]->idfichier==[]){

                    $result[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";

                }else{
                    $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$result[$i]->idfichier);
                    //  $result[$i]->etat="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom
                    $result[$i]->etat="<a href='".$fichier[0]->lien."'><i data-feather='download'></i>"; 

                }
                
                
              
              $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'></i>"; 
             
            }
            else{
               
                $result[$i]->etatremise="<i class='fa fa-times-circle fa-lg' aria-hidden='true'></i>";
             //   $result[$i]->etat="<input type='file' name='userimage' id='userimage'>";
                $result[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
             //      $result[$i]->notes="<button type='submit' value='Upload' name='upload' class='btn btn-success'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i> </button>";
                  $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
        }
        }else {
            if($result[$i]->etatremise>=5){
                $result[$i]->etatremise="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>";
               
                if($result[$i]->idfichier==[]){

                    $result[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";

                }else{
                    $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$result[$i]->idfichier);
                    //  $result[$i]->etat="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom
                    $result[$i]->etat="<a href='".$fichier[0]->lien."'><i data-feather='download'></i>"; 

                }
               
            $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
            }
            else{
               
                $result[$i]->etatremise="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
                $result[$i]->etat="<input type='file' name='userimage' id='userimage'>";
                $result[$i]->notes="<button type='submit' value='Upload' name='upload' class='btn btn-success'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i> </button>";
             //   $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
        }







        }

        }
        $data['result']=$result;
        $data['idclasse']=$classe[0]->idclasse;
     //   echo var_dump($data['result']);
           $this->load->view('Etudiant/Listedevoir',$data);
    }
    public function Devoirsurnotif(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        //$classe=$this->DAOGen->ReadGen("ue","where idue=".$idue);
        $ewault=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");
        $requete="select *from inscriptionClasse where idinscription=".$ewault[0]->idinscription;
        $anneescolaire=$this->DAOGen->RequeteFull($requete);
       
       
        // echo $idclasse;
       // $data['result']=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and idue='".$idue."' and eleve=".$u_rec_id);
        $result=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire."   and eleve=".$u_rec_id." and etat=1");
        for($i=0;$i<count($result);$i++){
            //echo $result[$i]->etat;
            if($result[$i]->etat==5){
            if($result[$i]->etatremise>=5){
            //    echo var_dump("ato");
                $result[$i]->etatremise="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>";
                if($result[$i]->idfichier==[]){

                    $result[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";

                }else{
                    $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$result[$i]->idfichier);
                    //  $result[$i]->etat="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom
                    $result[$i]->etat="<a href='".$fichier[0]->lien."'><i data-feather='download'></i>"; 

                }
                
                
              
              $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'></i>"; 
             
            }
            else{
               
                $result[$i]->etatremise="<i class='fa fa-times-circle fa-lg' aria-hidden='true'></i>";
             //   $result[$i]->etat="<input type='file' name='userimage' id='userimage'>";
                $result[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
             //      $result[$i]->notes="<button type='submit' value='Upload' name='upload' class='btn btn-success'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i> </button>";
                  $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
        }
        }else {
            if($result[$i]->etatremise>=5){
                $result[$i]->etatremise="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>";
               
                if($result[$i]->idfichier==[]){

                    $result[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";

                }else{
                    $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$result[$i]->idfichier);
                    //  $result[$i]->etat="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom
                    $result[$i]->etat="<a href='".$fichier[0]->lien."'><i data-feather='download'></i>"; 

                }
               
            $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
            }
            else{
               
                $result[$i]->etatremise="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
                $result[$i]->etat="<input type='file' name='userimage' id='userimage'>";
                $result[$i]->notes="<button type='submit' value='Upload' name='upload' class='btn btn-success'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i> </button>";
             //   $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
        }







        }

        }
        $data['result']=$result;
        $data['idclasse']=$anneescolaire[0]->idclasse;
     //   echo var_dump($data['result']);
           $this->load->view('Etudiant/Listedevoir',$data);
    }
    public function getnombredevoir(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $controle=$this->DAOGen->ReadGen("remisedevoir","where iddevoir=".$this->input->post('iddevoir')." and idutilisateur=".$u_rec_id);
    }
    public function ajoutetatdevoir(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        try{
    //    echo $this->input->post('iddevoir');
        $controle=$this->DAOGen->ReadGen("remisedevoir","where iddevoir=".$this->input->post('iddevoir')." and idutilisateur=".$u_rec_id);
        if($controle[0]->etat==5 || $controle[0]->etat==10){
            throw new \Exception("Devoir deja Valider");
        }
    
    
        if(isset($_FILES["userimage"]["name"]))  
        {
            //echo $_FILES["userimage"]["name"];
            $typeMessage=$this->ExtensionsModel->getExtension($_FILES["userimage"]["name"]);
        //    echo $typeMessage;
            $path=$this->ExtensionsModel->getPath($_FILES["userimage"]["name"]);
            echo $path;
            $config['upload_path']=$path;
            $config['allowed_types']='pdf|docx|zip';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload',$config);
      //    $this->upload->initialize($config);
     if(!$this->upload->do_upload('userimage'))  
     {  
     echo $this->upload->display_errors();  
     }  
     else  
     {  
     $data = array('upload_data' => $this->upload->data());
     $idfichier=$this->DAOGen->getSequence("idfichier","fichier");
     $title= $this->input->post('title');
     $image= $data['upload_data']['file_name']; 
     echo $image;
     $discussionforum=array(
        "idfichier"=>$idfichier->sequence,
        "etat"=>5
    );
    $where=array(
        "idutilisateur"=>$u_rec_id,
        "iddevoir"=> $this->input->post('iddevoir')
        
    );
    $datafichier=array(
        "idfichier"=>$idfichier->sequence,
        "idtypemessage"=>$typeMessage[0]->idtypemessage,
        "nom"=>$_FILES["userimage"]["name"],
        "lien"=>$this->ExtensionsModel->reglagepath($path,$image)
    );
    $this->DAOGen->create($datafichier,null,null,"fichier");
    $this->DAOGen->update($discussionforum,$where,"remisedevoir");
    redirect('Devoircontrolleur/parclasseEleve/'.$this->input->post('idclasse'));
    }
    }else{
        $data['error']="Votre extension n'est pas possible";
        $u_rec_id = $this->session->userdata('idutilisateur');
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $data['result']=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and  etat=1 and idclasse='".$this->input->post('idclasse')."' and eleve=".$u_rec_id);
        $data['idclasse']=$idclasse;
        $this->load->view('Etudiant/Listedevoir',$data);
        echo "ato";
    }
        }catch(\Exception $e){

            $devoir=$this->DAOGen->ReadGen("devoir","where iddevoir=".$this->input->post('iddevoir'));
            $idue=$devoir[0]->idue;
            $classe=$this->DAOGen->ReadGen("ue","where idue=".$idue);
            $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
           // echo $idclasse;
           // $data['result']=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and idue='".$idue."' and eleve=".$u_rec_id);
            $result=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and idue='".$idue."' and eleve=".$u_rec_id." and etat>0");
            for($i=0;$i<count($result);$i++){
                //echo $result[$i]->etat;
                if($result[$i]->etat==5){
                if($result[$i]->etatremise==5){
                    $result[$i]->etatremise="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>";
                    $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$result[$i]->idfichier);
                    $result[$i]->etat="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom;
                    $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'></i>"; 
                 
                }
                else{
                   
                    $result[$i]->etatremise="<i class='fa fa-times-circle fa-lg' aria-hidden='true'></i>";
                 //   $result[$i]->etat="<input type='file' name='userimage' id='userimage'>";
                    $result[$i]->etat="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
                 //      $result[$i]->notes="<button type='submit' value='Upload' name='upload' class='btn btn-success'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i> </button>";
                      $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
            }
            }else {
                if($result[$i]->etatremise==5){
                    $result[$i]->etatremise="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>";
                    $fichier=$this->DAOGen->ReadGen("fichier","where idfichier=".$result[$i]->idfichier);
                    $result[$i]->etat="<a href='".$fichier[0]->lien."'>".$fichier[0]->nom;
                 
                }
                else{
                   
                    $result[$i]->etatremise="<i class='fa fa-times-circle fa-lg' aria-hidden='true'>";
                    $result[$i]->etat="<input type='file' name='userimage' id='userimage'>";
                    $result[$i]->notes="<button type='submit' value='Upload' name='upload' class='btn btn-success'><i class='fa fa-check-circle fa-lg' aria-hidden='true'></i> </button>";
                 //   $result[$i]->notes="<i class='fa fa-check-circle fa-lg' aria-hidden='true'>"; 
            }
    
    
    
    
    
    
    
            }
    
            }
            $data['error']=$e->getMessage();
            $data['result']=$result;
            $data['idclasse']=$classe[0]->idclasse;
            $this->load->view('Etudiant/Listedevoir',$data);
        }



        
    }
    //x/////
    public function nombreDevoir(){
        $getnowanneescolaire=$this->DAOGen->RequeteFull("select*from anneescolaire order by idanneescolaire desc");
        $u_rec_id = $this->session->userdata('idutilisateur');
        if(count($getnowanneescolaire)>0){
            $listeidevoir=$this->DAOGen->RequeteFull("select distinct(iddevoir) from devoir where idanneescolaire='".$getnowanneescolaire[0]->idanneescolaire."' and etat=5 and idutilisateur=".$u_rec_id);        
            $employee_object = new stdClass;
            $employee_object->nombre=count($listeidevoir);
            echo json_encode($employee_object);
        }else{
            $employee_object = new stdClass;
            $employee_object->nombre=0;
            echo json_encode($employee_object);
        }
    }
    public function Devoirnotification(){
        $getnowanneescolaire=$this->DAOGen->RequeteFull("select*from anneescolaire order by idanneescolaire desc");
        $u_rec_id = $this->session->userdata('idutilisateur');
        if(count($getnowanneescolaire)>0){
            $listeidevoir=$this->DAOGen->RequeteFull("select distinct(iddevoir) from devoir where idanneescolaire='".$getnowanneescolaire[0]->idanneescolaire."' and etat=5 and idutilisateur=".$u_rec_id);        
            $getDevoir=$this->DAOGen->FonctiongetCreateORVersion2($listeidevoir,"iddevoir");
            $result=$this->DAOGen->ReadGen("devoircomplet","where ".$getDevoir);
            for($i=0;$i<count($result);$i++){
                $devoirpar=$this->DAOGen->ReadGen("remisedevoir","where iddevoir=".$result[$i]->iddevoir);
                $getfini=array_values(array_filter($devoirpar,function($a){return $a->idfichier!=NULL;}));
                 $result[$i]->idue=count($getfini)."/".count($devoirpar);
            }
            $data['result']=$result;
            $this->load->view("Prof/Notifdevoir",$data);
        }else{
            $data['result']=[];
            $this->load->view("Prof/Notifdevoir",$data);
        }
    }
    public function Insertion(){
        echo json_encode($this->input->post()); 
        
        if(isset($_FILES["file"]["name"]))  
        {
            echo $_FILES["file"]["name"];
            $typeMessage=$this->ExtensionsModel->getExtension($_FILES["file"]["name"]);
            $path=$this->ExtensionsModel->getPath($_FILES["file"]["name"]);
            $config['upload_path']=$path;
            $config['allowed_types']='gif|jpg|png|pdf|docx|zip';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload',$config);
      //    $this->upload->initialize($config);
     if(!$this->upload->do_upload('file'))  
     {  
     echo $this->upload->display_errors();  
     }  
     else  
     {  
     $data = array('upload_data' => $this->upload->data());
     
     $title= $this->input->post('title');
     $image= $data['upload_data']['file_name']; 
     
     //$result= $this->upload_model->save_upload($title,$image);
    
   //  $this->DAOUsers->create($url,null,null,"lien");
  //   echo '<img src="'.base_url().'assets/images/'.$image.'" width="300" height="225" class="img-thumbnail" />';  
  if($this->input->post('texte')!=null){
    $u_rec_id = $this->session->userdata('idutilisateur');
    $idtypefichier="TY1";
    $text=$this->input->post('texte');
    $idforum=$this->input->post('idforum');
    $idiscussionforum=$this->DAOGen->getSequence("iddiscussiondevoir","discussiondevoir");
    $datetime = new \DateTime("now"); 
    $idfichier=$this->DAOGen->getSequence("idfichier","fichier");
    
    
    
    $discussionforum=array(
        "iddiscussiondevoir"=>$idiscussionforum->sequence,
        "idtypemessage"=>$typeMessage[0]->idtypemessage, 
        "idutilisateur"=>$u_rec_id,
        "iddiscussionpossibledevoir"=>$this->input->post('idpossible'),
        "texte"=>$text,
        "idfichier"=>$idfichier->sequence,
        "iddevoir"=>$this->input->post('iddevoir'),
        "date"=>$datetime->format('Y-m-d H:i:s')   
    );
   
    $datafichier=array(
        "idfichier"=>$idfichier->sequence,
        "idtypemessage"=>$typeMessage[0]->idtypemessage,
        "nom"=>$_FILES["file"]["name"],
        "lien"=>$this->ExtensionsModel->reglagepath($path,$image)
    );
    $this->DAOGen->create($datafichier,null,null,"fichiermessage");
    $this->DAOGen->create($discussionforum,null,null,"discussiondevoir");
    
   /*
    $membreForum=$this->DAOGen->ReadGen("participant","where idforum='".$idforum."' and idutilisateur!='".$u_rec_id."'");
    $consulationforum2=array(
        "idforum"=>$idforum,
        "idutilisateur"=>$u_rec_id,
         "iddiscussionforum"=>"DI".$idiscussionforum->sequence,
         "etat"=>1
    );
    $this->DAOGen->create($consulationforum2,"idConsultationForum","CO","consultationforum");
    
    
    
    for($i=0;$i<count($membreForum);$i++){
        $consulationforum=array(
            "idforum"=>$idforum,
            "idutilisateur"=>$membreForum[$i]->idutilisateur,
             "iddiscussionforum"=>"DI".$idiscussionforum->sequence,
             "etat"=>0
        );
        $this->DAOGen->create($consulationforum,"idConsultationForum","CO","consultationforum");
    
    }
    */
    $test =new Jsend ("success","ok",null);   
        echo json_encode($test);     



    }else{
        $test =new Jsend ("failed","Veuillez taper Message",null);   
        echo json_encode($test);       
        ///erreur


    }
    


    }  
    }else{
        if($this->input->post('texte')!=null){
        echo json_encode($this->input->post());
        
        $u_rec_id = $this->session->userdata('idutilisateur');
        $idtypefichier="TY1";
        $text=$this->input->post('texte');
        $idforum=$this->input->post('idforum');
        $idiscussionforum=$this->DAOGen->getSequence("iddiscussiondevoir","discussiondevoir");
        $datetime = new \DateTime("now"); 
        $discussionforum=array(
            "iddiscussiondevoir"=>$idiscussionforum->sequence,
            "idtypemessage"=>$idtypefichier, 
            "idutilisateur"=>$u_rec_id,
            "iddiscussionpossibledevoir"=>$this->input->post('idpossible'),
            "texte"=>$text,
            "iddevoir"=>$this->input->post('iddevoir'),
            "date"=>$datetime->format('Y-m-d H:i:s')   
        );
        $this->DAOGen->create($discussionforum,null,null,"discussiondevoir");
        
        //$membreForum=$this->DAOGen->ReadGen("participant","where idforum='".$idforum."' and idutilisateur!='".$u_rec_id."'");
        
        /*
        $consulationforum2=array(
            "idforum"=>$idforum,
            "idutilisateur"=>$u_rec_id,
             "iddiscussionforum"=>"DI".$idiscussionforum->sequence,
             "etat"=>1
        );
        $this->DAOGen->create($consulationforum2,"idConsultationForum","CO","consultationforum");
        
        for($i=0;$i<count($membreForum);$i++){
            $consulationforum=array(
                "idforum"=>$idforum,
                "idutilisateur"=>$membreForum[$i]->idutilisateur,
                 "iddiscussionforum"=>"DI".$idiscussionforum->sequence,
                 "etat"=>0
            );
            $this->DAOGen->create($consulationforum,"idConsultationForum","CO","consultationforum");
            
        }
        
        */
        $test =new Jsend ("success","ok",null);   
            echo json_encode($test);     
        
    
    
        }else{
            $test =new Jsend ("failed","Veuillez taper Message",null);   
            echo json_encode($test);       
            ///erreur


        }
    
    }

    }

}


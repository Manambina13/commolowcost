<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Formulaire extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model('DAOGen');
        $this->load->model("Metieretudiant");
      }
    public function formulaireetudiant(){
        $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
        $data['parcour']=$this->DAOGen->ReadGen("parcour",null);
        $this->load->view('formulaireetudiant',$data);
    }
    public function process(){
    $email=$this->input->post('email');
    $pass=$this->input->post('pass');
   // echo $email." ".$pass;
  //  $where="Where email='".trim($email)."' and mdp='".sha1(trim($pass))."'";
    $where="Where email='".trim($email)."' and mdp='".sha1(trim($pass))."' and etat=1";
 ///   echo $where;
    $this->load->model('DAOGen');
    $login=$this->DAOGen->ReadGen("formateur",$where);
//	echo var_dump($where);	
    if(count($login)>0){
        
        $session_data = array(  
            'idutilisateur'     =>     $login[0]->idformateur,
            'idprofile' =>$login[0]->idprofile
           );
           $this->session->set_userdata($session_data);
           redirect('Prof/ProfController/index');
    }else{
        $where2="Where email='".trim($email)."' and mdp='".sha1(trim($pass))."' and etat=5";
        $login=$this->DAOGen->ReadGen("etudiant",$where2);
        if(count($login)>0){
        $session_data = array(  
            'idutilisateur'     =>     $login[0]->idetudiant,
            'idprofile' =>$login[0]->idprofile
           );
           $this->session->set_userdata($session_data);
           redirect('Etudiant/ETController');        
        }
        else{

            $login=$this->DAOGen->ReadGen("admin",$where);
            if(count($login)>0){
            $session_data = array(  
                'idutilisateur'     =>     $login[0]->idutilisateur,
                'idprofile' =>$login[0]->idprofile
               );
               $this->session->set_userdata($session_data);
               redirect('Admin/Etudiant/listeetudiant');  
            }
            else{

                $data['error']="Utilisateur Inexistant";
                $this->load->view('index',$data);


            }




    }
    
       
      
    }     
    }
    public function submitajoutetudiant(){
        try{
            $this->Metieretudiant->controleajout($this->input->post('nom'),$this->input->post('prenom'),$this->input->post('password'),$this->input->post('numero'),$this->input->post('email'),$this->input->post('password2'));      
            if($this->input->post('matricule')=="" || $this->input->post('matricule')==null ){
                throw new \Exception("Matricule probleme");
            }
            else{
                $result=$this->DAOGen->ReadGen("etudiant","where matricule='".strtoupper($this->input->post('matricule'))."'");
                if(count($result)>0){

                    throw new \Exception("Matricule existant");
                }
            }
            $checkdate=$this->DAOGen->ReadGen("anneeuniversitaire","where etat=1");
            if(count($checkdate)==0){

                throw new \Exception("L'annee a deja debute");
            }
            $gesequence=$this->DAOGen->getSequence("idetudiant","etudiant");
            $ajout=array(
                "idetudiant"=>$gesequence->sequence,
                "nom"=>$this->input->post('nom'),
                "prenom"=>$this->input->post('prenom'),
                "mdp"=>sha1($this->input->post('password')),
                "email"=>$this->input->post('email'),
                "numero"=>$this->input->post('numero'),
                "matricule"=>strtoupper($this->input->post('matricule')),
                "etat"=>1    
            );
            $this->DAOGen->create($ajout,null,null,"etudiant");

            $ajout=array(
                "idetudiant"=>$gesequence->sequence,
                "idniveau"=>$this->input->post('niveau'),
                "idparcour"=>$this->input->post('parcour'),
                "idanneeuniversitaire"=>$checkdate[0]->idanneeuniversitaire,
                "etat"=>1    
            );
            $this->DAOGen->create($ajout,null,null,"inscription");
            redirect('LoginController/');       
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
            $data['parcour']=$this->DAOGen->ReadGen("parcour",null);
            $this->load->view('formulaireetudiant',$data);

        }










    }

}

?>
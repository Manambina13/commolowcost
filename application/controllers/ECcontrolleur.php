<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\Jsend;
class ECcontrolleur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        $this->load->model("MetierEC");
        $this->load->library("pagination");
        $this->load->model('ExtensionsModel');
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function NotesExamenEC2($idexamen){
        $data['result']=$this->DAOGen->RequeteFull(" select examen.*, notesexamen.notes, utilisateur.nom, utilisateur.prenom FROM examen INNER JOIN notesexamen ON examen.idexamen = notesexamen.idexamen INNER JOIN utilisateur ON notesexamen.idutilisateur = utilisateur.idutilisateur where examen.idexamen=".$idexamen);
        $getec=$this->DAOGen->ReadGen("ecannee","where idecannee=".$data['result'][0]->idecannee);
        
        $data['idue']=$getec[0]->idec;
        
        $data['text']=$data['result'][0]->titre;
        $this->load->view('Prof/NotegenfinirEC',$data);

    }
    public function Notedevoir($iddevoir){
        $data['result']=$this->DAOGen->ReadGen("participantdevoircomplet","where iddevoir=".$iddevoir);
        $getnomdevoir=$this->DAOGen->ReadGen("devoir","where iddevoir=".$iddevoir);
        $data['idue']=$getnomdevoir[0]->idec;
        $data['text']=$getnomdevoir[0]->theme;
        $this->load->view('Prof/NotegenfinirEC',$data);


    }    
    public function Liste(){
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $this->load->view('admin/ListeUE',$data);
    }
    public function Recherche(){
        $where="where idclasse='".$this->input->post("choix")."'";
        $tableau=$this->DAOGen->ReadGen("Ue",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $data['result']=$tableau;
        $this->load->view('admin/Ueparclasse',$data);
    }
    public function ListeEcparUe($id){
          //  echo $id;
          $where="where idue='".$id."'";
          $tableau=$this->DAOGen->ReadGen("Ec",$where);
          $data['classe']=$this->DAOGen->ReadGen("classe",null);
          $data['result']=$tableau;
          $this->load->view('admin/EcparClasse.php',$data);
    }
    public function ListeNotes($idexamen){
          $data['result']=$this->DAOGen->RequeteFull("select notesexamen.*, utilisateur.nom, utilisateur.prenom FROM notesexamen INNER JOIN utilisateur ON notesexamen.idutilisateur = utilisateur.idutilisateur where idexamen=".$idexamen);
          $this->load->view('Prof/ListesNoteTravaux',$data);  
    }
    public function ListesNotesExamen($idexameninfo){
        $data['result']=$this->DAOGen->RequeteFull("select        utilisateur.nom, utilisateur.prenom, examensnec.* FROM utilisateur INNER JOIN examensnec ON utilisateur.idutilisateur = examensnec.idutilisateur where idexameninfo=".$idexameninfo);
        $this->load->view('Prof/ListesNoteTravaux',$data);  
    }
    public function supprimerauchoix($idmatiereAuchoix,$idec){
        $query=$this->db->query("update matiereauchoix SET idec=null where idmatiereauchoix=".$idmatiereAuchoix);
        redirect("ECcontrolleur/ListeChoice/".$idec);

    }
    public function ListeNoteRattrapage($idrattrapageinfo){
        $data['result']=$this->DAOGen->RequeteFull("select utilisateur.nom, utilisateur.prenom,rattrapagesnotesec.idrattrapagesnotesec, rattrapagesnotesec.idAnneeScolaire, rattrapagesnotesec.idUtilisateur, rattrapagesnotesec.idrattrapageinfo,rattrapagesnotesec.note as notes FROM utilisateur INNER JOIN rattrapagesnotesec ON utilisateur.idutilisateur = rattrapagesnotesec.idutilisateur where idrattrapageinfo=".$idrattrapageinfo);
        $this->load->view('Prof/ListesNoteTravaux',$data);  

      



    }
    public function DetailExamenEc($id,$idanneescolaire){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $test=$this->DAOGen->ReadGen("responsableec","where idec=".$id." and idutilisateur=".$u_rec_id);
        $data['submit']="";
        if(count($test)>0){
        $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
        }
        $annee=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
     //   echo var_dump($annee);
        $idecann=$this->DAOGen->ReadGen("ecannee","where idec='".$id."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$id."'");
      //  echo var_dump($checkchoiceEc);
        if($checkchoiceEc[0]->choix==0){    
        $data['result']=$this->DAOGen->ReadGen("detailexamen ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3");
     //   echo var_dump($data['result']);
        $data['idec']=$id;
        $data['idannee']=$idanneescolaire;
      //  $this->load->view('admin/ListeExamen.php',$data);
      $this->load->view('Prof/Newlistexamen2',$data);

    }else{
        $data['result']=$this->DAOGen->ReadGen("ListeExamenChoice ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etatExamen>=3 order by matiere asc");
   //     echo var_dump($data['result']);
    //    echo "where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3 order by matiere asc";
        $data['idec']=$id;
        $data['idannee']=$idanneescolaire;
      //  $this->load->view('admin/ListeExamenChoice',$data);
             $this->load->view('Prof/ListeExamenChoice',$data);  



        } 

    }
    public function VoirRattrapageEc($id,$idanneescolaire){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $test=$this->DAOGen->ReadGen("responsableec","where idec=".$id." and idutilisateur=".$u_rec_id);
        $data['submit']="";
        if(count($test)>0){
        $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
        }
        $annee=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
        
        $idecann=$this->DAOGen->ReadGen("ecannee","where idec='".$id."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$id."'");
      //  echo var_dump($checkchoiceEc);
        if($checkchoiceEc[0]->choix==0){    
        $data['result']=$this->DAOGen->ReadGen("rattrapageuser","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."'");
     //   echo var_dump($data['result']);
        $data['idec']=$id;
        $data['idannee']=$idanneescolaire;
      //  $this->load->view('admin/ListeExamen.php',$data);
      $this->load->view('Prof/NewlistexamenRattrapage',$data);

    }else{
  //      $data['result']=$this->DAOGen->ReadGen("ListeExamenChoice ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etatExamen>=3 order by matiere asc");
  $data['result']=$this->DAOGen->ReadGen("rattrapageuser","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."'"); 
  //     echo var_dump($data['result']);
    //    echo "where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3 order by matiere asc";
    $data['idannee']=$idanneescolaire;
        $data['idec']=$id;
        $this->load->view('Prof/NewlistexamenRattrapage',$data);
    
        //    $this->load->view('admin/ListeExamenChoice',$data);
        



        } 

    }
    public function checkchoicematiere(){
      
           // $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            $u_rec_id = $this->session->userdata('idutilisateur');
           // $lastInscription=$this->DAOGen->ReadGen("inscription","where idanneescolaire=".$anneescolaire[0]->etat." and idutilisateur=".$u_rec_id);    
           $ewault=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");   
         $getEC=$this->DAOGen->RequeteFull("select ec.*, ue.idclasse,ue.reference FROM ec INNER JOIN ue ON ec.idue = ue.idue where choix>0 and idclasse=".$ewault[0]->idclasse);
            $result=$this->DAOGen->ReadGen("choixinscription","where idinscription=".$ewault[0]->idinscription);
            if(count($getEC)==0){
                $employee_object = new stdClass; 
                $employee_object->value = 1; 
                $test =new Jsend ("success","Ok",$employee_object);   
                echo json_encode($test);   
            
            
            }else{
            
            if(count($result)>0){
                $employee_object = new stdClass; 
                $employee_object->value = 1; 
                $test =new Jsend ("success","Ok",$employee_object);   
                echo json_encode($test);   
                

            }else{
                $employee_object = new stdClass; 
                $employee_object->value = 0; 
                $test =new Jsend ("success","Ok",$employee_object);   
                echo json_encode($test);   


            }
        }







    }
    public function validerchoix(){
        try{
     //   $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $u_rec_id = $this->session->userdata('idutilisateur');
        
        $lastInscription=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");    
        //$lastInscription=$this->DAOGen->ReadGen("inscription","where idanneescolaire=".$anneescolaire[0]->etat." and idutilisateur=".$u_rec_id);    
        $getEC=$this->DAOGen->RequeteFull("select ec.*, ue.idclasse,ue.reference FROM ec INNER JOIN ue ON ec.idue = ue.idue where choix>0 and idclasse=".$lastInscription[0]->idclasse);
        $result=$this->DAOGen->ReadGen("choixinscription","where idinscription=".$lastInscription[0]->idinscription);
        if(count($getEC)==0){
            throw new \Exception("Il n'y a pas de matière facultative dans cette classe");
        }
        
        
        if(count($result)>0){
            throw new \Exception("Vous avez deja valide votre choix");
        }
        else{
            for($i=0;$i<count($getEC);$i++){
                $array=array(
                    "idinscription"=>$lastInscription[0]->idinscription,
                    "idmatiereauchoix"=>$this->input->post($getEC[$i]->idec)
                );
                $this->DAOGen->create($array,null,null,"choixinscription");
              //  echo var_dump($array);
            }
            $this->load->view('Etudiant/index');
            //aza afino le redirect
        }
    }catch(\Exception $e){
        echo $e->getMessage();



    }
    
    }
    public function ValidationRattrapage2($idanneescolaire){
        $idec=$this->input->post('idec');
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
        $detailec=$this->DAOGen->ReadGen("detailec","where idec=".$idec);
        try{
            if($anneescolaire[0]->etat==10){
                throw new \Exception("Cette année est déjà fini");
            }
            else if($anneescolaire[0]->etat==0){
                throw new \Exception("Cette année n'a pas encore commence");   
            }
        $this->MetierEC->NewSubmitRattrapage($idec,$detailec[0]->choix);
        $this->MetierEC->validationUERattrapage($detailec[0]->idue,$anneescolaire[0]->idanneescolaire);
        $this->MetierEC->FinirRattrapage($anneescolaire[0]->idanneescolaire,$detailec[0]->idclasse);
        redirect('UEController/ListeEcparUeExamenProf/'.$detailec[0]->idue);
        }catch(\Exception $e){
            $id=$this->input->post('idec');
           // echo $e->getMessage();
            $data['error']=$e->getMessage();
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $idecann=$this->DAOGen->ReadGen("ecannee","where idec='".$id."' and idanneescolaire='".$idanneescolaire."'");
        $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$id."'");
      //  echo var_dump($checkchoiceEc);
        if($checkchoiceEc[0]->choix==0){    
        $data['result']=$this->DAOGen->ReadGen("RattrapageUser","where idanneescolaire='".$idanneescolaire."' and idecannee='".$idecann[0]->idecannee."'");
     //   echo var_dump($data['result']);
        $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
        $data['idec']=$id;
      //  $this->load->view('admin/ListeExamen.php',$data);
      $data['idannee']=$idanneescolaire;
      $this->load->view('Prof/NewlistexamenRattrapage',$data);

    }else{
        $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
        $data['result']=$this->DAOGen->ReadGen("ListeExamenChoice ","where idanneescolaire='".$idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etatExamen>=3 order by matiere asc");
   //     echo var_dump($data['result']);
    //    echo "where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3 order by matiere asc";
        $data['idannee']=$idanneescolaire;
        $data['idec']=$id;
        $this->load->view('Prof/NewlistexamenRattrapage',$data);
        



        } 
        }
    }
    public function FinirEcSN(){





    }
    public function AjoutAvec($id){

        $data['result']=$id;
    //    echo var_dump($data);
            $this->load->view('admin/AjoutEc.php',$data);

    }
    public function Submit(){
        $nom=$this->input->post("nom");
        $idclasse=$this->input->post("idue");
        $choix=$this->input->post('choix');
        $x=array(
            "idue"=>$idclasse,
            "nom"=>$nom,
            "etat"=>1,
            "choix"=>$choix
        );
      //  $this->DAOGen->create($x,"idinscription","INS","inscription");
        $this->DAOGen->create($x,null,null,"ec");
        redirect('UEcontroller/ListeEcparUe/'.$idclasse);

    }
    public function GetParEC(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($_POST);
        $idue=$this->input->post('idue');
        $where="where idue='".$idue."'";
        $listeclasse=$this->DAOGen->ReadGen("EC",$where);
        $test =new Jsend ("success","Ok",$listeclasse);   
        echo json_encode($test);   
    }
    public function GetParECProf(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($_POST);
       $u_rec_id = $this->session->userdata('idutilisateur');
        $idue=$this->input->post('idue');
        $where="where idue='".$idue."' and idutilisateur=".$u_rec_id;
        $listeclasse=$this->DAOGen->ReadGen("detailec",$where);
        $test =new Jsend ("success","Ok",$listeclasse);   
        echo json_encode($test);   
    }
    public function deleteEC($id,$idue){
        $update=array(
            "etat"=>0
        );
        $id=array(
            "idec"=>$id
        );
        $this->DAOGen->update($update,$id,"ec");
        redirect('UEcontroller/ListeEcparUe/'.$idue);


    }
    public function fichemodication($id){
        $where="where idec='".$id."'";
        $data['result']=$this->DAOGen->ReadGen("ec",$where);
        $this->load->view('admin/fichemodicationEC',$data);

    }
    public function getListeEc(){
        $u_rec_id = $this->session->userdata('idutilisateur');
      //  echo $u_rec_id;
        $data['matiere']=$this->DAOGen->ReadGen("ec","where idutilisateur='".$u_rec_id."'");
        $this->load->view('Prof/courses',$data);
    }
    public function Detail($detail){
        $data['ec']=$this->DAOGen->ReadGen('detailec',"where idec='".$detail."'");
       // echo var_dump($data['ec']);
        $this->load->view('Prof/detailcours',$data);

    }
    public function NewDetail($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['ec']=$this->DAOGen->ReadGen('detailec',"where idclasse='".$idclasse."' and idutilisateur='".$u_rec_id."'");
       // echo var_dump($data['ec']);
       for($i=0;$i<count($data['ec']);$i++){
        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;

       }
        $this->load->view('Prof/Newdetailcours',$data);

    }
    public function getDetailEC($idec){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['ec']=$this->DAOGen->ReadGen('detailec',"where idutilisateur='".$u_rec_id."' and idec=".$idec);
        $resultEC=$this->DAOGen->ReadGen("support","where idec=".$idec." and idutilisateur=".$u_rec_id);
       // echo var_dump($data['ec']);
       $idfichier=array();
       $data['fichier']=[];
       for($i=0;$i<count($resultEC);$i++){
        array_push($idfichier,$resultEC[$i]->idfichier);
       }
       if(count($resultEC)==0){
           $data['fichier']=[];
       }else{
       $data['fichier']=$this->DAOGen->ReadGen("fichier","where ".$this->DAOGen->FonctiongetCreateOR($idfichier,"idfichier"));
       }
       for($i=0;$i<count($data['ec']);$i++){
        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;

       }
        $this->load->view('Prof/NewdetailcoursInfo',$data);
    }
    public function ajoutetatdevoir(){
        $u_rec_id = $this->session->userdata('idutilisateur');
    //    echo $this->input->post('iddevoir');
        
    
        try{
        if(isset($_FILES["userimage"]["name"]))  
        {
            //echo $_FILES["userimage"]["name"];
            $typeMessage=$this->ExtensionsModel->getExtension($_FILES["userimage"]["name"]);
        //    echo $typeMessage;
            $path=$this->ExtensionsModel->getPath($_FILES["userimage"]["name"]);
            echo $path;
            $config['upload_path']=$path;
            $config['allowed_types']='pdf|docx|zip';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload',$config);
      //    $this->upload->initialize($config);
     if(!$this->upload->do_upload('userimage'))  
     {  
     echo $this->upload->display_errors();  
     }  
     else  
     {  
     $data = array('upload_data' => $this->upload->data());
     $idfichier=$this->DAOGen->getSequence("idfichier","fichier");
     $title= $this->input->post('title');
     $image= $data['upload_data']['file_name']; 
     echo $image;
     $discussionforum=array(
        "idfichier"=>$idfichier->sequence,
        "idec"=>$this->input->post('idec'),
        "idutilisateur"=>$u_rec_id
    );
    
    $datafichier=array(
        "idfichier"=>$idfichier->sequence,
        "idtypemessage"=>$typeMessage[0]->idtypemessage,
        "nom"=>$_FILES["userimage"]["name"],
        "lien"=>$this->ExtensionsModel->reglagepath($path,$image)
    );
    $this->DAOGen->create($datafichier,null,null,"fichier");
    $this->DAOGen->create($discussionforum,null,null,"support");
    redirect('ECcontrolleur/getDetailEC/'.$this->input->post('idec'));
    }
    }else{
        $data['error']="Votre extension n'est pas possible";
     //   $u_rec_id = $this->session->userdata('idutilisateur');
      //  $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
       // $data['result']=$this->DAOGen->ReadGen("remisededevoirfiche","where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and  etat=1 and idclasse='".$this->input->post('idclasse')."' and eleve=".$u_rec_id);
      //  $data['idclasse']=$idclasse;
      //  $this->load->view('Etudiant/Listedevoir',$data);
        }
            }catch(Exception $e){
                $idec=$this->input->post('idec');
                $data['error']=$e->getMessage();
                $u_rec_id = $this->session->userdata('idutilisateur');
                $data['ec']=$this->DAOGen->ReadGen('detailec',"where idutilisateur='".$u_rec_id."' and idec=".$idec);
                $resultEC=$this->DAOGen->ReadGen("support","where idec=".$idec." and idutilisateur=".$u_rec_id);
                $idfichier=array();
                $data['fichier']=null;
                for($i=0;$i<count($resultEC);$i++){
                    array_push($idfichier,$resultEC[$i]->idfichier);
                }
                if(count($resultEC)==0){
                    $data['fichier']=null;
                }else{
                    $data['fichier']=$this->DAOGen->ReadGen("fichier","where ".$this->DAOGen->FonctiongetCreateOR($idfichier,"idfichier"));
                }
                    for($i=0;$i<count($data['ec']);$i++){
                        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;
                        }
                $this->load->view('Prof/NewdetailcoursInfo',$data);
            }
                }
    public function Rattrapageclasse($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['ec']=$this->DAOGen->ReadGen('detailec',"where idclasse='".$idclasse."' and idutilisateur='".$u_rec_id."'");
       // echo var_dump($data['ec']);
       for($i=0;$i<count($data['ec']);$i++){
        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;

       }
        $this->load->view('Prof/Newdetailrattrapage',$data);

    }
    public function ExamenparclasseListe($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['ec']=$this->DAOGen->ReadGen('detailec',"where idclasse='".$idclasse."' and idutilisateur='".$u_rec_id."'");
       // echo var_dump($data['ec']);
       $data['idclasse']=$data['ec'][0]->idclasse;
              for($i=0;$i<count($data['ec']);$i++){
        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;

       }
        $this->load->view('Prof/NewdetailExamen2',$data);

    }
    public function Listematiere($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['ec']=$this->DAOGen->RequeteFull("select distinct(idec),reference,position,classe,position,idclasse,nom from detailec where idclasse=".$idclasse);
       // echo var_dump($data['ec']);
       for($i=0;$i<count($data['ec']);$i++){
        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;

       }
        $this->load->view('Etudiant/Newdetailcours',$data);
    }
    public function Listesupport($idec){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['ec']=$this->DAOGen->ReadGen('detailec',"where  idec=".$idec);
        $resultEC=$this->DAOGen->ReadGen("support","where idec=".$idec);
       $idfichier=array();
       $data['fichier']=null;
       for($i=0;$i<count($resultEC);$i++){
        array_push($idfichier,$resultEC[$i]->idfichier);
       }
       if(count($resultEC)==0){
           $data['fichier']=null;
       }else{
       $data['fichier']=$this->DAOGen->ReadGen("fichier","where ".$this->DAOGen->FonctiongetCreateOR($idfichier,"idfichier"));
       }
       for($i=0;$i<count($data['ec']);$i++){
        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;

       }
        $this->load->view('Etudiant/NewdetailcoursInfo',$data);
    }
    public function getclasseResponsablilter(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $result=$this->DAOGen->RequeteFull("select distinct(idclasse),classe from responsableecfull where idutilisateur=".$u_rec_id);
        $test =new Jsend ("success","Ok",$result);   
        echo json_encode($test);
    }
    public function getecparclasse($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['result']=$this->DAOGen->ReadGen("responsableecfull"," where idclasse=".$idclasse." and idutilisateur=".$u_rec_id);
        for($i=0;$i<count($data['result']);$i++){
            $data['result'][$i]->position="UE". $data['result'][$i]->position;
        } 
        $this->load->view('Prof/Responsablec',$data);
    }

    public function SituationECWithoutRecherche($idec){
        // $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        // $data["idue"]=$id;
        // $where="where idue='".$id."' and etat=1";
       //  $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
       $anneescolaire=$this->DAOGen->ReadGen("anneescolaire",null);
       for($i=0;$i<count($anneescolaire);$i++){
         $anneescolaire[$i]->debut=$anneescolaire[$i]->debut."-".$anneescolaire[$i]->fin;
         }
         $data['anneescolaire']=$anneescolaire;
         $tableau=$this->DAOGen->ReadGen("detailecsansprof","where idec=".$idec);
         $data['classe']=[];
         $data['result']=[];
         $data['id']=$idec;
         $data['idannee']='';
       
      //   $this->load->view('Prof/EcparClasseExamenRes.php',$data);
      $this->load->view('Prof/Ecparesponsable.php',$data);
 
 
 
 
 
     }
    public function SituationEC($idec){
       // $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
       // $data["idue"]=$id;
       // $where="where idue='".$id."' and etat=1";
      //  $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
      $anneescolaire=[];
      if($this->input->post('anneescolaire')==null){
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire",null);
      }
      else{
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$this->input->post('anneescolaire'));
      }  
      for($i=0;$i<count($anneescolaire);$i++){
        $anneescolaire[$i]->debut=$anneescolaire[$i]->debut."-".$anneescolaire[$i]->fin;
        }
        $data['anneescolaire']=$anneescolaire;
        $tableau=$this->DAOGen->ReadGen("detailecsansprof","where idec=".$idec);
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $listerattrapageec=$this->DAOGen->ReadGen("rattrapageecannee","where idanneescolaire=".$anneescolaire[0]->idanneescolaire);
        for($i=0;$i<count($tableau);$i++){      
            $tableau2=$this->DAOGen->ReadGen("situationexamen","where idec=".$tableau[$i]->idec." and idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and (etat=5 or etat=10)");
          //  echo "where idec=".$tableau[$i]->idec." and idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and etat=5 or etat=3";
          
          $id=$tableau[$i]->idec;
          $count=array_filter($listerattrapageec,function($a) use ($id){return $a->idec==$id and $a->etat>0;});
           if(count($count)>0){
            $tableau[$i]->etat="<i data-feather='check-circle'></i>";
    
           }
           else{
            //
            $tableau[$i]->etat="<i data-feather='x-circle'></i>";
            // $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
          //   $tableau[$i]->etat="<a href='".base_url()."ECcontrolleur/VoirRattrapageEc/".$tableau[$i]->idec."'><p style='color:blue;'><i data-feather='x-circle'></i></p></a>";
          $tableau[$i]->choix="<i data-feather='x-circle'></i>";
            }
         
          if(count($tableau2)>0){
           //     $tableau[$i]->choix="<i class='fa fa-check-circle' aria-hidden='true'></i>";
           $tableau[$i]->choix="<i data-feather='check-circle'></i>";
           
            }
              else{
               // $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
          //      $tableau[$i]->choix="<a href='".base_url()."/ECcontrolleur/DetailExamenEc/".$tableau[$i]->idec."'><p style='color:blue;'><i data-feather='x-circle'></i></p></a>";
          $tableau[$i]->choix="<i data-feather='x-circle'></i>";
            }
            $recherche=$this->DAOGen->ReadGen("ecanneesn","where idanneescolaire='".$anneescolaire[0]->idanneescolaire."' and idec='".$idec."' and (etat is not null or etat>0)");
            if(count($recherche)>0){
                $tableau[$i]->idclasse="<i data-feather='check-circle'></i>";

            }else{
                $tableau[$i]->idclasse="<i data-feather='x-circle'></i>";

            }
            
            
            /*
            
            if($tableau[$i]->choix==1){
              $tableau[$i]->choix="<a href=http://localhost/CommoStagiairemysql/ECcontrolleur/ListeChoice/".$tableau[$i]->idec."   ?'><p style='color:blue;'> <i class='fa fa-check-circle' aria-hidden='true'></i></p></a>";
            }
            else{
              $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
            }
            */
        
        }
      //  echo var_dump($tableau);
        $data['idannee']=$anneescolaire[0]->idanneescolaire;
        $data['result']=$tableau;
        $data['id']=$idec;
        
      
     //   $this->load->view('Prof/EcparClasseExamenRes.php',$data);
     $this->load->view('Prof/Ecparesponsable.php',$data);





    }
    public function newsubmiRattrapage(){
            try{
            $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            $idec=$this->input->post("idec");
            $choice=$this->input->post("choice");
            $idclasse=$this->DAOGen->ReadGen("detailec","where idec=".$idec);
            $this->MetierEC->validationnotesrattrapage($idec,$choice);
            redirect('ECcontrolleur/Rattrapageclasse/'.$idclasse[0]->idclasse);
            }catch(\Exception $e){
                 $e->getMessage();
                 $idec=$this->input->post("idec");
                 $data['error']=$e->getMessage();
                 $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
                 $getidue=$this->DAOGen->ReadGen("detailec","where idec=".$idec);
                 $getueannee=$this->DAOGen->ReadGen("ueannee","where idue=".$getidue[0]->idue." and idanneescolaire=".$anneescolaire[0]->idanneescolaire);
                 $resultat=$this->DAOGen->ReadGen("rattrapageueannee","where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and idueannee=".$getueannee[0]->idueannee);
                 if($getidue[0]->choix==0){
                 $utilisateur=$this->DAOGen->ReadGen("listeutilisateurattrapage","where idueannee=".$getueannee[0]->idueannee);
                
                 $data['idec']=$idec;
                 $data['listeEleve']=$utilisateur;
                 $data['ec']=$idec;
                 $data['choice']=null;
                 $this->load->view("Prof/ListeRattrapage.php",$data);
                 }else{
                     $u_rec_id = $this->session->userdata('idutilisateur');
                     $getChoice=$this->DAOGen->ReadGen("examenmembrechoix","where idutilisateur=".$u_rec_id);
                    $utilisateur=$this->DAOGen->ReadGen("listeutilisateurattrapage","where idueannee=".$getueannee[0]->idueannee);
                     $utilisateurraary=array();
                     for($i=0;$i<count($utilisateur);$i++){
                         array_push($utilisateurraary,$utilisateur[$i]->idutilisateur);
                     }
                     $listeEleve=array();
                     if(count($utilisateurraary)>0){
                     $listeEleve=$this->DAOGen->ReadGen("detailinscriptionchoix","where idanneescolaire='".$anneescolaire[0]->idanneescolaire."' and  idmatiereauchoix='".$getChoice[0]->idmatiereauchoix."' and (".$this->DAOGen->FonctiongetCreateOR($utilisateurraary,"idutilisateur").")");
                     }
                     $data['choice']=$getChoice[0]->idmatiereauchoix;
                     $data['idec']=$idec;
                     $data['listeEleve']=$listeEleve;
                     $data['ec']=$idec;
                     $this->load->view("Prof/ListeRattrapage.php",$data); 
                 }
            }            
    }
    public function listeutilisateurattrapage($idec){
        try{
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        if(count($anneescolaire)==0){
            throw new \Exception("L'année n'a pas encore commencé");
        }
        $getidue=$this->DAOGen->ReadGen("detailec","where idec=".$idec);
        $getueannee=$this->DAOGen->ReadGen("ueannee","where idue=".$getidue[0]->idue." and idanneescolaire=".$anneescolaire[0]->idanneescolaire);
        $resultat=$this->DAOGen->ReadGen("rattrapageueannee","where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and idueannee=".$getueannee[0]->idueannee);
        if($getidue[0]->choix==0){
        $utilisateur=$this->DAOGen->ReadGen("listeutilisateurattrapage","where idueannee=".$getueannee[0]->idueannee);
       
        $data['idec']=$idec;
        $data['listeEleve']=$utilisateur;
        $data['ec']=$idec;
        $data['choice']=null;
        $this->load->view("Prof/ListeRattrapage.php",$data);
        }else{
            if($resultat[0]->etat==0){
                throw new \Exception("Le rattrapage n'a pas encore commencé");
            }
            $u_rec_id = $this->session->userdata('idutilisateur');
            $getChoice=$this->DAOGen->ReadGen("examenmembrechoix","where idutilisateur=".$u_rec_id);
           $utilisateur=$this->DAOGen->ReadGen("listeutilisateurattrapage","where idueannee=".$getueannee[0]->idueannee);
           $utilisateurraary=array();
            for($i=0;$i<count($utilisateur);$i++){
                array_push($utilisateurraary,$utilisateur[$i]->idutilisateur);
            }
            $listeEleve=array();
            if(count($utilisateur)>0){
                 $listeEleve=$this->DAOGen->ReadGen("detailinscriptionchoix","where idanneescolaire='".$anneescolaire[0]->idanneescolaire."' and  idmatiereauchoix='".$getChoice[0]->idmatiereauchoix."' and (".$this->DAOGen->FonctiongetCreateOR($utilisateurraary,"idutilisateur").")");
            }
            //$listeEleve=$this->DAOGen->ReadGen("choixlvrattrapage","where idanneescolaire='".$anneescolaire[0]->idanneescolaire."' and  idmatiereauchoix='".$getChoice[0]->idmatiereauchoix."'");
           
            $data['choice']=$getChoice[0]->idmatiereauchoix;
            $data['idec']=$idec;
            $data['listeEleve']=$listeEleve;
            $data['ec']=$idec;
            $this->load->view("Prof/ListeRattrapage.php",$data);

        }
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $u_rec_id = $this->session->userdata('idutilisateur');
            $getClasse=$this->DAOGen->ReadGen('detailec',"where idec=".$idec);
            $u_rec_id = $this->session->userdata('idutilisateur');

            $data['ec']=$this->DAOGen->ReadGen('detailec',"where idclasse='".$getClasse[0]->idclasse."' and idutilisateur='".$u_rec_id."'");
           // echo var_dump($data['ec']);
        //     $this->load->view('Prof/Newdetailcours',$data);
        for($i=0;$i<count($data['ec']);$i++){
            $data['ec'][$i]->position="UE".$data['ec'][$i]->position;
    
           }
        
        $this->load->view('Prof/Newdetailrattrapage',$data);
    }



    }
    public function getDevoirEc(){
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $idutilisateur = $this->session->userdata('idutilisateur');
        $data['devoir']=$this->DAOGen->ReadGen("devoir","where idutilisateur='".$idutilisateur."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
    //    echo var_dump($data['devoir']);
        $this->load->view('Prof/Devoir',$data);

    }
    public function ECexamen($idec){
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $ec=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
        $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$ec[0]->idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        try{
        $this->load->model('MetierEC');   
        $this->MetierEC->getEcAnnee($idec);
        if($ue[0]->etat==0){
            throw new \Excpetion("Ue n'a pas encore commencer");
        }
        else if($ue[0]->etat==5){
            throw new \Exception("Ue deja Fini");
        }
        else if($ue[0]->etat==1){
            $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $data['idec']=$idec;
            $data['listeEleve']=$listeEleve;
            $this->load->view('Prof/CreateExamen',$data);
        }
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['ec']=$this->DAOGen->ReadGen('detailec',"where idec='".$idec."'");            // echo var_dump($data['ec']);
             $this->load->view('Prof/detailcours',$data);
            

        }
    }
    public function validationfinalExamen(){
        try{
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $idue=$this->input->post("idue");
        $choice=$this->input->post("choix");
        $this->MetierEC->validationUESN($idue,$anneescolaire[0]->idanneescolaire,$choice);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
    public function NewECexamen($idec){
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $ec=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
        $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$ec[0]->idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        try{
        $this->load->model('MetierEC');   
        $this->MetierEC->getEcAnnee($idec);
        if($ue[0]->etat==0){
            throw new \Excpetion("Ue n'a pas encore commencer");
        }
        else if($ue[0]->etat==5){
            throw new \Exception("Ue deja Fini");
        }
        else if($ue[0]->etat==1){
            $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $now = date_create()->format('Y-m-d H:i:s');
            $data['idec']=$idec;
            $data['listeEleve']=$listeEleve;
            $data['idec']=$idec;
            $data['date']=$now; 
            $data['name']="";
            $this->load->view('Prof/NewEleveExamen',$data);
        }
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['ec']=$this->DAOGen->ReadGen('detailec',"where idec='".$idec."'");            // echo var_dump($data['ec']);
             $this->load->view('Prof/detailcours',$data);
            

        }
    }
    //
    public function AddExamen($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
  //      $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $data['listeEc']=$this->DAOGen->ReadGen("detailec","where idclasse='".$idclasse."' and idutilisateur='".$u_rec_id."'");
   //     $data['listeEleve']=$listeEleve;
        $this->load->view('Prof/NewCreateExamen',$data);
        
    }
    public function StepSubmitExamenone(){
        try{
            $this->load->model('MetierEC');   
            $date=$this->input->post('date');  
            $idec=$this->input->post('idec');  
            $texte=$this->input->post('name');
            $this->MetierEC->Controle($texte,$date);
            //
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            $ec=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
            $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$ec[0]->idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $this->load->model('MetierEC');   
            $this->MetierEC->getEcAnnee($idec);
            if($ue[0]->etat==0){
                throw new \Exception("Ue n'a pas encore commencer");
            }
            else if($ue[0]->etat==5){
                throw new \Exception("Ue deja Fini");
            }
            else if($ue[0]->etat==1){
                if($ec[0]->choix==0)
                {
                    $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
              //  $data['idec']=$idec;
                    $data['listeEleve']=$listeEleve;
                    $data['idec']=$idec;
                    $data['date']=$date;
                    $data['name']=$texte;
                    $data['idchoice']=$this->input->post('idchoice');
                    $this->load->view('Prof/NewEleveExamen',$data);
                }else{

                    $listeEleve=$this->DAOGen->ReadGen("DetailInscriptionChoix","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."' and  idmatiereauchoix='".$this->input->post('idchoice')."'");
                    //  $data['idec']=$idec;
                //    echo "where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."' and  idmatiereauchoix='".$this->input->post('idchoice')."'";    
           //         echo var_dump($this->input->post());
                          $data['listeEleve']=$listeEleve;
                          $data['idec']=$idec;
                          $data['date']=$date;
                          $data['idchoice']=$this->input->post('idchoice');
                          $data['name']=$texte;
                          $this->load->view('Prof/NewEleveExamen',$data);
                          




                }
            }


        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $idec=$this->input->post('idec');  
            $ec=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
            $u_rec_id = $this->session->userdata('idutilisateur');
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
          //  echo var_dump($ec);
            $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
           // echo var_dump($ec);
            $data['listeEc']=$this->DAOGen->ReadGen("detailec","where idclasse='".$ec[0]->idclasse."' and idutilisateur='".$u_rec_id."'");
            $data['listeEleve']=$listeEleve;
               $this->load->view('Prof/NewCreateExamen',$data);

        }
    }
    public function newsubmitExamen(){
    //    $now = date_create()->format('Y-m-d H:i:s');
    //    $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
    $idec=$this->input->post('idec');
    $date=$this->input->post('date');  
    $titre=$this->input->post('titre');
    $this->load->model('MetierEC');
        try{
        $calcul=$this->MetierEC->Examen($this->input->post('idec'),$this->input->post('date'),$this->input->post('idchoice'),$this->input->post('titre'));
        $result= $this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
           // echo var_dump($res[0]->idclasse);
            redirect('ECcontrolleur/Examenparclasse/'.$result[0]->idclasse);
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
           
            $data['listeEleve']=$this->MetierEC->GetNombreEleve($this->input->post('idec'));
            $data['idec']=$this->input->post('idec');
            $data['date']=$date;
            $data['texte']=$this->input->post('titre');
            $this->load->view('Prof/CreateExamen',$data);
            

        }
    }
    public function NotesExamenEC(){
        $now = date_create()->format('Y-m-d H:i:s');
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $this->load->model('MetierEC');
        try{
        $calcul=$this->MetierEC->Examen($this->input->post('idec'),$this->input->post('date'));
        redirect('ECcontrolleur/getListeEc');
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['listeEleve']=$this->MetierEC->GetNombreEleve($this->input->post('idec'));
            $data['idec']=$this->input->post('idec');
            $this->load->view('Prof/CreateExamen',$data);

        }
    }
    public function AfficherListeBox($ec){
        
        try{
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            if(count($annee)==0){
                throw new \Exception("L'année n'a pas encore commencé");
                
                
            }
        $u_rec_id = $this->session->userdata('idutilisateur');
        $this->load->model('MetierEC');
        $this->MetierEC->getEcAnnee($ec);       
        $this->MetierEC->controleValidationNormale($ec,$annee[0]->idanneescolaire,$u_rec_id);
        $eca=$this->DAOGen->ReadGen("detailec","where idec='".$ec."'");
        $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$eca[0]->idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$eca[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $ecannee=$this->DAOGen->ReadGen("ecannee","where idue='".$eca[0]->idue."' and idec='".$ec."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $data['examen']=$this->DAOGen->ReadGen("ListeExamen","where idecannee='".$ecannee[0]->idecannee."' and idutilisateur='".$u_rec_id."'");
        $data['ec']=$ec;
        $data['devoir']=$this->DAOGen->ReadGen("devoir","where idec=".$ec." and idutilisateur=".$u_rec_id." and etat=10 and idanneescolaire=".$annee[0]->idanneescolaire);
        $this->load->view('Prof/FinirEc',$data);
        }catch(\Exception $e){
            $data['error']=$e->getMessage(); 
            $eca=$this->DAOGen->ReadGen("detailec","where idec='".$ec."'");
             $u_rec_id = $this->session->userdata('idutilisateur');
             $data['ec']=$this->DAOGen->ReadGen('detailec',"where idclasse='".$eca[0]->idclasse."' and idutilisateur='".$u_rec_id."'");
       // echo var_dump($data['ec']);
       for($i=0;$i<count($data['ec']);$i++){
        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;

       }
           $this->load->view('Prof/Newdetailcours',$data);

         //   $data['error']=$e->getMessage();
        //    $data['ec']=$this->DAOGen->ReadGen('detailec',"where idec='".$ec."'");            // echo var_dump($data['ec']);
        //     $this->load->view('Prof/detailcours',$data);



        }
    }
    public function FinirEC(){
      // echo var_dump($this->input->post('Examen'));
       // "select sum(valeur),idutilisateur from notesExamen where idexamen='EX1' or idexamen='EX2' group by idutilisateur";
       try{
        $u_rec_id = $this->session->userdata('idutilisateur');
       $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
       //$examen= $this->input->post('Examen');
       $examen=$this->MetierEC->controlecaseachoher($this->input->post('Devoir'),$this->input->post('Examen'),$this->input->post('idec'),$annee[0]->idanneescolaire,$u_rec_id);
       $this->MetierEC->controleValidationNormale($this->input->post("idec"),$annee[0]->idanneescolaire,$u_rec_id);
      
       
       $requete=$this->MetierEC->AccesRequete($examen);
       $verificationEC=$this->DAOGen->ReadGen("Examen","where idexamen='".$examen[0]."'");
       if($verificationEC[0]->idmatiereauchoix!=null){
       $this->MetierEC->getEcAnnee($this->input->post("idec"));
      //  echo var_dump($examen);
        $examenss = $this->DAOGen->ReadGen("examen","where idexamen='".$examen[0]."'");
     //   $idue=$this->DAOGen->ReadGen("ecannee","where idueannee='".$examenss[0]->idecannee."'");
        /*
        for($i=0;$i<count($requete);$i++){
            $notes=$requete[$i]->sum/count($examen);
            $data=array(
                "idecannee"=>$examenss[0]->idecannee,
                "idueannee"=>$examenss[0]->idueannee,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idutilisateur"=>$requete[0]->idutilisateur,
                "notes"=>$notes
            );
            $this->DAOGen->create($data,"idNotes","NO","notes");
        }
        */
            $data=array(
                "etat"=>3
            );
            $id=array(
                "idecannee"=>$examenss[0]->idecannee
            );

            for($i=0;$i<count($examen);$i++){
                $insert=array(
                        "idexamen"=>$examen[$i]
                );
        //    $this->DAOGen->create($insert,"idvalidationexam","VAD","validationexam");
        $this->DAOGen->update($data,$insert,"examen");
        }
            $this->DAOGen->update($data,$id,"ecannee");
            $where="where idec='".$this->input->post("idec")."'";
            $data['ec']=$this->DAOGen->ReadGen('detailec',"where idec='".$this->input->post("idec")."'");            // echo var_dump($data['ec']);
        //     $this->load->view('Prof/Newdetailcours',$data);
        redirect('ECcontrolleur/NewDetail/'.$data['ec'][0]->idclasse);
        }else{
            $examenss = $this->DAOGen->ReadGen("examen","where idexamen='".$examen[0]."'");
            $data=array(
                "etat"=>3
            );
            $id=array(
                "idecannee"=>$examenss[0]->idecannee
            );
            $this->DAOGen->update($data,$id,"ecannee");
          for($i=0;$i<count($examen);$i++){
                $insert=array(
                        "idexamen"=>$examen[$i]
                );
            $this->DAOGen->update($data,$insert,"examen");

          }            
          $data['ec']=$this->DAOGen->ReadGen('detailec',"where idec='".$this->input->post("idec")."'");            // 
      //    $this->db->set($data); 
       //   $this->db->where_in('idexamen',$examen); 
        //  $this->db->update("examen", $data);
      //    $this->load->view('Prof/Newdetailcours',$data);
          redirect('ECcontrolleur/NewDetail/'.$data['ec'][0]->idclasse);
        }
        }catch(\Exception $e){

            $u_rec_id = $this->session->userdata('idutilisateur');
         $ec=$this->input->post("idec");
        $data['ec']=$ec;
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $eca=$this->DAOGen->ReadGen("detailec","where idec='".$ec."'");
        $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$eca[0]->idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$eca[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $ecannee=$this->DAOGen->ReadGen("ecannee","where idue='".$ue[0]->idue."' and idec='".$ec."'");
        $data['examen']=$this->DAOGen->ReadGen("examen","where idecannee='".$ecannee[0]->idecannee."'");
        $data['error']=$e->getMessage();
        $data['devoir']=$this->DAOGen->ReadGen("devoir","where idec=".$ec." and idutilisateur=".$u_rec_id." and etat=10 and idanneescolaire=".$annee[0]->idanneescolaire);
        $this->load->view('Prof/FinirEc',$data);

        }
    }
    public function Examenparclasse($idclasse){
       /*
        $u_rec_id = $this->session->userdata('idutilisateur');
        $result=$this->DAOGen->ReadGen("ListeExamen","where idclasse='".$idclasse."' and idutilisateur='".$u_rec_id."'");
        for($i=0; $i<count($result);$i++){
            $result[$i]->debut=$result[$i]->debut."-".$result[$i]->debut;
        }
        $data['result']=$result;
        $data['idclasse']=$idclasse;
        $this->load->view("Prof/NewListeExamen",$data);
        */
        $u_rec_id = $this->session->userdata('idutilisateur');
        
       
        $this->load->model('MetierExamen');

        $config = array();
        $config["base_url"] = base_url() . "ECcontrolleur/Examenparclasse/".$idclasse;
        $config["total_rows"] = $this->MetierExamen->getCount($u_rec_id,$idclasse);
        $config["per_page"] = 5;
        $config["uri_segment"] = 4;
       // echo  var_dump($this->uri->segment(3));
        $this->pagination->initialize($config);
       

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data["links"] = $this->pagination->create_links();
        $result=$this->MetierExamen->get_Examen($idclasse,$u_rec_id,$config["per_page"], $page);
        $data['idclasse']=$idclasse;
        $data['result'] = $result;
    //   $this->load->view('authors/index', $data);
   // echo var_dump($this->uri->segment(4)); 
   //echo var_dump($data['result']);
     $this->load->view('Prof/NewListeExamen',$data);







    }
    public function FicheExamen($idexamen){
        $data['result']=$this->DAOGen->ReadGen("notexameneleve","where idexamen=".$idexamen);
        $getClasse=$this->DAOGen->ReadGen("listeExamen","where idexamen=".$idexamen);
        $data['idclasse']=$getClasse[0]->idclasse;
        $this->load->view('Prof/Listenote',$data);





    }
    public function Submitmodification($idec,$idue){
        $str = $this->input->post('nom');
        $pattern = "/[a-zA-Z]{1,}/";
       
        $x=preg_match($pattern, $str);
        if($x==0){
            $where="where idec='".$idec."'";
            $data['error']="Veuillez taper  nom approprier";
            $data['result']=$this->DAOGen->ReadGen("ec",$where);
            $this->load->view('admin/fichemodicationEC',$data);
        }
        else {
        $update=array(
            "nom"=>$this->input->post('nom')
        );
        $id=array(
            "idec"=>$this->input->post('idec')
        );
        $this->DAOGen->update($update,$id,"ec");
        redirect('UEcontroller/ListeEcparUe/'.$idue);
    }
    }
    ///Change
    public function DeleteProfEC($idec,$idprof){
        $listeEC=$this->DAOGen->ReadGen("detailec","where idec=".$idec);
        if($listeEC[0]->choix>0){
            $matiereauchoix=$this->DAOGen->ReadGen("matiereauchoix","where idec=".$idec." and idprof=".$idprof);
            $update=array(
                "idec"=>null
            );   
            $where=array(
                "idprof"=>$idprof
            );
            $update2=array(
               "idprof"=>null
            );
            $where2=array(
                "idmatiereauchoix"=>$matiereauchoix[0]->idmatiereauchoix
            );
            $this->DAOGen->update($update,$where,"prof");
            $this->DAOGen->update($update2,$where2,"matiereauchoix");
            redirect('ECcontrolleur/ListeProfEc/'.$idec);
            


        }else{
            $update=array(
                "idec"=>null
            );   
            $where=array(
                "idprof"=>$idprof
            );
            $this->DAOGen->update($update,$where,"prof");
            redirect('ECcontrolleur/ListeProfEc/'.$idec);
        }



    }
    public function ListeProfEc($idec){
        $result['prof']=$this->DAOGen->ReadGen("profparec","where idec='".$idec."'");
        $result['idec']=$idec;
        $this->load->view('admin/ProfParEC',$result);
    }
    public function AjouterProf($idec){
            $result['idec']=$idec;
            $this->load->view('admin/AjoutProfEc',$result);
    }
    public function ListeChoice($idec){
        $data['idec']=$idec;
        $result=$this->DAOGen->ReadGen("matiereauchoix","where idec='".$idec."'");
        for($i=0;$i<count($result);$i++){
            if($result[$i]->idprof==null){
                $result[$i]->idprof="<p style='color:#1a75ff;'> <i class='fa fa-times-circle' aria-hidden='true'></i></p>";
            }
            else{

                $result[$i]->idprof="<p style='color:#1a75ff;'><i class='fa fa-check-circle' aria-hidden='true'></i></p>";
            }

        }
        $data['result']=$result;
        $this->load->view('admin/ListeChoice',$data);






    }
    public function AjoutProfEcc(){
     //   echo var_dump($this->input->post());
        if($this->input->post('city')==null){
            $result['idec']=$this->input->post('idec');
            $result['error']="Veuillez completer le Nom";
            $this->load->view('admin/AjoutProfEc',$result);


        }else{
            $str=$this->input->post('city');
            $valeur=explode(" ",$str);
            try{
            if(!isset($valeur[1])){
                throw new \Exception("Ce formateur n'existe pas dans le staff");

            }
            $idprof=$this->DAOGen->ReadGen("utilisateur","where etat=1 and nom='".trim($valeur[0])."' and prenom='".trim($valeur[1])."' and idprofile='3'");
            
            //echo var_dump($idprof);
            $data=array(
                "idec"=>$this->input->post('idec'),
                "idutilisateur"=>$idprof[0]->idutilisateur
            );
			$ec=$this->DAOGen->ReadGen("ec","where idec=".$this->input->post('idec'));
			if($ec[0]->choix==1){
				throw new Exception('Cette ec a des matiere facultatif donc pour inserer des formateur veuillez cliquer le logo au choix');
			}
            $this->DAOGen->create($data,null,null,"prof");
            redirect("ECcontrolleur/ListeProfEc/".$this->input->post('idec'));
            }catch(\Exception  $e){
                $result['idec']=$this->input->post('idec');
                $result['error']=$e->getMessage();
                $this->load->view('admin/AjoutProfEc',$result);

            }
        }
    }
    public function ListeResponsable($idec){
        $data['result']=$this->DAOGen->RequeteFull("select responsableec.*, utilisateur.nom, utilisateur.prenom, utilisateur.email FROM responsableec INNER JOIN utilisateur ON responsableec.idutilisateur = utilisateur.idutilisateur where idec=".$idec);
        $data['idec']=$idec;
        $this->load->view('admin/ListeResponsableEC',$data);
    }
    public function AjouterResponsable($idue){
        $result['idue']=$idue;
        $result['cible']=base_url('ECcontrolleur/AjoutProfECRES');
        $this->load->view('admin/AjoutResEC',$result); 
    }
    public function PrecedentEC($id){
        //  echo $id;
        $getIdue=$this->DAOGen->ReadGen("ec","where idec=".$id);
        $where="where idue='".$getIdue[0]->idue."' and etat=1";
        $tableau=$this->DAOGen->ReadGen("ec",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");
        for($i=0;$i<count($tableau);$i++){
            if($tableau[$i]->choix==1){
              $tableau[$i]->choix="<a href='".base_url()."ECcontrolleur/ListeChoice/".$tableau[$i]->idec."'   ?'><p style='color:blue;'> <i class='fa fa-check-circle' aria-hidden='true'></i></p></a>";
            }
            else{
              $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
            }
        }
        
        $data['result']=$tableau;
        $data['id']=$id;
      
        $this->load->view('admin/EcparClasse.php',$data);
  }
    public function AjoutProfECRES(){
        if($this->input->post('city')==null){
            $result['idue']=$this->input->post('idue');
            $result['cible']=base_url('ECcontrolleur/AjoutProfECRES');
            $result['error']="Veuillez completer le Nom";
            $this->load->view('admin/AjoutResUE',$result);


        }else{
            $str=$this->input->post('city');
            $valeur=explode(" ",$str);
            try{
            if(!isset($valeur[1])){
                throw new \Exception("Ce formateur n'existe pas dans le staff");
            }
            $idprof=$this->DAOGen->ReadGen("utilisateur","where etat=1 and nom='".trim($valeur[0])."' and prenom='".trim($valeur[1])."' and idprofile='3'");
            $exception=$this->DAOGen->ReadGen("responsableec","where idutilisateur=".$idprof[0]->idutilisateur." and idec=".$this->input->post('idue'));
            if(count($exception)>0){
                $result['idue']=$this->input->post('idue');
                $result['cible']=base_url('ECcontrolleur/AjoutProfECRES');
                $result['error']="Ce prof est déjà responsable de cette EC";
                $this->load->view('admin/AjoutResUE',$result);


            }else{
            //echo var_dump($idprof);
            $data=array(
                 "idec"=>$this->input->post('idue'),
                "idutilisateur"=>$idprof[0]->idutilisateur
            );
            $this->DAOGen->create($data,null,null,"responsableec");
            redirect("ECcontrolleur/ListeResponsable/".$this->input->post('idue'));
            }
        }catch(\Exception $e){

            $result['idue']=$this->input->post('idue');
            $result['cible']=base_url('ECcontrolleur/AjoutProfECRES');
            $result['error']=$e->getMessage();
            $this->load->view('admin/AjoutResUE',$result);
    
        }
    }


    } 
    public function Supprimeresponsable($idresponsableEC){
        $result=$this->DAOGen->ReadGen("responsableec","where idresponsableec=".$idresponsableEC);
        $this->db->query("delete  from responsableec where idresponsableec='".$idresponsableEC."'");
        $data['result']=$this->DAOGen->RequeteFull("select utilisateur.nom, utilisateur.prenom,utilisateur.email, responsableec.idec, responsableec.idresponsableec FROM utilisateur INNER JOIN responsableec ON utilisateur.idutilisateur = responsableec.idutilisateur where idec=".$result[0]->idec);
        $data['idec']=$result[0]->idec;
        $this->load->view('admin/ListeResponsableEC',$data);
    }
    public function AjoutChoice($idec){
        $data['idec']=$idec;
        $this->load->view("admin/Createchoice",$data);
    }
    public function AssignerChoice($id){
        $result=$this->DAOGen->ReadGen("matiereauchoix","where idmatiereauchoix='".$id."'");
        $data['result']=$result;
        $this->load->view('admin/AssignerChoice',$data);
    }
    public function ChoiceSubmit(){ 
        $str = $this->input->post('nom');
		$pattern = "/[a-zA-Z]{1,}/";
		$x=preg_match($pattern, $str);
		if($x==0){
             $data['error']="Veuillez entre un nom Exacte";
             $data['idec']=$this->input->post('idec');
             $this->load->view("admin/Createchoice",$data);
        }
        else{
            $data=array(
                "idec"=>$this->input->post('idec'),
                "matiere"=>$this->input->post('nom') 
            );
            $this->DAOGen->create($data,null,null,"matiereauchoix");
            redirect("ECcontrolleur/ListeChoice/".$this->input->post('idec'));
        }
    }
    public function ModifierChoice($idchoice){
        $data['result']=$this->DAOGen->ReadGen("matiereauchoix","where idmatiereauchoix='".$idchoice."'");
        $this->load->view('admin/ModifierChoice',$data);




    }
    public function Modifysubmit(){
       $idmatiereAuChoix=$this->input->post('idmatiereauchoix');
       if($this->input->post('nom')==null){
        $data['idec']=$this->input->post('idec');
        $data['error']="Veuillez completer le Nom";
        $data['result']=$this->DAOGen->ReadGen("matiereauchoix","where idmatiereauchoix='".$this->input->post('idmatiereauchoix')."'");
        $this->load->view('admin/ModifierChoice',$data);


    }else{
        $str=$this->input->post('nom');
        $valeur=explode(" ",$str);
        $idprof=$this->DAOGen->ReadGen("utilisateur","where etat=1 and nom='".trim($valeur[0])."' and prenom='".trim($valeur[1])."' and idprofile='PR3'");
     //   echo var_dump($idprof);
         $data=array(
            "matiere"=>$this->input->post('nom'),
        );
        $id=array(
            "idmatiereauchoix"=>$this->input->post('idmatiereauchoix')
        );
        $this->DAOGen->update($data,$id,"matiereauchoix");
        redirect("ECcontrolleur/ListeChoice/".$this->input->post('idec'));
   
    }



    }
  
    public function finirclasse($idclasse){
            try{
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            $distinctionUtilisateur=$this->DAOGen->RequeteFull("select distinct idutilisateur from calculuesn where idclasse='".$idclasse."'");
            $countiduser=$this->DAOGen->RequeteFull("select distinct idue from ue where idclasse='".$idclasse."'");
            $taille=$this->DAOGen->ReadGen("ue","where idclasse='".$idclasse."'");
            $classe=$this->DAOGen->ReadGen("classe","where idclasse='".$idclasse."'");
            $result=$this->DAOGen->ReadGen("semestreannee","where idclasse=".$idclasse." and idanneescolaire=".$annee[0]->idanneescolaire);
            if($result[0]->reference==1){
                throw new \Exception("Session normale deja fini");

            }            
            for($i=0;$i<count($distinctionUtilisateur);$i++){
            //    echo var_dump($distinctionUtilisateur);
                $notesfinal=$this->DAOGen->ReadGen("notestotaluecomplet","where idutilisateur='".$distinctionUtilisateur[$i]->idutilisateur."' and idclasse='".$idclasse."' and idanneescolaire=".$annee[0]->idanneescolaire);
                $stockagenotes=array();
                for($ii=0;$ii<count($notesfinal);$ii++){
                    array_push($stockagenotes,$notesfinal[$ii]->note); 
                }

                $data2=array(
                    "idutilisateur"=>$distinctionUtilisateur[$i]->idutilisateur,
                    "idclasse"=>$idclasse,
                    "Note"=>array_sum($stockagenotes)/count($taille)  
                );
                $this->DAOGen->create($data2,null,null,"totalnotes");
                $notevariable=array_sum(($stockagenotes))/(count($taille));
                echo var_dump($notesvariable);
                if($notevariable >=10 ){
                    $inscription=array(
                        "idutilisateur"=>$distinctionUtilisateur[$i]->idutilisateur,
                        "idclasse"=>$classe[0]->suivant,
                        "Statut"=>1
                    );
                    $this->DAOGen->create($inscription,null,null,"inscription");

                }else{

                    
                
                    $inscription=array(
                        "idutilisateur"=>$distinctionUtilisateur[$i]->idutilisateur,
                        "idclasse"=>$idclasse,
                        "Statut"=>0
                    );
                    $this->DAOGen->create($inscription,null,null,"inscription");
                    $uerattrapage=$this->DAOGen->ReadGen("fulltotaluesn","where idutilisateur=".$distinctionUtilisateur[$i]->idutilisateur." and idanneescolaire=".$annee[0]->idanneescolaire." and note<10");
                    for($jj=0;$jj<count($uerattrapage);$jj++){
                        $datautilisateur=array(
                            "idutilisateur"=>$distinctionUtilisateur[$i]->idutilisateur,
                            "idue"=>$uerattrapage[$jj]->idue,
                            "idanneescolaire"=>$annee[0]->idanneescolaire,
                            "idueannee"=>$uerattrapage[$jj]->idueannee

                        );
                        $this->DAOGen->create($datautilisateur,null,null,"utilisateurrattrapage");
                    }
                }
                
            }
            $updaterattrapage=array(
                "etat"=>1
            );
            $condition2=array(
                "idanneescolaire"=>$annee[0]->idanneescolaire
            );
            $this->DAOGen->update("rattrapageueannee","rattrapageueannee");
            $maj=array(
                "reference"=>1
            );
            
            $condition=array(
                "idclasse"=>$idclasse,
                "idanneescolaire"=>$annee[0]->idanneescolaire
            );
            $where="where idclasse='".$idclasse."'";
            $tableau=$this->DAOGen->ReadGen("ue",$where);
            $this->DAOGen->update($maj,$condition,"semestreannee");
            $data['classe']=$this->DAOGen->ReadGen("classe",null);
            $data['result']=$tableau;
            $data['idclasse']=$idclasse;
            $this->load->view('admin/UeparclasseExamen',$data);
        }catch(\Exception $e){
            //echo $e->getMessage();
            $where="where idclasse='".$idclasse."'";
            $tableau=$this->DAOGen->ReadGen("ue",$where);
            $data['error']=$e->getMessage();
            $data['classe']=$this->DAOGen->ReadGen("classe",null);
            $data['result']=$tableau;
            $data['idclasse']=$idclasse;
            $this->load->view('admin/UeparclasseExamen',$data);


      }

        }
    public function AssignerChoiceProf(){
        if($this->input->post('city')==null){
            $result['idec']=$this->input->post('idec');
            $result['error']="Veuillez completer le Nom";
            $this->load->view('admin/AjoutProfEc',$result);


        }else{
            $result=$this->DAOGen->ReadGen("matiereauchoix","where idmatiereauchoix='".$this->input->post('idchoice')."'");
            $str=$this->input->post('city');
            $valeur=explode(" ",$str);
            $idprof=$this->DAOGen->ReadGen("utilisateur","where etat=1 and nom='".trim($valeur[0])."' and prenom='".trim($valeur[1])."' and idprofile='3'");
            echo var_dump($idprof);
            $sequence=$this->DAOGen->getSequence("idprof","prof");
            $insert=array(
                "idprof"=>$sequence->sequence,
                "idec"=>$result[0]->idec,
                "idutilisateur"=>$idprof[0]->idutilisateur
            );
            $this->DAOGen->create($insert,null,null,"prof");
            $where=array(
                "idmatiereauchoix"=>$this->input->post('idchoice'),
            );
            $update=array(
                "idprof"=>$sequence->sequence
            );
            $this->DAOGen->update($update,$where,"matiereauchoix");
            redirect("ECcontrolleur/ListeChoice/".$result[0]->idec);
       
        }
    }
    /*
    public function finirEcAdmin(){
        $this->load->model('MetierEC');
        $idec=$this->input->post('idec');
        $detailEc=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'"); 
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat='1'");
        try{
        
        if($detailEc[0]->choix==0){
       //     echo "ato o";
           $ec=$this->DAOGen->ReadGen("ecannee","where idec='".$idec."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
           $examen=$this->DAOGen->ReadGen("Examen","where idecannee='".$ec[0]->idecannee."'");
           $idexamen=array();
           if(count($examen)==0){
            throw new \Exception("Il n'y a pas d'examen à valider");
           }
           $controleExamen=array_filter($examen,function($a){return $a->etat==5;});
           $controleExamenFini=array_filter($examen,function($a){return $a->etat==10;});
          if(count($controleExamen)>0){
            throw new \Exception("Examen deja valider");
          }
          if(count($controleExamen)>0){
            throw new \Exception("Examen deja fini");
          }
          


            

           for($i=0;$i<count($examen);$i++){
            array_push($idexamen,$examen[$i]->idexamen);
           }
           $requete=$this->MetierEC->AccesRequete($idexamen); 
           for($i=0;$i<count($requete);$i++){
            $notes=$requete[$i]->sum/count($idexamen);
            $data=array(
                "idecannee"=>$examen[0]->idecannee,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idutilisateur"=>$requete[$i]->idutilisateur,
                "total"=>$notes
            );

            


            $this->DAOGen->create($data,null,null,"notesectotalsn");
            }
            $data=array(
                "etat"=>5
            );
            $id=array(
                "idecannee"=>$ec[0]->idecannee
            );

            for($i=0;$i<count($examen);$i++){
                $insert=array(
                        "idexamen"=>$examen[$i]->idexamen
                );
        //    $this->DAOGen->create($insert,"idvalidationexam","VAD","validationexam");
         $this->DAOGen->update($data,$insert,"examen");    
        }

            
            redirect('UEController/ListeEcparUeExamenProf/1/'.$ec[0]->idue);
        }else{
            $ec=$this->DAOGen->ReadGen("ecannee","where idec='".$idec."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $examen=$this->DAOGen->ReadGen("Examen","where idecannee='".$ec[0]->idecannee."'");
            $resultt=$this->DAOGen->RequeteSpecifique("select distinct idmatiereauchoix from examen where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$ec[0]->idecannee."'");
            if($examen[0]->etat==5){
                throw new \Exception("Examen deja valider");
               }
               $controleExamen=array_filter($examen,function($a){return $a->etat==5;});
               $controleExamenFini=array_filter($examen,function($a){return $a->etat==10;});
              if(count($controleExamen)>0){
                throw new \Exception("Examen deja valider");
              }
              if(count($controleExamen)>0){
                throw new \Exception("Examen deja fini");
              }
              
            //  echo var_dump($resultt);
          //  echo count($resultt);;
            for($i=0;$i<count($resultt);$i++){
            
           //     echo $i;
                    
            //    echo $resultt[$i]->idmatiereauchoix;
                    $examenn=$this->DAOGen->ReadGen("examen","where idmatiereauchoix='".$resultt[$i]->idmatiereauchoix."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
                    $idexamen=array();
                    for($jj=0;$jj<count($examenn);$jj++){
                        array_push($idexamen,$examenn[$jj]->idexamen);
                    }
                    
                    $requete=$this->MetierEC->AccesRequete($idexamen);
                    //echo var_dump($requete);
                    for($ii=0;$ii<count($requete);$ii++){
                        $notes=$requete[$ii]->sum/count($idexamen);
                        $data=array(
                            "idecannee"=>$ec[0]->idecannee,
                            "idanneescolaire"=>$annee[0]->idanneescolaire,
                            "idutilisateur"=>$requete[$ii]->idutilisateur,
                          
                            "total"=>$notes
                        );
                        $this->DAOGen->create($data,null,null,"notesectotalsn");
                        } 
                        
                    
            }
            $data=array(
                "etat"=>5
            );
            $id=array(
                "idecannee"=>$ec[0]->idecannee
            );
         //   echo var_dump($examen);

            for($iii=0;$iii<count($examen);$iii++){
                $insert=array(
                        "idexamen"=>$examen[$iii]->idexamen
                );
        //    $this->DAOGen->create($insert,"idvalidationexam","VAD","validationexam");
        $this->DAOGen->update($data,$insert,"examen");
            }
          
            

            


        }
      //  redirect('UEController/ListeEcparUeExamen/'.$ec[0]->idue);
      redirect('UEController/ListeEcparUeExamenProf/1/'.$ec[0]->idue);
        }catch(\Exception $e){
            //echo $e->getMessage();
            $data['error']=$e->getMessage();
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            $idecann=$this->DAOGen->ReadGen("ecannee","where idec='".$this->input->post('idec')."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$this->input->post('idec')."'");
            if($checkchoiceEc[0]->choix==0){    
            $data['result']=$this->DAOGen->ReadGen("detailexamen ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3");
            $data['idec']=$this->input->post('idec');
            $this->load->view('admin/ListeExamen.php',$data);
            }else{
            $data['result']=$this->DAOGen->ReadGen("ListeExamenChoice ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etatExamen>=3 order by matiere asc");
       //     echo var_dump($data['result']);
        //    echo "where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3 order by matiere asc";
            $data['idec']=$this->input->post('idec');
    //        $this->load->view('admin/ListeExamenChoice',$data);
    
    $this->load->view('admin/Newlistexamen2',$data);

        }
    }




    }
    */
    public function finirEcAdminResposable($idanneescolaire){
        $this->load->model('MetierEC');
        $this->load->model('MetierUE');
        $idec=$this->input->post('idec');
        $detailEc=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'"); 
        $annee=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
        try{
            if($annee[0]->etat==10){
                throw new \Exception("Cette année est déjà fini");
            }
            else if($annee[0]->etat==0){
                throw new \Exception("Cette année n'a pas encore commence");   
            }
        
        if($detailEc[0]->choix==0){
       //     echo "ato o";
           $ec=$this->DAOGen->ReadGen("ecannee","where idec='".$idec."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
           $examen=$this->DAOGen->ReadGen("Examen","where idecannee='".$ec[0]->idecannee."'");
           $idexamen=array();
           if(count($examen)==0){
            throw new \Exception("Il n'y a pas d'examen à valider");
           }
           $controleExamen=array_filter($examen,function($a){return $a->etat==5;});
           $controleExamenFini=array_filter($examen,function($a){return $a->etat==10;});
          if(count($controleExamen)>0){
            throw new \Exception("Examen deja valider");
          }
          if(count($controleExamenFini)>0){
            throw new \Exception("Examen deja fini");
          }
          


            

           for($i=0;$i<count($examen);$i++){
            array_push($idexamen,$examen[$i]->idexamen);
           }
           $requete=$this->MetierEC->AccesRequete($idexamen); 
           for($i=0;$i<count($requete);$i++){
            $notes=$requete[$i]->sum/count($idexamen);
            $data=array(
                "idecannee"=>$examen[0]->idecannee,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "idutilisateur"=>$requete[$i]->idutilisateur,
                "total"=>$notes
            );

            


            $this->DAOGen->create($data,null,null,"notesectotalsn");
            }
            $data=array(
                "etat"=>5
            );
            $id=array(
                "idecannee"=>$ec[0]->idecannee
            );
            $this->DAOGen->update($data,$id,"ecannee");  
            for($i=0;$i<count($examen);$i++){
                $insert=array(
                        "idexamen"=>$examen[$i]->idexamen
                );
        //    $this->DAOGen->create($insert,"idvalidationexam","VAD","validationexam");
         $this->DAOGen->update($data,$insert,"examen");    
        }
            $result=$this->MetierUE->finirunue($detailEc[0]->idue);
            echo var_dump($result); 
            if($result==1){
               
                $this->MetierUE->finirclasse($detailEc[0]->idclasse);                
            }
            
                //miantso le fonction finiruen(0 na 1)
                //si==1
                //miantso le finirsemestre//
                //si==1
                //miantso le finirsessionnorame//


          //  redirect('UEController/ListeEcparUeExamenProf/1/'.$ec[0]->idue);
        }else{
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            $ec=$this->DAOGen->ReadGen("ecannee","where idec='".$idec."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $examen=$this->DAOGen->ReadGen("Examen","where idecannee='".$ec[0]->idecannee."'");
            $result=$this->DAOGen->RequeteSpecifique("select distinct idmatiereauchoix from examen where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$ec[0]->idecannee."'");
            $distinct=$this->DAOGen->RequeteFull("select distinct idmatiereauchoix from matiereauchoix where idec=".$ec[0]->idec);
            if(count($result)<count($distinct)){
                throw new \Exception("Il y a un matiere qui n'a pas encore ete fait");
            }
            if(count($examen)==0){
                throw new \Exception("Il n'y a pas d'examen à valider");
               }

            if($examen[0]->etat==5){
                throw new \Exception("Examen deja valider");
               }
               $controleExamen=array_filter($examen,function($a){return $a->etat==5;});
               $controleExamenFini=array_filter($examen,function($a){return $a->etat==10;});
              if(count($controleExamen)>0){
                throw new \Exception("Examen deja valider");
              }
              if(count($controleExamen)>0){
                throw new \Exception("Examen deja fini");
              }
              
            //  echo var_dump($resultt);
          //  echo count($resultt);;
            for($i=0;$i<count($distinct);$i++){
            
           //     echo $i;
                    
            //    echo $resultt[$i]->idmatiereauchoix;
                    $examenn=$this->DAOGen->ReadGen("examen","where idmatiereauchoix='".$distinct[$i]->idmatiereauchoix."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
                    $idexamen=array();
                    for($jj=0;$jj<count($examenn);$jj++){
                        array_push($idexamen,$examenn[$jj]->idexamen);
                    }
                    
                    $requete=$this->MetierEC->AccesRequete($idexamen);
                    //echo var_dump($requete);
                    for($ii=0;$ii<count($requete);$ii++){
                        $notes=$requete[$ii]->sum/count($idexamen);
                        $data=array(
                            "idecannee"=>$ec[0]->idecannee,
                            "idanneescolaire"=>$annee[0]->idanneescolaire,
                            "idutilisateur"=>$requete[$ii]->idutilisateur,
                          
                            "total"=>$notes
                        );
                        $this->DAOGen->create($data,null,null,"notesectotalsn");
                        } 
                        
                    
            }
            $data=array(
                "etat"=>5
            );
            $id=array(
                "idecannee"=>$ec[0]->idecannee
            );
         //   echo var_dump($examen);
         $this->DAOGen->update($data,$id,"ecannee");
            for($iii=0;$iii<count($examen);$iii++){
                $insert=array(
                        "idexamen"=>$examen[$iii]->idexamen
                );
        //    $this->DAOGen->create($insert,"idvalidationexam","VAD","validationexam");
        $this->DAOGen->update($data,$insert,"examen");
            }
            $result=$this->MetierUE->finirunue($detailEc[0]->idue);
            echo var_dump($result); 
            if($result==1){
                $this->MetierUE->finirclasse($detailEc[0]->idclasse);   
                            
            }
          
            

            


        }

        redirect('UEController/ListeEcparUeExamenProf/'.$ec[0]->idue);
    //    http://localhost/CommoStagiairemysql/ECcontrolleur/SituationEC/2
  //      redirect('UEController/ListeEcparUeExamenProf/'.$ec[0]->idue);
   
        //   redirect('UEController/ListeEcparUeExamenProf/1/'.$ec[0]->idue);
        }catch(\Exception $e){
            //echo $e->getMessage();
            $data['error']=$e->getMessage();
            $annee=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
            $idecann=$this->DAOGen->ReadGen("ecannee","where idec='".$this->input->post('idec')."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$this->input->post('idec')."'");
            if($checkchoiceEc[0]->choix==0){    
            $data['result']=$this->DAOGen->ReadGen("detailexamen ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3");
            $data['idec']=$this->input->post('idec');
        //    $this->load->view('admin/ListeExamen.php',$data);
            $data['idannee']=$idanneescolaire;
            $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
            $this->load->view('Prof/Newlistexamen2.php',$data);   
         }else{
            $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
            $data['result']=$this->DAOGen->ReadGen("ListeExamenChoice ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etatExamen>=3 order by matiere asc");
       //     echo var_dump($data['result']);
        //    echo "where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3 order by matiere asc";
        $data['idannee']=$idanneescolaire;    
        $data['idec']=$this->input->post('idec');
    //        $this->load->view('admin/ListeExamenChoice',$data);
    
    $this->load->view('Prof/Newlistexamen2',$data);

        }
    }




    }
    public function GetChoice(){
        $postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$idr = $request->reponse;
      //  $idr= $this->input->post('reponse');
    //    $this->load->model('DAOplacement');
    //    $result=$this->DAOplacement->getnombre($idr);
      //  echo $idr
        //$this->load->view('reservation',$result);
        $result=$this->DAOGen->ReadGen("ec","where idec='".$idr."'");
        if($result[0]->choix==0){
            $res=[];
            $test =new Jsend ("success","Ok",$res);   
            $data= json_encode($test);
            echo $data;
        }else{
            
            $u_rec_id = $this->session->userdata('idutilisateur');
            $choice=$this->DAOGen->ReadGen("ProfChoice","where idec='".$idr."' and idutilisateur='".$u_rec_id."'");
            $test =new Jsend ("success","Ok",$choice);   
            $data= json_encode($test);
            echo $data;

        }
    }
    public function GetChoiceFULL(){
        $postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
      //  echo json_encode($request->reponse);
        
        $idr = $request->reponse;
      //  $idr= $this->input->post('reponse');
    //    $this->load->model('DAOplacement');
    //    $result=$this->DAOplacement->getnombre($idr);
      //  echo $idr
        //$this->load->view('reservation',$result);
        $result=$this->DAOGen->ReadGen("ec","where idec='".$idr."'");
        if($result[0]->choix==0){
            $res=[];
            $test =new Jsend ("success","Ok",$res);   
            $data= json_encode($test);
            echo $data;
        }else{
            
            $u_rec_id = $this->session->userdata('idutilisateur');
            $choice=$this->DAOGen->ReadGen("matiereauchoix","where idec='".$idr."'");
            $test =new Jsend ("success","Ok",$choice);   
            $data= json_encode($test);
            echo $data;

        }
        
    }
    public function GetdetailProf(){
        $postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
    //    echo json_encode($request->reponse);
     //   echo $request;
       $idr = $request->reponse;
      //  $idr= $this->input->post('reponse');
      //  echo $idr
        //$this->load->view('reservation',$result);
       
        $result=$this->DAOGen->ReadGen("profchoice","where idmatiereauchoix='".$idr."'");
       
        for($i=0;$i<count($result);$i++){
                $result[$i]->nom=$result[$i]->nom." ".$result[$i]->prenom; 
        }
          $test =new Jsend ("success","Ok",$result);   
          echo json_encode($test);
        
      
        
    
        
    }
    public function Getprof(){
        $postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
    //    echo json_encode($request->reponse);
     //   echo $request;
       $idr = $request->reponse;
      //  $idr= $this->input->post('reponse');
      //  echo $idr
        //$this->load->view('reservation',$result);
       
        $result=$this->DAOGen->ReadGen("profparec","where idec='".$idr."'");
        if($result[0]->choix==1){
            $test =new Jsend ("success","Ok",[]);   
            echo json_encode($test);
        }
        else{
        for($i=0;$i<count($result);$i++){
                $result[$i]->nom=$result[$i]->nom." ".$result[$i]->prenom; 
        }
          $test =new Jsend ("success","Ok",$result);   
          echo json_encode($test);
        }    
    }
    public function getUtilisateurExamen($idec){
            try{
            $data['listeEleve']=[];
            $u_rec_id = $this->session->userdata('idutilisateur');
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
            if(count($annee)==0){
                throw new \Exception("L'année n'a pas encore commencé");
            }
            $ec=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
            $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$ec[0]->idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $this->load->model('MetierEC');   
        //    $this->MetierEC->getEcAnnee($idec);
            $ecannee=$this->DAOGen->ReadGen("ecannee","where idanneescolaire='".$annee[0]->idanneescolaire."' and idec='".$idec."'");
            $prof=$this->DAOGen->ReadGen("prof","where idutilisateur='".$u_rec_id."' and idec='".$idec."'");
            $resultatexamen=$this->DAOGen->ReadGen("exameninfo","where idprof='".$prof[0]->idprof."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $recherche=$this->DAOGen->ReadGen("ecanneesn","where idanneescolaire='".$annee[0]->idanneescolaire."' and idec='".$idec."' and (etat is not null or etat>0)");
            if(count($resultatexamen)>0){
                throw new \Exception("examen deja note pour vous");
            }
            if(count($recherche)>0){
                throw new \Exception("examen deja ec deja valide");
            }
            
            
            /*
            if($ue[0]->etat==0){
                throw new \Exception("Ue n'a pas encore commencer");
            }
            else if($ue[0]->etat==5){
                throw new \Exception("Ue deja Fini");
            }
            */
          //  else if($ue[0]->etat==1){
                if($ec[0]->choix==0)
                {
                    
                    $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
                    //  $data['idec']=$idec;
                    $getredoublant=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."' and statut=0");
                    if(count($getredoublant)>0){
                        $getRequete=$this->DAOGen->FonctiongetCreateORVersion2($getredoublant,"idutilisateur");
                        $resultUtilisateur=$this->DAOGen->RequeteFull("select*from obtention where (".$getRequete.") and mention is not null and idue=".$ec[0]->idue);
                        $listefinal=array();
                        for($i=0;$i<count($listeEleve);$i++){
                            for($jj=0;$jj<count($resultUtilisateur);$jj++){
                                    
                                    if($listeEleve[$i]->idutilisateur!=$resultUtilisateur[$jj]->idutilisateur)
                                    {
                                        array_push($listefinal,$listeEleve[$i]);
                                    }
                            }
                        }
                    //    $listeEleve=$listefinal;
                    if(count($listefinal)==0){
                       $data['listeEleve']=$listeEleve;
                      }
                      else{
                        $data['listeEleve']=$listefinal;
                
                        }
                    }else{
                        $data['listeEleve']=$listeEleve;

                    }

                    
                    
                   
                    $data['idec']=$idec;
                 //   $data['date']=$date;
                  //  $data['name']=$texte;
                       $data['idchoice']=null;
                    $data['idchoice']=$this->input->post('idchoice');
                   $this->load->view('Prof/NewEleveExamenSN',$data);
                }else{
                    $u_rec_id = $this->session->userdata('idutilisateur');
                    $gen=$this->DAOGen->ReadGen("examenmembrechoix","where idutilisateur=".$u_rec_id." and  idec=".$idec);

                    $listeEleve=$this->DAOGen->ReadGen("detailinscriptionchoix","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."' and  idmatiereauchoix='".$gen[0]->idmatiereauchoix."'");
                    $getredoublant=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."' and statut=0");
                    if(count($getredoublant)>0){
                        $getRequete=$this->DAOGen->FonctiongetCreateORVersion2($getredoublant,"idutilisateur");
                        $resultUtilisateur=$this->DAOGen->RequeteFull("select*from obtention where (".$getRequete.") and mention is not null and idue=".$ec[0]->idue);
                        $listefinal=array();
                        for($i=0;$i<count($listeEleve);$i++){
                            for($jj=0;$jj<count($resultUtilisateur);$jj++){
                                    if($listeEleve[$i]->idutilisateur!=$resultUtilisateur[$jj]->idutilisateur)
                                    {
                                        array_push($listefinal,$listeEleve[$i]);
                                    }
                            }
                        }
                     //   $listeEleve=$listefinal;
                     if(count($listefinal)==0){
                        $data['listeEleve']=$listeEleve;
                      }
                      else{
                        $data['listeEleve']=$listefinal;
                      }
                    }else{


                        $data['listeEleve']=$listeEleve;   
                    }
                    
                    
                    
                    //  $data['idec']=$idec;
                //    echo "where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."' and  idmatiereauchoix='".$this->input->post('idchoice')."'";    

           //         echo var_dump($this->input->post());
                     //     $data['listeEleve']=$listeEleve;
                          $data['idec']=$idec;
                      //    $data['date']=$date;
                        //  $data['idchoice']=$this->input->post('idchoice');
                       //   $data['name']=$texte;
                        $data['idchoice']=$gen[0]->idmatiereauchoix;
                          $this->load->view('Prof/NewEleveExamenSN',$data);
                }
             


                }   catch(\Exception $e){
                        $data['error']=$e->getMessage();
                        $ec=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
                        $u_rec_id = $this->session->userdata('idutilisateur');
                        $data['ec']=$this->DAOGen->ReadGen('detailec',"where idclasse='".$ec[0]->idclasse."' and idutilisateur='".$u_rec_id."'");
                       // echo var_dump($data['ec']);
                       $data['idclasse']=$data['ec'][0]->idclasse;
                              for($i=0;$i<count($data['ec']);$i++){
                        $data['ec'][$i]->position="UE".$data['ec'][$i]->position;
                
                       }
                        $this->load->view('Prof/NewdetailExamen2',$data);




                }
          //  }
        }
        public function afficherListeExamenEC($id,$idanneescolaire){
            $u_rec_id = $this->session->userdata('idutilisateur');
            $test=$this->DAOGen->ReadGen("responsableec","where idec=".$id." and idutilisateur=".$u_rec_id);
            $data['submit']="";
            if(count($test)>0){
            $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
            }
            $annee=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
            $idecann=$this->DAOGen->ReadGen("ecannee","where idec='".$id."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$id."'");
          //  echo var_dump($checkchoiceEc);
            if($checkchoiceEc[0]->choix==0){    
            $data['result']=$this->DAOGen->ReadGen("fullexameninfo","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."'");
         //   echo var_dump($data['result']);
            $data['idannee']=$idanneescolaire;
            $data['idec']=$id;
            $data['idannee']=$idanneescolaire;
          //  $this->load->view('admin/ListeExamen.php',$data);
          $this->load->view('Prof/NewlistexamenNormale',$data);
    
        }else{
         //   $data['result']=$this->DAOGen->ReadGen("listeexamenchoiceinfo ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etatExamen>=3 order by matiere asc");
       //     echo var_dump($data['result']);
        //    echo "where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3 order by matiere asc";
        $data['idannee']=$idanneescolaire;
        $data['result']=$this->DAOGen->ReadGen("listeexamenchoiceinfo ","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."'  order by matiere asc");
            $data['idec']=$id;
            $this->load->view('Prof/NewlistexamenNormale',$data);
          //  $this->load->view('admin/ListeExamenChoice',$data);
            
    
    
    
            }




        }
     
        public function newsubmitExamenSN(){
            try{
                $idec=$this->input->post("idec");
                $idclasse=$this->DAOGen->ReadGen("detailec","where idec=".$idec);
                $choice=$this->input->post("idchoice");
                $this->MetierEC->validationnoteExamen($idec,$choice);
                redirect('ECcontrolleur/ExamenparclasseListe/'.$idclasse[0]->idclasse);
                }catch(\Exception $e){
                    $idec=$this->input->post("idec");
                     $data['error']=$e->getMessage();
                     $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
                $ec=$this->DAOGen->ReadGen("detailec","where idec='".$idec."'");
                $ue=$this->DAOGen->ReadGen("ueannee","where idue='".$ec[0]->idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
                $this->load->model('MetierEC');   
                if($ec[0]->choix==0)
                {                    
                    $listeEleve=$this->DAOGen->ReadGen("detailinscription","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
                    $data['listeEleve']=$listeEleve;
                    $data['idec']=$idec;
                    $data['idchoice']=null;
                    $data['idchoice']=$this->input->post('idchoice');
                    $this->load->view('Prof/NewEleveExamenSN',$data);
                }else{
                    $u_rec_id = $this->session->userdata('idutilisateur');
                    $gen=$this->DAOGen->ReadGen("examenmembrechoix","where idutilisateur=".$u_rec_id." and  idec=".$idec);
                    $listeEleve=$this->DAOGen->ReadGen("detailinscriptionchoix","where idclasse='".$ec[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."' and  idmatiereauchoix='".$gen[0]->idmatiereauchoix."'");
                          $data['listeEleve']=$listeEleve;
                          $data['idec']=$idec;
                          $data['idchoice']=$gen[0]->idmatiereauchoix;
                          $this->load->view('Prof/NewEleveExamenSN',$data);
                }
            }
        }
        public function ValidationExamen2($idanneescolaire){
        
            $idec=$this->input->post('idec');
            $arrayexamen=$this->input->post('idexamen');
            $arraydevoir=$this->input->post('iddevoir');
            $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
            $detailec=$this->DAOGen->ReadGen("detailec","where idec=".$idec);
            try{
            if($anneescolaire[0]->etat==10){
                throw new \Exception("Cette année est déjà fini");
            }
            else if($anneescolaire[0]->etat==0){
                throw new \Exception("Cette année n'a pas encore commencé");
            }
            $this->MetierEC->NewValidationExamen($idec,$detailec[0]->choix,$this->input->post('choix'));
            $value=$this->MetierEC->validationExamen($detailec[0]->idue,$anneescolaire[0]->idanneescolaire);
            /*
            if($value==1){
            $this->MetierEC->FinirExamen($anneescolaire[0]->idanneescolaire,$detailec[0]->idclasse);
            }
            */
               // echo "UEController/ListeEcparUeExamenProf/".$detailec[0]->idue; 
         
         
               redirect('/UEController/ListeEcparUeExamenProf/'.$detailec[0]->idue);
            }catch(\Exception $e){
                
                $data['submit']="<button class='btn btn-primary' type='submit'>Validation</button>";
                $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
                $getecannee=$this->DAOGen->ReadGen("ecannee","where idanneescolaire='".$idanneescolaire."' and idec='".$this->input->post('idec')."'");
                $id=$this->input->post('idec');
                $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$this->input->post('idec')."'");
                if($checkchoiceEc[0]->choix==0){    
                    $data['error']=$e->getMessage();
                    $data['idannee']=$idanneescolaire;
                    

                   
                $checkchoiceEc=$this->DAOGen->ReadGen("ec","where idec='".$this->input->post('idec')."'");
                $getecannee=$this->DAOGen->ReadGen("ecannee","where idanneescolaire='".$idanneescolaire."' and idec='".$this->input->post('idec')."'");
                    $data['result']=$this->DAOGen->ReadGen("fullexameninfo","where idanneescolaire='".$idanneescolaire."' and idecannee='".$getecannee[0]->idecannee."'");
                 //   echo var_dump($data['result']);
                    $data['idec']=$id;
                  //  $this->load->view('admin/ListeExamen.php',$data);
                  $this->load->view('Prof/NewlistexamenNormale',$data);
            
                }else{
                  //  $data['result']=$this->DAOGen->ReadGen("listeexamenchoiceinfo","where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$getecannee[0]->idecannee."' and etatExamen>=3 order by matiere asc");
                    $data['result']=$this->DAOGen->ReadGen("listeexamenchoiceinfo","where idanneescolaire='".$idanneescolaire."' and idecannee='".$getecannee[0]->idecannee."'  order by matiere asc");
               //     echo var_dump($data['result']);
                //    echo "where idanneescolaire='".$annee[0]->idanneescolaire."' and idecannee='".$idecann[0]->idecannee."' and etat>=3 order by matiere asc";
                $data['idannee']=$idanneescolaire;
                    $data['error']=$e->getMessage();
                    $data['idec']=$id;
                    $this->load->view('Prof/NewlistexamenNormale',$data);
                  //  $this->load->view('admin/ListeExamenChoice',$data);
                    
            
            
            
                    }
            }
            
            }
        
        


}
  //  SELECT        MatiereAuChoix.*, Prof.idUtilisateur
  //  FROM            Prof INNER JOIN
    //                         MatiereAuChoix ON Prof.idProf = MatiereAuChoix.idProf



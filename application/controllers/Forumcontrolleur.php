<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\Jsend;
class Forumcontrolleur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        $this->load->library("pagination");
        $this->load->library("session");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function Discussion($idForum){
            $data['idforum']=$idForum;
            $this->load->view('Prof/ForumDiscussion',$data);
            
    }
    public function Notification(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $infoutilisateur=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$u_rec_id);
        $this->load->model('MetierForum');
        $config = array();
        $listeConsultation=$this->DAOGen->ReadGen("consultationforum","where etat=0 and idutilisateur=".$u_rec_id);
        if(count($listeConsultation)==0){
        $result=[]; 
        }else{
            $listeidForum=$this->DAOGen->FonctiongetCreateORVersion2($listeConsultation,"idforum");
            $result=$count=$this->DAOGen->RequeteSpecifique("select distinct idforum,idutilisateur,idec,idue,idclasse,classe,idanneescolaire,creation,titre,ecnom,nom,prenom,debut,fin from forumparticipant2 where (idutilisateur ='".$u_rec_id."' and participant!='".$u_rec_id."') or (idutilisateur!='".$u_rec_id."' and participant='".$u_rec_id."') and ($listeidForum)  group by idforum,participant,idutilisateur,idec,idue,idclasse,classe,idanneescolaire,creation,titre,ecnom,nom,prenom,debut,fin order by creation desc");
        }
        for($i=0;$i<count($result);$i++){
            if($result[$i]->idutilisateur==$u_rec_id){
                $result[$i]->nom="<p style='color:blue;'><i data-feather='user' width='20'></i></p>";
                $result[$i]->idue="<p style='color:blue;'>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
                $result[$i]->creation="<p style='color:blue;'>".$result[$i]->creation."</p>";
                $result[$i]->debut="<p style='color:blue;'>".$result[$i]->debut."/".$result[$i]->fin."</p>";
            }else{
                $result[$i]->nom="<p style='color:blue;'>".$result[$i]->nom." ".$result[$i]->prenom."</p>";
                $result[$i]->idue="<p style='color:blue;'>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
                $result[$i]->creation="<p style='color:blue;'>".$result[$i]->creation."</p>";
                $result[$i]->debut="<p style='color:blue;'>".$result[$i]->debut."/".$result[$i]->fin."</p>";
            }
        }
        $data['result'] = $result;
        if($infoutilisateur[0]->idprofile==2){
            $this->load->view('Etudiant/Forumnotif',$data);
        }else{
            $this->load->view('Prof/Forumnotif',$data);
        }
    }
    public function newDiscussion($idForum){
        $data['idforum']=$idForum;
        $u_rec_id = $this->session->userdata('idutilisateur');
        $update=array(
            "etat"=>1
        );
        $where=array(
            "idforum"=>$idForum,
            "idutilisateur"=>$u_rec_id
        );
        $this->DAOGen->update($update,$where,"consultationforum");
            $this->load->view('Prof/NewForumDiscussion',$data);       
}
    public function newDiscussionEtudiant($idForum){
    $data['idforum']=$idForum;
    $u_rec_id = $this->session->userdata('idutilisateur');
    $update=array(
        "etat"=>1
    );
    $where=array(
        "idforum"=>$idForum,
        "idutilisateur"=>$u_rec_id
    );
    $this->DAOGen->update($update,$where,"consultationforum");
    $this->load->view('Etudiant/NewForumDiscussion',$data);       
}
    public function Create(){    
        
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $classe=$this->DAOGen->ReadGen("classe",null);
   ///     echo var_dump($classe);
        /*
        for($i=0;$i<count($classe);$i++){
            $x[$classe[$i]->classe]=$this->DAOGen->ReadGen("detailInscription","where idclasse='".$classe[$i]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        //    $listeEleve=$this->DAOGen->ReadGen("detailInscription","where idclasse='".$classe[$i]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        }

       // $x['L1']= $this->DAOGen->ReadGen("detailInscription","where idclasse='".$classe[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
       // $x['L2']=$this->DAOGen->ReadGen("detailInscription","where idclasse='CL2' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $data['classe']=$classe=$this->DAOGen->ReadGen("classe",null);
        $data['liste']=$x;
        $this->load->view("Test",$data);
        */
        $data['prof']=$this->DAOGen->ReadGen("utilisateur","where idprofile='PR3'");
        $data['classe'] =$classe;
        $this->load->view("Prof/CreateForum",$data);        
    }
    public function Insertion(){
          
        try{

       // echo var_dump($this->input->post('classe'));
        $u_rec_id = $this->session->userdata('idutilisateur');
        $nom=$this->input->post('NomForum');
        $listeClasse=$this->input->post('classe');
        $this->load->model('MetierForum');
        $this->MetierForum->Controlle($nom,$listeClasse);
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $sequenceForum=$this->DAOGen->getSequence("idforum","forum");
        $datetime = new \DateTime("now"); 
        $dataForum=array(
            "idforum" =>$sequenceForum->sequence,
            "idutilisateur"=>$u_rec_id,
            "reference"=> $nom,
             "creation"=>$datetime->format('Y-m-d H:i:s')         
        );
        $this->DAOGen->create($dataForum,null,null,"forum");
        for($i=0;$i<count($listeClasse);$i++){
        $listeEleveCl=$this->DAOGen->ReadGen("inscription","where idclasse='".$listeClasse[$i]."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
            for($j=0;$j<count($listeEleveCl);$j++){
                    $data2=array(
                        "idforum"=>$sequenceForum->sequence,
                        "idutilisateur"=>$listeEleveCl[$j]->idutilisateur
                    );
              $this->DAOGen->create($data2,null,null,"participant");
            }
        }
        $data3=array(
            "idforum"=>$sequenceForum->sequence,
            "idutilisateur"=>$u_rec_id
        );
        $this->DAOGen->create($data3,null,null,"participant");


        redirect("Utilisateur/getForum");

        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['classe']=$this->DAOGen->ReadGen("classe",null);
            $this->load->view("Prof/CreateForum",$data); 

        }

    }
    ///Cote Prof new affichage//

    public function Forumparprof($idclasse){
       /*
        $u_rec_id = $this->session->userdata('idutilisateur');
     //   $this->load->model('DAOGen');
     //   $result=$this->DAOGen->ReadGen("ForumDetaille","where idutilisateur ")
        $requete="select distinct idforum,idutilisateur,idec,idue,idclasse,classe,idanneescolaire,creation,reference,ecnom,nom,prenom,debut,fin from forumparticipant2 where (idutilisateur ='".$u_rec_id."' and participant!='".$u_rec_id."') or (idutilisateur!='".$u_rec_id."' and participant='".$u_rec_id."') and idclasse='".$idclasse."' group by idforum,participant,idutilisateur,idec,idue,idclasse,classe,idanneescolaire,creation,reference,ecnom,nom,prenom,debut,fin order by creation desc"; 
        $result=$this->DAOGen->RequeteFull($requete);
        for($i=0;$i<count($result);$i++){
            if($result[$i]->idutilisateur==$u_rec_id){
         //   $result[$i]->nom="<p style='color:blue;'>".$result[$i]->nom." ".$result[$i]->prenom."</p>";
         $result[$i]->nom="<p style='color:blue;'>".$result[$i]->nom." ".$result[$i]->prenom."</p>";
            $result[$i]->idue="<p style='color:blue;'>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
            $result[$i]->creation="<p style='color:blue;'>".$result[$i]->creation."</p>";
            $result[$i]->debut="<p style='color:blue;'>".$result[$i]->debut."/".$result[$i]->fin."</p>";
            }else{
                $result[$i]->nom="<p>".$result[$i]->nom." ".$result[$i]->prenom."</p>";
                $result[$i]->idue="<p>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
                $result[$i]->creation="<p>".$result[$i]->creation."</p>";
                $result[$i]->debut="<p>".$result[$i]->debut."/".$result[$i]->fin."</p>";


            }

        }
        $data['result']=$result;
        $this->load->view('Prof/Forumparclasse',$data);
        */
        $u_rec_id = $this->session->userdata('idutilisateur');
        $infoutilisateur=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$u_rec_id);
        $this->load->model('MetierForum');
        $config = array();
        $config["base_url"] = base_url() . "Forumcontrolleur/Forumparprof/".$idclasse;
        $config["total_rows"] = $this->MetierForum->getAll2($u_rec_id,$idclasse);
        $config["per_page"] = 5;
        $config["uri_segment"] = 4;
       // echo  var_dump($this->uri->segment(3));
        $this->pagination->initialize($config);
       

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data["links"] = $this->pagination->create_links();
        $result=$this->MetierForum->get_authorsLimit($u_rec_id,$idclasse,$config["per_page"], $page);
        for($i=0;$i<count($result);$i++){
            if($result[$i]->idutilisateur==$u_rec_id){
         //   $result[$i]->nom="<p style='color:blue;'>".$result[$i]->nom." ".$result[$i]->prenom."</p>";
            $result[$i]->nom="<p style='color:blue;'><i data-feather='user' width='20'></i></p>";
            $result[$i]->idue="<p style='color:blue;'>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
            $result[$i]->creation="<p style='color:blue;'>".$result[$i]->creation."</p>";
            $result[$i]->debut="<p style='color:blue;'>".$result[$i]->debut."/".$result[$i]->fin."</p>";
         //   $result[$i]->idue="<p>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
         //   $result[$i]->creation="<p>".$result[$i]->creation."</p>";
          //  $result[$i]->debut="<p>".$result[$i]->debut."/".$result[$i]->fin."</p>";
            }else{
                /*
                $result[$i]->nom="<p >".$result[$i]->nom." ".$result[$i]->prenom."</p>";
                $result[$i]->idue="<p>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
                $result[$i]->creation="<p>".$result[$i]->creation."</p>";
                $result[$i]->debut="<p>".$result[$i]->debut."/".$result[$i]->fin."</p>";
                */
                $result[$i]->nom="<p style='color:blue;'>".$result[$i]->nom." ".$result[$i]->prenom."</p>";
                $result[$i]->idue="<p style='color:blue;'>".$result[$i]->classe."/".$result[$i]->ecnom."</p>";
                $result[$i]->creation="<p style='color:blue;'>".$result[$i]->creation."</p>";
                $result[$i]->debut="<p style='color:blue;'>".$result[$i]->debut."/".$result[$i]->fin."</p>";
            } 
         



    }
    $data['idclasse']=$idclasse;
    $data['result'] = $result;
    //   $this->load->view('authors/index', $data);
  //  echo var_dump($this->uri->segment(4)); 
    if($infoutilisateur[0]->idprofile==2){
        $this->load->view('Etudiant/Forumparclasse',$data);
    }else{
    $this->load->view('Prof/Forumparclasse',$data);
    }
    }
    public function CreateParClasse($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');    
        $data['result']=$this->DAOGen->ReadGen("detailec","where idclasse='".$idclasse."' and idutilisateur='".$u_rec_id."'");
            $this->load->view('Prof/Newcreateforum',$data);

    }
    public function validation(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        if($this->input->post('titre')==null){
            $data['error']="veuillez compléter les cases";
        }
        else{
            $titre=array(
                "titre"=>$this->input->post('titre')

            );
            $data['title']=$this->input->post('titre');
            $data['ec']=$this->input->post('choix');
            
            //redirect('Forumcontrolleur/listeprofec/'.$this->input->post('choix'));
            $ue = $this->DAOGen->ReadGen("detailecnom","where idec='".trim($data['ec'])."'");
            $data['idclasse']=$ue[0]->idclasse;
        //    $data['listeprof']=$this->DAOGen->ReadGen("detailecnom","where idue='".trim($ue[0]->idue)."' and idutilisateur!='".$u_rec_id."' group by idutilisateur");
           $requete="select distinct idutilisateur,reference,nomutilisateur,prenom from detailecnom ";
            $suite="where idue='".trim($ue[0]->idue)."' and idutilisateur!='".$u_rec_id."' group by idutilisateur,reference,nomutilisateur,prenom;";
         //   $this->load->view("Prof/NewListeProf",$data);
            $data['listeprof']=$this->DAOGen->RequeteFull($requete."".$suite);
            $this->load->view("Prof/NewListeProf",$data);

        }

    }

    public function submitcreation(){
        echo var_dump($this->input->post());
            
            
            $iduee=$this->DAOGen->ReadGen("detailec","where idec='".trim($this->input->post('idec'))."'");
            echo var_dump($iduee);
            // echo var_dump($this->input->post('classe'));
           
            $u_rec_id = $this->session->userdata('idutilisateur');
             $nom=$this->input->post('titre');
             $listeClasse=$this->input->post('idclasse');
             $this->load->model('MetierForum');
            $this->MetierForum->Controlle($nom,$listeClasse);
             $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
             $sequenceForum=$this->DAOGen->getSequence("idforum","forum");
             $datetime = new \DateTime("now"); 
             $dataForum=array(
                 "idforum" =>$sequenceForum->sequence,
                 "idutilisateur"=>$u_rec_id,
                 "titre"=> $nom,
                 "creation"=>$datetime->format('Y-m-d H:i:s'),
                 "idec"=>$this->input->post('idec'),
                 "idanneescolaire"=>$annee[0]->idanneescolaire,
                 "idue"=> $iduee[0]->idue       

             );
             $this->DAOGen->create($dataForum,null,null,"forum");
             
             $listeEleveCl=$this->DAOGen->ReadGen("inscription","where idclasse='".$listeClasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
                 for($j=0;$j<count($listeEleveCl);$j++){
                         $data2=array(
                             "idforum"=>$sequenceForum->sequence,
                             "idutilisateur"=>$listeEleveCl[$j]->idutilisateur
                         );
                   $this->DAOGen->create($data2,null,null,"participant");
                 }
             
             $data3=array(
                 "idforum"=>$sequenceForum->sequence,
                 "idutilisateur"=>$u_rec_id
             );
             $this->DAOGen->create($data3,null,null,"participant");
             $prof=$this->input->post('Prof');
             for($jj=0;$jj<count($prof);$jj++){
                $data2=array(
                    "idforum"=>$sequenceForum->sequence,
                    "idutilisateur"=>trim($prof[$jj])
                );
          $this->DAOGen->create($data2,null,null,"participant");
             }
            
     
     
             redirect("Forumcontrolleur/Forumparprof/".$this->input->post('idclasse'));
     
     
         
            
            }
            
        



    
}
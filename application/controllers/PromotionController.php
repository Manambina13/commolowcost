<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
class PromotionController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
     //   $this->load->model("DAOUsers");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function ListePromotion(){
        $where=null;
     //  echo "Ato o";
         $data['result']=$this->DAOGen->ReadGen("promotionAnnee",$where);
      //      echo var_dump($data['result']);
             $this->load->view('admin/Promotion',$data);
    }
    public function process(){
        $nom=$this->input->post('nom');
        $aneescolaire=$this->input->post('choix');
        $pattern = "/[a-zA-Z]{1,}/";
		$x=preg_match($pattern, $nom);
		if($x==0){
            $data['error']="Veuillez taper le Nom";
            $data['result']=$this->DAOGen->ReadGen("promotionAnnee",null);
            //      echo var_dump($data['result']);
                   $this->load->view('admin/CreatePromotion',$data);
        }
        else{
            $where="where idanneescolaire='".$aneescolaire."'";
            $resultat=$this->DAOGen->ReadGen("promotion",$where);
            if(count($resultat)>0){
                $data['error']="Annee Scolaire Deja Pris";
            $data['result']=$this->DAOGen->ReadGen("promotionAnnee",null);
            //
                   $this->load->view('admin/CreatePromotion',$data);


            }
            else{
                    $x=array(
                        'idanneescolaire'=>$aneescolaire,
                        'nom'=>$nom
                    );
                    $this->DAOGen->create($x,null,null,"promotion");
                    redirect("PromotionController/ListePromotion");




            }
            



        }
       // echo $email." ".$pass;
      
       
    }
    public function getListeEleve(){
        $this->load->model('DAOGen');
        $where="where idprofile ='2'";
        $eleve=$this->DAOGen->ReadGen("utilisateur",$where);
        $data['result']=$eleve;
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $this->load->view('admin/ListeEleve',$data);
    }
    public function Ajout(){
        $this->load->model('DAOGen');
        $data['result']=$this->DAOGen->ReadGen("anneescolaire",null);
   //     echo var_dump($data["result"]);
        $this->load->view('admin/CreatePromotion',$data);
    }
    public function Modifier($idutilisateur){
        
    }
    public function insertionEtudiant(){
        $this->load->model('DAOGen');
        $result=$this->DAOGen->ReadGen("Classe",null);
        try{
        $utilisateur=new UtilisateurClasse('','',$this->input->post('nom'),$this->input->post('prenom'),'',$this->input->post('date'),$this->input->post('numero'),$this->input->post('email'));
        $sequence=$this->DAOGen->getSequence("idutilisateur","utilisateur");
        $id=$sequence;
        $x=array(
            "idutilisateur"=>$id,
            "idprofile"=>'2',
            "nom"=>$this->input->post('nom'),
            "prenom"=>$this->input->post('prenom'),
            "email"=>$this->input->post('email'),
            "date"=>$this->input->post('date'),
            "numero"=>$this->input->post('numero')
        );
       
       // $this->DAOGen->create($x,'idutilisateur','UT','utilisateur');
         $this->DAOGen->create($x,null,null,"utilisateur");
        $where="where etat=1";
        $annee=$this->DAOGen->ReadGen("anneescolaire",$where);
        
        $x=array(
            'idanneescolaire'=>$annee[0]->idanneescolaire,
            'idclasse'=>$this->input->post('choix'),
            'idutilisateur'=>$id,
            'statut'=> '1',
            'annee'=>$annee[0]->debut,
            'etat'=>1
        );
        $this->DAOGen->create($x,null,null,"inscription");
        redirect("Utilisateur/getListeEleve");
        }catch(Exception $e ){
            $data['result']=$result;
            $data['error']=$e->getMessage();
       //     echo var_dump($data["result"]);
            $this->load->view('admin/CreateUtilisateur',$data);

        }
    
        
      //  $this->load->view('CreateUtilisateur',$data);
    }
}


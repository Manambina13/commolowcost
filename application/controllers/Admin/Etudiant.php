<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Etudiant extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
      }
    public function Dashboard(){
        $this->load->view('Admin/');
    }
    public function users(){
        $this->load->view('Admin/listeUsers');
    }
    public function addusers(){
        $this->load->view('Admin/adduser');
    }
    public function updateusers(){
        $this->load->view('Admin/edituser');
    }
    public function deconnexion(){
        $this->session->session_destroy;
        $this->session->unset_userdata('Utilisateur');
        redirect('/');
    }
    public function listeetudiant(){
        $data['result']=$this->DAOGen->ReadGen("etudiant","where etat=5");
        $this->load->view('Admin/etudiant',$data);
    }
    public function Listevalidation(){
        $data['result']=$this->DAOGen->ReadGen("etudiant","where etat=1");
        $this->load->view('Admin/validationetudiant',$data);
    }
    public function formupdate($formateur){
        $data['result']=$this->DAOGen->ReadGen("etudiant","where etat=5 and idetudiant=".$formateur);
        $this->load->view('Admin/formeditetudiant',$data);
    }
    public function submitedit(){
        try{
            $this->Metierformateur->controleedit($this->input->post('nom'),$this->input->post('prenom'),$this->input->post('password'),$this->input->post('numero'),$this->input->post('email'),$this->input->post('password2'),$this->input->post('idformateur'));
            
            $idutilisateur=array(
                "idformateur"=>$this->input->post('idformateur')
            );
          
          
            $update=array(
              
                "nom"=>$this->input->post('nom'),
                "prenom"=>$this->input->post('prenom'),
                "mdp"=>sha1($this->input->post('password')),
                "email"=>$this->input->post('email'),
                "numero"=>$this->input->post('numero'),
                "matricule"=>$this->input->post('matricule'),
                "etat"=>1    
            );
            $this->DAOGen->update($update,$idutilisateur,"formateur");  
            redirect('Admin/Formateur/listeformateur');
        }catch(\Exception $e){
            $formateur=$this->input->post('idformateur');
            $data['error']=$e->getMessage();
            $data['result']=$this->DAOGen->ReadGen("formateur","where etat=1 and idformateur=".$formateur);
            $this->load->view('Admin/formeditformateur',$data);
            
        }

    }
    public function submitdelete(){
        $idformateur=$this->input->post('id');
        $update=array(
            "etat"=>0
        );
        $id=array(
            "idetudiant"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"etudiant");
        $this->DAOGen->update($update,$id,"inscription");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
    }
    public function valider(){
        $idformateur=$this->input->post('id');
        $update=array(
            "etat"=>5
        );
        $id=array(
            "idetudiant"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"etudiant");
        $this->DAOGen->update($update,$id,"inscription");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
    }

}

?>
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Eccontroller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
      }
    public function listeec($idmatiere){
        $data['result']=$this->DAOGen->ReadGen("ec","where idmatiere=".$idmatiere);
        $data['idmatiere']=$idmatiere;
        $this->load->view('admin/listeec',$data);    
    } 
    public function ajoutenseignent(){




    }
    public function ajout($idmatiere){
        $data['idmatiere']=$idmatiere;
        $this->load->view('Admin/ajoutec',$data);


    }
    public function ajouterec(){
        try{
            $idmatiere=$this->input->post('idmatiere');
            $getidanneescolaire=$this->DAOGen->RequeteFull("select * from anneeuniversitaire ORDER BY  idanneeuniversitaire desc");
            if(count($getidanneescolaire)==0){
                throw new \Exception("Il n'y a pas encore d'annee");
            }
            if($getidanneescolaire[0]->etat==5){
                throw new \Exception("L'annee a deja commencer");
            }
            if($getidanneescolaire[0]->etat==10){
                throw new \Exception("Annee universitaire deja fini");
            }
            $nom=$this->input->post('nom');
            if($nom=="" || $nom==NULL){
                throw new \Exception("Veuillez completer le case nom");
            
            }
            $ajout=array(
                "idmatiere"=>$idmatiere,
                "nom_ec"=>$nom,
                "etat"=>1
            );
            $this->DAOGen->create($ajout,null,null,"ec");
            redirect('Admin/Eccontroller/listeec/'.$idmatiere);
            }catch(\Exception $e){
                $data['error']=$e->getMessage();
                $idmatiere=$this->input->post('idmatiere');
                $data['idmatiere']=$idmatiere;
                $this->load->view('Admin/ajoutec',$data);



            }
        }
        public function submitedit(){
            try{
            
    
    
                if($this->input->post('nom')==NULL){

                    throw new Exception("Veuillez completer les cases");
                }
                $idec=$this->input->post('idec');
                $idutilisateur=array(
                    "idec"=>$this->input->post('idec')
                );
                $ajout=array(
                    "nom_ec"=>$this->input->post('nom'),
                );
                $this->DAOGen->update($ajout,$idutilisateur,"ec");
                $getidmatiere=$this->DAOGen->ReadGen("ec","where idec=".$idec);  
                redirect('Admin/Eccontroller/listeec/'.$getidmatiere[0]->idmatiere);       
            }catch(\Exception $e){
                $data['error']=$e->getMessage();
                $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
                $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
                $this->load->view('Admin/ajoutmatiere',$data);
            }
    
        }

        public function formupdate($formateur){
            $data['idmatiere']=$formateur;
            $data['ec']=$this->DAOGen->ReadGen("ec","where idec=".$formateur);
            $this->load->view('Admin/formeditec',$data);
        }
        public function listeformateur($idec){
                $data['result']=$this->DAOGen->RequeteFull('select formateur.*,assignationec.idec,assignationec.etat,assignationec.idformateur,assignationec.idassignationec from assignationec join formateur on assignationec.idformateur=formateur.idformateur where assignationec.etat=1 and assignationec.idec='.$idec);
                $data['idec']=$idec;
                $data['formateur']=$this->DAOGen->ReadGen("formateur","where etat=1");
                $this->load->view('Admin/listeformateurec',$data);
        }
        public function deleteassignation(){
                    $idecassignation=$this->input->post('id');
                    $update=array(
                        "etat"=>0
                    );
                    $id=array(
                        "idassignationec"=>$idecassignation
                    );
                    $this->DAOGen->update($update,$id,"assignationec");
                    $age = array(
                        "status"=>"success", 
                        "message"=>"ok"
                    );
                    
                echo json_encode($age);
        
        }
        public function submitassignation(){
            $idformateur=$this->input->post('idformateur');
            $idmatiere=$this->input->post('id');
            try{
                $result=$this->DAOGen->ReadGen("assignationec","where  etat=1 and idec='".$idmatiere."' and idformateur='".$idformateur."'");
                if(count($result)>0){
    
                    throw new \Exception("Formateur deja assigne");
                }
                $ajout=array(
               
                
                    "idformateur"=>$idformateur,
                    "idec"=>$idmatiere,
                    "etat"=>1    
                );
                $this->DAOGen->create($ajout,null,null,"assignationec");
            $age = array(
                "status"=>"success", 
                "message"=>"ok"
            );
            echo json_encode($age);
            }catch(Exception $e){
                $age = array(
                "status"=>"error",  
                "message"=>$e->getMessage(), 
            );
            echo json_encode($age);
            }
        }
        public function supprimer(){
            $idmatiere=$this->input->post('id');
            try{
                $getidanneescolaire=$this->DAOGen->RequeteFull("select * from anneeuniversitaire ORDER BY  idanneeuniversitaire desc");
                if(count($getidanneescolaire)==0){
                    throw new \Exception("Il n'y a pas encore d'annee");
                }
                if($getidanneescolaire[0]->etat==5){
                    throw new \Exception("L'annee a deja commencer");
                }
                if($getidanneescolaire[0]->etat==10){
                    throw new \Exception("Annee universitaire deja fini");
                }
                $update=array(
                    "etat"=>0
                );
                $id=array(
                    "idec"=>$idmatiere
                );
                $this->DAOGen->update($data,$id,"ec");
                $age = array(
                    "status"=>"success", 
                    "message"=>"ok"
                );
                
            echo json_encode($age);
            }catch(Exception $e){
                $age = array(
                "status"=>"error",  
                "message"=>$e->getMessage(), 
            );
            echo json_encode($age);
            }
        }
    }
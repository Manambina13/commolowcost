<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
      }
    public function Dashboard(){
        $this->load->view('Admin/');
    }
    public function users(){
        $this->load->view('Admin/listeUsers');
    }
    public function addusers(){
        $this->load->view('Admin/adduser');
    }
    public function updateusers(){
        $this->load->view('Admin/edituser');
    }
    public function deconnexion(){
        $this->session->session_destroy;
        $this->session->unset_userdata('Utilisateur');
        redirect('/');
    }
	public function agenda(){
        $this->load->view('Admin/agenda');
		
	}
}

?>
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inscription extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
      }
    public function Dashboard(){
        $this->load->view('Admin/');
    }
    public function users(){
        $this->load->view('Admin/listeUsers');
    }
    public function addusers(){
        $this->load->view('Admin/adduser');
    }
    public function updateusers(){
        $this->load->view('Admin/edituser');
    }
    public function deconnexion(){
        $this->session->session_destroy;
        $this->session->unset_userdata('Utilisateur');
        redirect('/');
    }
    public function listeetudiant(){
        $data['result']=$this->DAOGen->RequeteFull("select inscription.*,etudiant.nom,etudiant.email,etudiant.prenom,etudiant.numero from etudiant join inscription on etudiant.idetudiant=inscription.idetudiant where idanneeuniversitaire is NULL and inscription.etat=5");
        $this->load->view('Admin/listeetudiant',$data);
    }
    public function inscrire(){
        try{
        $checkannee=$this->DAOGen->ReadGen("anneeuniversitaire","where etat >=5");
        if(count($checkannee)>0){

            throw new Exception("Annee pas encore fini ou deja fini");
        }
        $idformateur=$this->input->post('id');
        $checkannee=$this->DAOGen->ReadGen("anneeuniversitaire","where etat =1");
        $update=array(
            "etat"=>10,
            "idanneeuniversitaire"=>$checkannee[0]->idanneeuniversitaire,

        );
        $id=array(
            "idetudiant"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"inscription");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
        }catch(Exception $e){

            $age = array(
                "status"=>"error", 
                "message"=>$e->getMessage(),
            );
            echo json_encode($age);

        }
    }
    public function inscrire2(){
        try{
        $checkannee=$this->DAOGen->ReadGen("anneeuniversitaire","where etat=1");
        if($this->input->post('bordereau')==NULL){

            
            throw new Exception("Completer le bordereau"); 
        }
        if(count($checkannee)==0){

            throw new Exception("Annee pas encore fini ou deja fini");
        }
        $idformateur=$this->input->post('idinscription');
        $checkannee=$this->DAOGen->ReadGen("anneeuniversitaire","where etat =1");
        $update=array(
            "etat"=>10,
            "bordereau"=>$this->input->post('bordereau'),
            "idanneeuniversitaire"=>$checkannee[0]->idanneeuniversitaire,

        );
        $id=array(
            "idetudiant"=>$idformateur,
            "idanneeuniversitaire"=>NULL,
        );
        $this->DAOGen->update($update,$id,"inscription");
        redirect('Admin/Inscription/listeetudiant');
        }catch(Exception $e){
            $data['error']=$e->getMessage();
            $data['result']=$this->DAOGen->RequeteFull("select inscription.*,etudiant.nom,etudiant.email,etudiant.prenom,etudiant.numero from etudiant join inscription on etudiant.idetudiant=inscription.idetudiant where idanneeuniversitaire is NULL and inscription.etat=5");
            $this->load->view('Admin/listeetudiant',$data);

        }
    }

}

?>
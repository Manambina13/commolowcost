<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Anneecontroller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
      }
    public function Dashboard(){
        $this->load->view('Admin/');
    }
    
    public function users(){
        $this->load->view('Admin/listeUsers');
    }
    public function addusers(){
        $this->load->view('Admin/adduser');
    }
    public function updateusers(){
        $this->load->view('Admin/edituser');
    }
    public function deconnexion(){
        $this->session->session_destroy;
        $this->session->unset_userdata('Utilisateur');
        redirect('/');
    }
    public function createform(){
        $year = \date("Y");  
        $debut=array();
        for($i=$year;$i<$year+10;$i++){
            array_push($debut,$i);
        }
        $data["date"]=$debut;
        $this->load->view('Admin/Createannee',$data);
    }
    public function liste(){
        $data['result']=$this->DAOGen->ReadGen("anneeuniversitaire",null);
        $this->load->view('Admin/annee',$data);
    }
    public function Listevalidation(){
        $data['result']=$this->DAOGen->ReadGen("etudiant","where etat=1");
        $this->load->view('Admin/validationetudiant',$data);
    }
    public function formupdate($formateur){
        $data['result']=$this->DAOGen->ReadGen("etudiant","where etat=5 and idetudiant=".$formateur);
        $this->load->view('Admin/formeditetudiant',$data);
    }
    public function lancer(){
        $idformateur=$this->input->post('id');
        try{
        $result=$this->DAOGen->ReadGen('anneeuniversitaire','where idanneeuniversitaire='.$idformateur);
        if($result[0]->etat>1){
            throw new \Exception("Annee deja fini ou en cours");
        }
        $update=array(
            "etat"=>5
        );
        $id=array(
            "idanneeuniversitaire"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"anneeuniversitaire");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
       
        }catch(Exception $e){

            $age = array(
                "status"=>"error", 
                "message"=>$e->getMessage()
            );
            echo json_encode($age);



        }
    }
    public function submitcreate(){
        $debuts=$this->input->post('debut');
        $fin=$this->input->post('fin');
        try{
            $result=$this->DAOGen->ReadGen("anneeuniversitaire","where etat=5 or etat=1");
            if(count($result)>0){
                throw new Exception("Annee pas encore termine");
            }
        if($fin==$debuts){
            $where="order by fin desc";
            $result=$this->DAOGen->ReadGen("anneeuniversitaire",$where);
            $debut=array();
            /// 
            if(count($result)==0){
                $year = \date("Y");
                for($i=$year;$i<$year+10;$i++){
                    array_push($debut,$i);
                }
            }else{  
            for($i=$result[0]->fin;$i<$result[0]->fin+10;$i++){
                array_push($debut,$i);
            }
        }
            $data["date"]=$debut;
            $data["error"]="Date semblable";
            $this->load->view('Admin/Createannee',$data);

        }
        else if($fin<$debuts){
            $where="order by fin desc";
            $result=$this->DAOGen->ReadGen("anneeuniversitaire",$where);
            $debut=array();
            if(count($result)>0){
            for($i=$result[0]->fin;$i<$result[0]->fin+10;$i++){
                array_push($debut,$i);
                }
            }else{
                $year = \date("Y");
                for($i=$year;$i<$year+10;$i++){
                    array_push($debut,$i);
                }

            }
            $data["date"]=$debut;
            $data["error"]="Date debut superieure a Fin";
            $this->load->view('Admin/Createannee',$data);
        }
        else if($fin>$debuts){
            $where="order by fin desc";
            $result=$this->DAOGen->ReadGen("anneeuniversitaire",$where);
            if(count($result)==0){
                $x=array(
                    "debut"=>$debuts,
                    "fin"=>$fin,
                    "etat"=>1
                );
                $this->DAOGen->create($x,null,null,"anneeuniversitaire");
                redirect("Admin/Anneecontroller/liste");


            }else{
            $debut=array();
            for($i=$result[0]->fin;$i<$result[0]->fin+10;$i++){
                array_push($debut,$i);
                }
            if($debuts+1==$fin){
                $x=array(
                    "debut"=>$debuts,
                    "fin"=>$fin,
                    "etat"=>1
                );
                $where="where debut='".$debuts."' and fin='".$fin."'";
                $tableau=$this->DAOGen->ReadGen("anneeuniversitaire",$where);
                if(count($tableau)>0){
                    
                $data["date"]=$debut;
                $data["error"]="date deja existant";
                $this->load->view('Admin/Createannee',$data);
                }
                else{
                $this->DAOGen->create($x,null,null,"anneeuniversitaire");
                redirect("Admin/Anneecontroller/liste");
                }
            }
            else{

                $data["date"]=$debut;
                $data["error"]="date Anormale";
                $this->load->view('Admin/Createannee',$data);
            }
        }

            }
        }
        catch(\Exception $e){
            $where="order by fin desc";
            $result=$this->DAOGen->ReadGen("anneeuniversitaire",$where);
            $debut=array();
            /// 
            if(count($result)==0){
                $year = \date("Y");
                for($i=$year;$i<$year+10;$i++){
                    array_push($debut,$i);
                }
            }else{  
            for($i=$result[0]->fin;$i<$result[0]->fin+10;$i++){
                array_push($debut,$i);
            }
        }
            $data["date"]=$debut;
            $data["error"]=$e->getMessage();
            $this->load->view('Admin/Createannee',$data);
           
            




        }
    }
    public function submitdelete(){
        $idformateur=$this->input->post('id');
        $update=array(
            "etat"=>0
        );
        $id=array(
            "idetudiant"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"etudiant");
        $this->DAOGen->update($update,$id,"inscription");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
    }
    public function information($idate){
        $data['idassignation']=$idate;
        $data['result']=$this->DAOGen->RequeteFull("select inscription.*,etudiant.nom,etudiant.numero,etudiant.prenom,etudiant.email from etudiant join inscription on etudiant.idetudiant=inscription.idetudiant where inscription.etat=10 and idanneeuniversitaire=".$idate);
        $this->load->view('Admin/listeleveencours',$data);
    }
    public function passer(){
        try{
        $idformateur=$this->input->post('idinscription');
       
        $result=$this->DAOGen->ReadGen("inscription","where idinscription=".$idformateur);
        if($result[0]->etat==15  ){
            throw new Exception("Annee deja finie pour cette etudiant");
        }
        $update=array(
            "etat"=>15
        );
        $id=array(
            "idinscription"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"inscription");
        if($this->input->post('monselect')==1){
                if($result[0]->idniveau==1){

                    $ajout=array(
                        "idetudiant"=>$result[0]->idetudiant,
                        "idniveau"=>"2",
                        "idparcour"=>$result[0]->idparcour,
                        "etat"=>5   
                    );
                    $this->DAOGen->create($ajout,null,null,"inscription");
                    redirect('Admin/Anneecontroller/information/'.$this->input->post('annee'));
                
                }
                else if($result[0]->idniveau==2){

                    $ajout=array(
                        "idetudiant"=>$result[0]->idetudiant,
                        "idniveau"=>"3",
                        "idparcour"=>$result[0]->idparcour,
                        "etat"=>5    
                    );
                    $this->DAOGen->create($ajout,null,null,"inscription");
                    redirect('Admin/Anneecontroller/information/'.$this->input->post('annee'));
                }
        }
        else{
            if($result[0]->idniveau==1){
                $ajout=array(
                    "idetudiant"=>$result[0]->idetudiant,
                        "idniveau"=>"1",
                        "idparcour"=>$result[0]->idparcour,
                        "etat"=>5 
                );
                $this->DAOGen->create($ajout,null,null,"inscription");
                redirect('Admin/Anneecontroller/information/'.$this->input->post('annee'));

            }
            else if($result[0]->idniveau==2){
                $ajout=array(
                    "idetudiant"=>$result[0]->idetudiant,
                    "idniveau"=>"2",
                    "idparcour"=>$result[0]->idparcour,
                    "etat"=>5     
                );
                $this->DAOGen->create($ajout,null,null,"inscription");
                redirect('Admin/Anneecontroller/information/'.$this->input->post('annee'));


            }
            else if($result[0]->idniveau==3){
                $ajout=array(
                    "idetudiant"=>$result[0]->idetudiant,
                    "idniveau"=>"3",
                    "idparcour"=>$result[0]->idparcour,
                    "etat"=>5   
                );
                $this->DAOGen->create($ajout,null,null,"inscription");
                redirect('Admin/Anneecontroller/information/'.$this->input->post('annee'));
            }
            redirect('Admin/Anneecontroller/information/'.$this->input->post('annee'));
        }
        }catch(Exception $e){
           
            $idate= $this->input->post('annee');
            $data=$e->getMessage();
            $data['idassignation']=$idate;
            $data['result']=$this->DAOGen->RequeteFull("select inscription.*,etudiant.nom,etudiant.numero,etudiant.prenom,etudiant.email from etudiant join inscription on etudiant.idetudiant=inscription.idetudiant where inscription.etat=10 and idanneeuniversitaire=".$idate);
            $this->load->view('Admin/listeleveencours',$data);

        }
        



    }
    public function valider(){
        $idformateur=$this->input->post('id');
        $update=array(
            "etat"=>5
        );
        $id=array(
            "idetudiant"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"etudiant");
        $this->DAOGen->update($update,$id,"inscription");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
    }
    public function finir(){
        $x=$this->input->post('id');
        try{

    
        $result=$this->DAOGen->ReadGen("inscription","where etat=10 and idanneeuniversitaire=".$x);
        $result2= $this->DAOGen->ReadGen("anneeuniversitaire","where etat=1  and idanneeuniversitaire=".$x);
        if(count($result2)>0){
            throw new \Exception("Cette annee n'a pas encore commencer");
        }
        $result4= $this->DAOGen->ReadGen("anneeuniversitaire","where etat=10  and idanneeuniversitaire=".$x);
        if(count($result4)>0){
            throw new \Exception("Annee deja fini");
        }
        if(count($result)>0){
                throw new \Exception("Il y a des etudiants qui n'ont pas encore fini l'annee");
            }
            $update=array(
                "etat"=>10
            );
            $id=array(
                "idanneeuniversitaire"=>$x
            );
            $this->DAOGen->update($update,$id,"anneeuniversitaire");
            $age = array(
                "status"=>"success", 
                "message"=>"ok"
            );
            echo json_encode($age);


        }catch(Exception $e){
            $data['error']=$e->getMessage();
            $age = array(
                "status"=>"error", 
                "message"=>$e->getMessage()
            );
            echo json_encode($age);

        }

    }

}

?>
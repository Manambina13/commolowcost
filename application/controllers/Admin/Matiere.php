<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Matiere extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
      }
    public function Dashboard(){
        $this->load->view('Admin/');
    }
    public function users(){
        $this->load->view('Admin/listeUsers');
    }
    public function addusers(){
        $this->load->view('Admin/adduser');
    }
    public function updateusers(){
        $this->load->view('Admin/edituser');
    }
    public function deconnexion(){
        $this->session->session_destroy;
        $this->session->unset_userdata('Utilisateur');
        redirect('/');
    }
    ///apii
    public function listematiereapi(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($_POST);  
        $idmarque=$this->input->post('idclasse');
       // $data['result']=$this->DAOGen->ReadGen("parcour","where id_mention='".$idmarque."' order by id_parcour asc");
       // $data['result']=$this->DAOGen->RequeteFull("select distinct(nomparcour) as nomparcour,id_niveau,id_mention,id_parcour from parcour where id_mention=".$idmarque);
       $data['result']=$this->DAOGen->RequeteFull("select  detailparcour.*,niveau.chiffre from niveau join detailparcour on niveau.idniveau=detailparcour.idniveau  where detailparcour.idparcour=".$idmarque); 
       $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "ok";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);



    }
    /////
    public function listematiere(){
        $data['result']=$this->DAOGen->RequeteFull("select matiere.*,parcour.nom_parcour,parcour.etat as etatparcour,niveau.chiffre from niveau join matiere on niveau.idniveau=matiere.idniveau join parcour on matiere.idparcour=parcour.idparcour where matiere.etat=1 and parcour.etat=1");
        $this->load->view('Admin/matiere',$data);
    }
    public function Listevalidation(){
        $data['result']=$this->DAOGen->ReadGen("etudiant","where etat=1");
        $this->load->view('Admin/validationetudiant',$data);
    }
    public function ajoutmatiere(){
        $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
        $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
        $this->load->view('Admin/ajoutmatiere',$data);

    }
    public function formupdate($formateur){
        $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
        $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
        $data['result']=$this->DAOGen->ReadGen("matiere","where etat=1");
        $this->load->view('Admin/formeditmatiere',$data);
    }
    public function submitedit(){
        try{
            if($this->input->post('nom')=="" || $this->input->post('nom')==null ){
                throw new \Exception("Nom probleme");
            }
            else{
                $result=$this->DAOGen->ReadGen("anneeuniversitaire","where etat=1");
                if(count($result)==0){
                    throw new \Exception("L'annee a deja commence");
                }
                $result2=$this->DAOGen->ReadGen("matiere","where etat=1 and idmatiere!='".$this->input->post('idmatiere')."' nom_matiere='".$this->input->post('nom')."'");
                if(count($result2)==0){
                    throw new \Exception("Nom existant");
                }


            }
            $idutilisateur=array(
                "idmatiere"=>$this->input->post('idmatiere')
            );
            $ajout=array(
           
                "nom_matiere"=>$this->input->post('nom'),
                "idniveau"=>$this->input->post('niveau'),
                "idparcour"=>$this->input->post('parcour'),
                "etat"=>1    
            );
            $this->DAOGen->update($update,$idutilisateur,"matiere");  
            redirect('Admin/Matiere/listematiere');       
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
            $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
            $this->load->view('Admin/ajoutmatiere',$data);
        }

    }
    public function submitdelete(){
        $idformateur=$this->input->post('id');
        try{
        $result=$this->DAOGen->ReadGen("anneeuniversitaire","where etat=1");
        if(count($result)==0){
                throw new \Exception("L'annee a deja commence");
        }
        $update=array(
            "etat"=>0
        );
        $id=array(
            "idmatiere"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"matiere");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
        }catch(Exception $e){
            $age = array(
            "status"=>"error",  
            "message"=>$e->getMessage(), 
        );
        echo json_encode($age);
        }
      
    }
    public function submitdeleteassignation(){
        $idformateur=$this->input->post('id');
        $idmatiere=$this->input->post('idm');
        $result=$this->DAOGen->ReadGen("anneeuniversitaire","where etat=1");
        $update=array(
            "etat"=>0
        );
        $id=array(
            "idmatiere"=>$idmatiere,
            "idformateur"=>$idformateur,

        );
        $this->DAOGen->update($update,$id,"assignation");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
        
    }
    public function recherche(){
      //  echo $this->input->post('userselected3');
       // echo $this->input->post('listeniveau');
        /*
        //$pizza  = "string:8";
        //$pieces = explode(":", $pizza);
        //echo $pieces[1]; // piece1
        */
        
        /*    
        if($this->input->post('recherche')==NULL){
                $data['result']=$this->DAOGen->ReadGen("matirere",'where idparcour='.$this->input->post('idparcour'));

            }else{


                $data['result']=$this->DAOGen->ReadGen("matirere",'where idparcour='.$this->input->post('idparcour'));

            }
            */
        $idparcour=$this->DAOGen->changement($this->input->post('userselected3'));
        $idniveau=$this->DAOGen->changement($this->input->post('listeniveau'));
       // $data['result']=$this->DAOGen->ReadGen("matiere","where idniveau=".$idniveau." and idparcour=".$idparcour." and etat=1");
       $data['result']=$this->DAOGen->RequeteFull("select matiere.*,parcour.nom_parcour,parcour.etat as etatparcour,niveau.chiffre from niveau join matiere on niveau.idniveau=matiere.idniveau join parcour on matiere.idparcour=parcour.idparcour where  matiere.idniveau=".$idniveau." and matiere.idparcour=".$idparcour." and matiere.etat=1"); 
       $this->load->view('Admin/matiere',$data);
        
    }
    public function listeformateur($formateur){
        $data['idassignation']=$formateur;
        $data['formateur']=$this->DAOGen->ReadGen("formateur","where etat=1");
        $data['result']=$this->DAOGen->RequeteFull("select formateur.*,assignation.idmatiere,assignation.etat from assignation join matiere on assignation.idmatiere=matiere.idmatiere join formateur on assignation.idformateur=formateur.idformateur where assignation.etat=1");
        $this->load->view('Admin/listeformateurmatiere',$data);
    }
    public function submitassignation(){
        $idformateur=$this->input->post('idformateur');
        $idmatiere=$this->input->post('id');
        try{
            $result=$this->DAOGen->ReadGen("assignation","where  etat=1 and idmatiere='".$idmatiere."' and idformateur='".$idformateur."'");
            if(count($result)>0){

                throw new \Exception("Formateur deja assigne");
            }
            $ajout=array(
           
            
                "idformateur"=>$idformateur,
                "idmatiere"=>$idmatiere,
                "etat"=>1    
            );
            $this->DAOGen->create($ajout,null,null,"assignation");
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
        }catch(Exception $e){
            $age = array(
            "status"=>"error",  
            "message"=>$e->getMessage(), 
        );
        echo json_encode($age);
        }
    }
    public function submitajout(){
        try{
            if($this->input->post('nom')=="" || $this->input->post('nom')==null ){
                throw new \Exception("Matricule probleme");
            }
            else{
                $result=$this->DAOGen->ReadGen("anneeuniversitaire","where etat=1");
                if(count($result)==0){

                    throw new \Exception("L'annee a deja commence");
                }



            }
            $ajout=array(
           
                "nom_matiere"=>$this->input->post('nom'),
                "idniveau"=>$this->input->post('niveau'),
                "idparcour"=>$this->input->post('parcour'),
                "etat"=>1    
            );
            $this->DAOGen->create($ajout,null,null,"matiere");
            redirect('Admin/Matiere/listematiere');       
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
            $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
            $this->load->view('Admin/ajoutmatiere',$data);

        }
    }

}

?>
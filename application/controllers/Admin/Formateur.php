<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Formateur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model('DAOGen');
        $this->load->model('Metierformateur');
    }
    public function listeformateur(){
        $data['result']=$this->DAOGen->ReadGen("formateur","where etat=1");
        $this->load->view('Admin/formateur',$data);
    }
    public function addusers(){
        $this->load->view('Admin/adduser');
    }
    public function updateusers(){
        $this->load->view('Admin/edituser');
    }
    public function addformateur(){
        $this->load->view('Admin/addformateur');

    }
    public function submitajout(){
        try{
            $this->Metierformateur->controleajout($this->input->post('nom'),$this->input->post('prenom'),$this->input->post('password'),$this->input->post('numero'),$this->input->post('email'),$this->input->post('password2'));      
            if($this->input->post('matricule')=="" || $this->input->post('matricule')==null ){
                throw new \Exception("Matricule probleme");
            }
            else{
                $result=$this->DAOGen->ReadGen("formateur","where matricule='".strtoupper($this->input->post('matricule'))."'");
                if(count($result)>0){

                    throw new \Exception("Matricule existant");
                }



            }
            $ajout=array(
           
                "nom"=>$this->input->post('nom'),
                "prenom"=>$this->input->post('prenom'),
                "mdp"=>sha1($this->input->post('password')),
                "email"=>$this->input->post('email'),
                "numero"=>$this->input->post('numero'),
                "matricule"=>strtoupper($this->input->post('matricule')),
                "etat"=>1    
            );
            $this->DAOGen->create($ajout,null,null,"formateur");
            redirect('Admin/Formateur/listeformateur');       
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $this->load->view('Admin/addformateur',$data);

        }
    }
    public function submitedit(){
        try{
            $this->Metierformateur->controleedit($this->input->post('nom'),$this->input->post('prenom'),$this->input->post('password'),$this->input->post('numero'),$this->input->post('email'),$this->input->post('password2'),$this->input->post('idformateur'));
            
            $idutilisateur=array(
                "idformateur"=>$this->input->post('idformateur')
            );
          
          
            $update=array(
              
                "nom"=>$this->input->post('nom'),
                "prenom"=>$this->input->post('prenom'),
                "mdp"=>sha1($this->input->post('password')),
                "email"=>$this->input->post('email'),
                "numero"=>$this->input->post('numero'),
                "matricule"=>$this->input->post('matricule'),
                "etat"=>1    
            );
            $this->DAOGen->update($update,$idutilisateur,"formateur");  
            redirect('Admin/Formateur/listeformateur');
        }catch(\Exception $e){
            $formateur=$this->input->post('idformateur');
            $data['error']=$e->getMessage();
            $data['result']=$this->DAOGen->ReadGen("formateur","where etat=1 and idformateur=".$formateur);
            $this->load->view('Admin/formeditformateur',$data);
            
        }

    }
    public function deconnexion(){
        $this->session->session_destroy;
        $this->session->unset_userdata('Utilisateur');
        redirect('/');
    }
    public function formupdate($formateur){
        $data['result']=$this->DAOGen->ReadGen("formateur","where etat=1 and idformateur=".$formateur);
        
        $this->load->view('Admin/formeditformateur',$data);
    }
    public function submitdelete(){
        $idformateur=$this->input->post('id');
        $update=array(
            "etat"=>0
        );
        $id=array(
            "idetudiant"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"formateur");
        $result=$this->DAOGen->RequeteFullminimal("update assignation set idmatiere=NULL where idformateur=".$idformateur);
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);
    }
}

?>
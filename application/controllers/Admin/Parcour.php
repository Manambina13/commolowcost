<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parcour extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
      }
    public function listeparcour(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($_POST);  
    
        $data['result']=$this->DAOGen->ReadGen("parcour","where etat=1");
       // $data['result']=$this->DAOGen->RequeteFull("select distinct(nomparcour) as nomparcour,id_niveau,id_mention,id_parcour from parcour where id_mention=".$idmarque); 
       $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "ok";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);
    }
    //api///
    public function getparcour(){
        $idutilisateur=$this->session->userdata('idutilisateur');
        //"select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere from matiere join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec where assignationec.idformateur=1 and assignationec.etat=1";
       // "select assignationec.idformateur,matiere.idmatiere,matiere.nom_matiere,parcour.nom_parcour,parcour.idparcour from parcour join matiere on parcour.idparcour=matiere.idparcour join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec  where assignationec.etat=1"
        //$data['result']=$this->DAOGen->ReadGen("region","order by nom_region asc");
        $data['result']=$this->DAOGen->RequeteFull("select distinct idformateur,nom_parcour,idparcour from listeparcoursvue  WHERE idformateur='".$idutilisateur."'");
        $employee_object = new stdClass;
        $employee_object->status = "success";
        $employee_object->message = "ok";
        $employee_object->data = $data['result'];
        echo json_encode($employee_object);    

    }
    

}

?>
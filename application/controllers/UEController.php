<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\UE;
use App\models\Jsend;
class UEController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        $this->load->model("MetierUE");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function ListeRattrapageValider(){
        $result=$this->DAOGen->ReadGen("RattrapageValidation","where etat=5");
        for($i=0;$i<count($result);$i++){
            $result[$i]->debutannee=$result[$i]->debutannee."-".$result[$i]->finanneescolaire;
        }
        $data['result']=$result;
        $this->load->view("admin/ListeValidationRattrapage",$data);
    }
    public function getclasseResponsablilter(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $result=$this->DAOGen->RequeteFull("select distinct(idclasse),classe from ueresponsable where idutilisateur=".$u_rec_id);
        $test =new Jsend ("success","Ok",$result);   
        echo json_encode($test);
    }
    public function AjouterResponsable($idue){
        $result['idue']=$idue;
        $result['cible']=base_url('UEController/AjoutProfUERES');
        $this->load->view('admin/AjoutResUE',$result); 
    }
    public function AjoutProfUERES(){
        //   echo var_dump($this->input->post());
           if($this->input->post('city')==null){
               $result['idue']=$this->input->post('idue');
               $result['cible']=base_url('UEController/AjoutProfUERES');
               $result['error']="Veuillez completer le Nom";
               $this->load->view('admin/AjoutResUE',$result);
   
   
           }else{
               try{
               $str=$this->input->post('city');
               $valeur=explode(" ",$str);
               if(!isset($valeur[1])){
                throw new \Exception("Ce formateur n'existe pas dans le staff");
               }
               $idprof=$this->DAOGen->ReadGen("utilisateur","where etat=1 and nom='".trim($valeur[0])."' and prenom='".trim($valeur[1])."' and idprofile='3'");
               $exception=$this->DAOGen->ReadGen("responsable","where idutilisateur=".$idprof[0]->idutilisateur." and idue=".$this->input->post('idue'));
             
    
                
               
               
               
               
               if(count($exception)>0){
                   $result['idue']=$this->input->post('idue');
                   $result['cible']=base_url('ECcontrolleur/AjoutProfECRES');
                   $result['error']="Ce prof est déjà responsable de cette UE";
                   $this->load->view('admin/AjoutResUE',$result);
               }else{
               //echo var_dump($idprof);
               $data=array(
                    "idue"=>$this->input->post('idue'),
                   "idutilisateur"=>$idprof[0]->idutilisateur
               );
               $this->DAOGen->create($data,null,null,"responsable");
               redirect("UEController/ListeResponsable/".$this->input->post('idue'));
          
           }
        }catch(\Exception $e){
            $result['idue']=$this->input->post('idue');
            $result['cible']=base_url('UEController/AjoutProfUERES');
            $result['error']=$e->getMessage();
            $this->load->view('admin/AjoutResUE',$result);
        }
           
        }
           
          
       }
    public function Responsabliliter($idclasse){
        $u_rec_id = $this->session->userdata('idutilisateur');    
        $data['result']=$this->DAOGen->ReadGen("ueresponsable","where idclasse=".$idclasse." and idutilisateur=".$u_rec_id);
        for($i=0;$i<count($data['result']);$i++){
            $data['result'][$i]->position="UE". $data['result'][$i]->position;
        } 
        $this->load->view("Prof/newListeResponsableUE",$data);
    }
    public function validationUERatttrapage($idueannee){
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $idue=$this->DAOGen->ReadGen("ueannee","where idueannee=".$idueannee." and idanneescolaire='".$anneescolaire[0]->idanneescolaire."'");
        $this->load->model('MetierEC');
        $this->MetierEC->validationUERattrapage($idue[0]->idue,$anneescolaire[0]->idanneescolaire);


    }
    public function Liste2(){
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $tableau=$this->DAOGen->ReadGen("ue",null);
        $data['result']=$tableau;
        $this->load->view('admin/ListeUe2',$data);
    }
    public function Liste(){
        $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");
        $tableau=$this->DAOGen->ReadGen("ue",null);
        $data['result']=$tableau;
        $this->load->view('admin/ListeUe',$data);
    }
    public function ListeRattrapage(){
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $tableau=$this->DAOGen->ReadGen("ue",null);
        $data['result']=$tableau;
        $this->load->view('admin/ListeRattrapage',$data);
    }
    public function Rechercheavecondition($idclasse){
        $where="where idclasse='".$idclasse."'";
        $tableau=$this->DAOGen->ReadGen("ue",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $data['result']=$tableau;
        $this->load->view('admin/Ueparclasse',$data);
    }
    public function Recherche(){
        $where="where idclasse='".$this->input->post("choix")."'";
        $tableau=$this->DAOGen->ReadGen("ue",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");
        $data['result']=$tableau;
        $this->load->view('admin/Ueparclasse',$data);
    }
    public function PrecedentUE($idue){
        $getidclasse=$this->DAOGen->ReadGen("ue","where idue=".$idue);
        $where="where idclasse='".$getidclasse[0]->idclasse."'";
        $tableau=$this->DAOGen->ReadGen("ue",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");
        $data['result']=$tableau;
        $this->load->view('admin/Ueparclasse',$data);
    }
    public function deleteUE($idue){
        try{
        $classe=$this->DAOGen->ReadGen("ue","where idue=".$idue);
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
      //  if(count($annee)>0){
        //   throw new \Exception("Il y a encore un année en cours.");
       // }
        $update=array(
            "idclasse"=>NULL
        );
        $where=array(
            "idue"=>$idue
        );
        $this->DAOGen->update($update,$where,"ue");
        redirect("UEController/Liste/".$classe[0]->idclasse);
        }catch(\Exception $e){
            $tableau=$this->DAOGen->ReadGen("ue","where idue=".$idue);
            $where="where idclasse='".$tableau[0]->idclasse."'";
            $tableau=$this->DAOGen->ReadGen("ue",$where);
            $data['error']=$e->getMessage();
            $data['classe']=$this->DAOGen->ReadGen("classe",null);
            $data['result']=$tableau;
            $this->load->view('admin/Ueparclasse',$data);


         //   Rechercheavecondition


        }
    }
    public function ListeResponsable($idue){
        $data['result']=$this->DAOGen->RequeteFull("select utilisateur.nom, utilisateur.prenom,utilisateur.email, responsable.idue, responsable.idresponsable FROM utilisateur INNER JOIN responsable ON utilisateur.idutilisateur = responsable.idutilisateur where idue=".$idue);
        $data['idue']=$idue;
        $this->load->view('admin/ListeResponsableUE',$data);
    }
    public function Supprimer($idresponsable){
        $result=$this->DAOGen->ReadGen("responsable","where idresponsable=".$idresponsable);
        $this->db->query("delete  from responsable where idresponsable='".$idresponsable."'");
        $data['result']=$this->DAOGen->RequeteFull("select utilisateur.nom, utilisateur.prenom,utilisateur.email, responsable.idue, responsable.idresponsable FROM utilisateur INNER JOIN responsable ON utilisateur.idutilisateur = responsable.idutilisateur where idue=".$result[0]->idue);
        $this->load->view('admin/ListeResponsableUE',$data);

    }
    public function RechercheExamen(){
        $where="where idclasse='".$this->input->post("choix")."'";
        $tableau=$this->DAOGen->ReadGen("ue",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $data['result']=$tableau;
        $data['idclasse']=$this->input->post("choix");
        $this->load->view('admin/UeparclasseExamen',$data);
    }
    public function ModifUE($idue){
        $where="where idue='".$idue."'";
        $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1   ");
        $tableau=$this->DAOGen->ReadGen("ue",$where);
        for($i=0;$i<count($data['classe']);$i++){
            if($data['classe'][$i]->idclasse==$tableau[0]->idclasse){
                $data['classe'][$i]->suivant="selected";
            }else{

                $data['classe'][$i]->suivant="";
            }

        }
        $data['idue']=$idue;
        $data['result']=$tableau;
        $this->load->view("admin/FicheModificationUE",$data);
    }
    public function SubmitModifUE(){
        $str = $this->input->post('nom');
		$pattern = "/[a-zA-Z]{1,}/";
        $x=preg_match($pattern, $str);
        $chiffre = $this->input->post('coefficient');
        $pattern2 = "/^(?=\d*[1-9])\d+$/";
        $response=preg_match($pattern2, $chiffre);
        try{
            if($x==0 && $response==0){
              
              //  $where="where idec='".$idec."'";
                //$data['error']="Veuillez taper des lettres approprier";
            //    $data['result']=$this->DAOGen->ReadGen("ec",$where);
          //      $this->load->view('admin/fichemodicationEC',$data);
                throw new \Exception("Veuillez taper  nom approprie");
            }
            else if($x==0 && $response>0){
          //      $data['error']="Veuillez taper  nom approprier";
                throw new \Exception("Veuillez taper  nom approprie");
            }
            else if($x>0 && $response==0){
           //     $data['error']="Veuillez taper chiffre normale";
                throw new \Exception("Veuillez taper chiffre normale");
        }
        $where=array(
            "idue"=>$this->input->post('idue')
        );
        $data=array(
            "reference"=>$str,
            "idclasse"=>$this->input->post('choix'),
            "coefficient"=>$chiffre

        );
        $this->DAOGen->update($data,$where,"ue");
        redirect("UEController/Liste");
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $where="where idue='".$this->input->post('idue')."'";
            $tableau=$this->DAOGen->ReadGen("ue",$where);
            $data['result']=$tableau;
            $this->load->view("admin/FicheModificationUE",$data);
            




        }
    }
    public function ListeEcparUe($id){
          //  echo $id;
          $where="where idue='".$id."' and etat=1";
          $tableau=$this->DAOGen->ReadGen("ec",$where);
          $data['classe']=$this->DAOGen->ReadGen("classe","where etat=1");
          for($i=0;$i<count($tableau);$i++){
              if($tableau[$i]->choix==1){
                $tableau[$i]->choix="<a href='".base_url()."ECcontrolleur/ListeChoice/".$tableau[$i]->idec."'   ?'><p style='color:blue;'> <i class='fa fa-check-circle' aria-hidden='true'></i></p></a>";
              }
              else{
                $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
              }
          }
          
          $data['result']=$tableau;
          $data['id']=$id;
        
          $this->load->view('admin/EcparClasse.php',$data);
    }
    
    public function ListeEcparUeExamen($id){
        //  echo $id;
        $where="where idue='".$id."' and etat=1";
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $tableau=$this->DAOGen->ReadGen("ec",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $listerattrapageec=$this->DAOGen->ReadGen("rattrapageecannee","where idanneescolaire=".$anneescolaire[0]->idanneescolaire);
        for($i=0;$i<count($tableau);$i++){
            $tableau2=$this->DAOGen->ReadGen("situationexamen","where idec=".$tableau[$i]->idec." and idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and (etat=5 or etat=10)");
          //  echo "where idec=".$tableau[$i]->idec." and idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and etat=5 or etat=3";
           // $xarray_filter($options,function($a){return $a->option_Type=='gender';})
            if(count($tableau2)>0){
                $tableau[$i]->choix="<i class='fa fa-check-circle' aria-hidden='true'></i>";
            }
              else{
                $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
              }
            
            
            
            /*
            
            if($tableau[$i]->choix==1){
              $tableau[$i]->choix="<a href=http://localhost/CommoStagiairemysql/ECcontrolleur/ListeChoice/".$tableau[$i]->idec."   ?'><p style='color:blue;'> <i class='fa fa-check-circle' aria-hidden='true'></i></p></a>";
            }
            else{
              $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
            }
            */
        
        }
        $data["idue"]=$id;
        $data['result']=$tableau;
        $data['id']=$id;
      
        $this->load->view('admin/EcparClasseExamen.php',$data);
        
    }
  
  public function ListeEcparUeExamenProf($id){
    $idannee="";
    $data["idue"]=$id;
    
    $where="where idue='".$id."' and etat=1";
  //  $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
    $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","order by idanneescolaire desc");
    if($this->input->post('anneescolaire')==null){
        $idannee=$anneescolaire[0]->idanneescolaire;
    }
    else{
        $idannee=$this->input->post('anneescolaire');
    }
    $tableau=$this->DAOGen->ReadGen("detailecsansprof",$where);
    $data['classe']=$this->DAOGen->ReadGen("classe",null);
            for($i=0;$i<count($anneescolaire);$i++){
                    $anneescolaire[$i]->debut=$anneescolaire[$i]->debut."-".$anneescolaire[$i]->fin;
            }
            $data['anneescolaire']=$anneescolaire;
    $listerattrapageec=$this->DAOGen->ReadGen("rattrapageecannee","where idanneescolaire=".$idannee);
    for($i=0;$i<count($tableau);$i++){
      
        $tableau2=$this->DAOGen->ReadGen("situationexamen","where idec=".$tableau[$i]->idec." and idanneescolaire=".$idannee."  and (etat=5 or etat=10)");
      //  echo "where idec=".$tableau[$i]->idec." and idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and etat=5 or etat=3";
      $id=$tableau[$i]->idec;
      $count=array_filter($listerattrapageec,function($a) use ($id){return $a->idec==$id and $a->etat>0;});
       if(count($count)>0){
        $tableau[$i]->etat="<i data-feather='check-circle'></i>";

       }
       else{
        // $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
       //  $tableau[$i]->etat="<a href='".base_url()."ECcontrolleur/VoirRattrapageEc/".$tableau[$i]->idec."'><p style='color:blue;'><i data-feather='x-circle'></i></p></a>";
       $tableau[$i]->etat="<i data-feather='x-circle'></i>";   
        }
     
      if(count($tableau2)>0){
       //     $tableau[$i]->choix="<i class='fa fa-check-circle' aria-hidden='true'></i>";
       $tableau[$i]->choix="<i data-feather='check-circle'></i>";
       
        }
          else{
           // $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
        //      $tableau[$i]->choix="<a href='".base_url()."ECcontrolleur/DetailExamenEc/".$tableau[$i]->idec."'><p style='color:blue;'><i data-feather='x-circle'></i></p></a>";
            $tableau[$i]->choix="<i data-feather='x-circle'></i>";    
        }
        $recherche=$this->DAOGen->ReadGen("ecanneesn","where idanneescolaire='".$idannee."' and idec='".$id."' and (etat is not null or etat>0)");
        if(count($recherche)>0){
            $tableau[$i]->idclasse="<i data-feather='check-circle'></i>";

        }else{
            $tableau[$i]->idclasse="<i data-feather='x-circle'></i>";

        }
        
        
        
        /*
        
        if($tableau[$i]->choix==1){
          $tableau[$i]->choix="<a href=http://localhost/CommoStagiairemysql/ECcontrolleur/ListeChoice/".$tableau[$i]->idec."   ?'><p style='color:blue;'> <i class='fa fa-check-circle' aria-hidden='true'></i></p></a>";
        }
        else{
          $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
        }
        */
    
    }
    $data['idannee']=$idannee;
    $data['result']=$tableau;
    $data['id']=$id;
    $data['button']="<a href=".base_url()."UEController/DetailExamen/".$data["idue"]."/".$idannee." class='btn btn-primary float-right'>Valider UE</a>";
    
  
    $this->load->view('Prof/EcparClasseExamenRes.php',$data);
    }
    public function ListeEcparUeExamenProfWithoutRecherche($id){
    
        $data["idue"]=$id;
        $where="where idue='".$id."' and etat=1";
      //  $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire",null);
        $tableau=$this->DAOGen->ReadGen("detailecsansprof",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
                for($i=0;$i<count($anneescolaire);$i++){
                        $anneescolaire[$i]->debut=$anneescolaire[$i]->debut."-".$anneescolaire[$i]->fin;
                }
                $data['anneescolaire']=$anneescolaire;
        $data['idannee']=null;
        $data['result']=[];
        $data['id']=$id;
        $data['button']='';
        
      
        $this->load->view('Prof/EcparClasseExamenRes.php',$data);
        }
    public function CreateUe(){
        $data['result']=$this->DAOGen->ReadGen("classe",null);
      //  $data['result']=$tableau;
        $this->load->view('admin/CreateUE.php',$data);
       // redirect('UEController/Recherche');
        
    }
    public function GetNotes($idclasse){
            $result=$this->DAOGen->ReadGen("anneescolaire",null);
            for($i=0;$i<count($result);$i++){
                    $result[$i]->debut=$result[$i]->debut."-".$result[$i]->fin;
            }
            $data['anneescolaire']=$result;
            $data['result']=[];
            $data['idclasse']=$idclasse;
            $data['idannee']=null;
       //     echo var_dump($data['anneescolaire']);
            $this->load->view('Prof/Liste',$data);
    }
    public function GetNotesEtudiant(){
        $result=$this->DAOGen->ReadGen("anneescolaire",null);
        for($i=0;$i<count($result);$i++){
                $result[$i]->debut=$result[$i]->debut."-".$result[$i]->fin;
        }
        $data['anneescolaire']=$result;
        $data['result']=null;
        $data['idclasse']=null;
   //     echo var_dump($data['anneescolaire']);
        $this->load->view('Etudiant/Liste',$data);
    }
    public function RechercheNote2(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $idclasse=$this->input->post('idclasse');
        $result=$this->DAOGen->ReadGen("anneescolaire",null);
        for($i=0;$i<count($result);$i++){
                $result[$i]->debut=$result[$i]->debut."-".$result[$i]->fin;
        }
        $data['anneescolaire']=$result;
        $data['result']=$this->DAOGen->RequeteFull("select totalueapres.*, ue.idue AS Expr1, ue.reference,ue.coefficient, ue.position FROM totalueapres INNER JOIN ue ON totalueapres.idue = ue.idue where idanneescolaire=".$this->input->post('anneescolaire')." and idutilisateur=".$u_rec_id);  //     echo var_dump($data['anneescolaire']);
        $resultRattrapage=$this->DAOGen->ReadGen("ratrrapagetotalue","where idanneescolaire=".$this->input->post('anneescolaire')." and idutilisateur=".$u_rec_id);
        for($i=0;$i<count($data['result']);$i++){
            if($data['result'][$i]->note<10){
                $idue=$data['result'][$i]->idue;
                $idutilisateur=$u_rec_id;
                $note=array_values(array_filter($resultRattrapage,function($a) use($idue,$idutilisateur){return $a->idutilisateur==$idutilisateur and $a->idue==$idue;}));
                if($note[0]->note>10){
                    $data['result'][$i]->note=$note[0]->note." APR";

                }
            }
                



        }    
        $this->load->view('Etudiant/Liste',$data);
 //  echo var_dump($data['result']);
    }


                         
    public function RechercheNote(){
        $idclasse=$this->input->post('idclasse');
        $result=$this->DAOGen->ReadGen("anneescolaire",null);
        for($i=0;$i<count($result);$i++){
                $result[$i]->debut=$result[$i]->debut."-".$result[$i]->fin;
        }
        $data['anneescolaire']=$result;
        $data['result']=$this->DAOGen->RequeteFull("select totalnoteexamen.*, utilisateur.nom, utilisateur.prenom FROM totalnoteexamen INNER JOIN utilisateur ON totalnoteexamen.idutilisateur = utilisateur.idutilisateur where idclasse=".$this->input->post('idclasse')." and idanneescolaire=".$this->input->post('anneescolaire'));
        $data['idclasse']=$idclasse;
        $data['idannee']=$this->input->post('anneescolaire');
        $data['courbe']=$this->MetierUE->RechercheNoteStat($idclasse,$this->input->post('anneescolaire'));
   //     echo var_dump($data['anneescolaire']);
        $this->load->view('Prof/Liste',$data);
 //  echo var_dump($data['result']);
    }
    public function RechercheNoteRetour($idclasse,$idanneescolaire){
        $result=$this->DAOGen->ReadGen("anneescolaire",null);
        for($i=0;$i<count($result);$i++){
                $result[$i]->debut=$result[$i]->debut."-".$result[$i]->fin;
        }
        $data['anneescolaire']=$result;
        $data['result']=$this->DAOGen->RequeteFull("select totalnoteexamen.*, utilisateur.nom, utilisateur.prenom FROM totalnoteexamen INNER JOIN utilisateur ON totalnoteexamen.idutilisateur = utilisateur.idutilisateur where idclasse=".$idclasse." and idanneescolaire=".$idanneescolaire);
        $data['idclasse']=$idclasse;
        $data['courbe']=$this->MetierUE->RechercheNoteStat($idclasse,$idanneescolaire);
        $data['idannee']=$idanneescolaire;
     
   //     echo var_dump($data['anneescolaire']);
        $this->load->view('Prof/Liste',$data);
 //  echo var_dump($data['result']);
    }
    public function DetailExamen($idue,$idanneescolaire){
   //    $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
      ///  echo var_dump($anneescolaire);
        $getidueannee=$this->DAOGen->ReadGen("ueannee","where idanneescolaire=".$idanneescolaire." and idue=".$idue);
        $detailue=$this->DAOGen->ReadGen("ue","where idue=".$idue);
     //   echo var_dump(" where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and idue=".$idue);
       
        $count=$this->DAOGen->RequeteFull("select count(idutilisateur) as nombre from inscription where idanneescolaire=".$idanneescolaire." and idclasse=".$detailue[0]->idclasse);
     
        //$this->DAOGen->RequeteFull("totaluesn""where idueannee=".$getidueannee[0]->idueannee);
        //$this->DAOGen->ReadGen("totaluexamen","where idueannee=".$getidueannee[0]->idueannee);


       // select sum(note)/6 as moyenne from totaluesn where idueannee=12;
      $moyennecontrole=$this->DAOGen->RequeteFull(" select sum(note)/".$count[0]->nombre."  as moyenne from totaluesn where idueannee=".$getidueannee[0]->idueannee);
      $moyenneexamen=$this->DAOGen->RequeteFull(" select sum(note)/".$count[0]->nombre." as moyenne from totaluexamen where idueannee=".$getidueannee[0]->idueannee);
      
      $sn = new stdClass; 
      $sn->nom = "Session Normale"; 
      $sn->type="";
      $sn->lien="<a href=".base_url()."UEController/detailCT/1/".$idue."/".$idanneescolaire."><i data-feather='book'></i></a>";
      $sn->check="<input type='radio' name='pseudo' value='ton_pseudo' disabled='disabled' checked /></a>";
      for($i=0;$i<count($moyenneexamen);$i++){
      $sn->moyenne=round($moyenneexamen[$i]->moyenne,2);
      }
      $tr=new stdClass;
      $tr->nom = "Controle Continue"; 
      $tr->type="";
      $tr->lien="<a href=".base_url()."UEController/detailCT/0/".$idue."/".$idanneescolaire."><i data-feather='book'></i></a>";
      $tr->check="<input type='checkbox' name='choix' value='1'>";
      for($i=0;$i<count($moyennecontrole);$i++){
        $tr->moyenne=round($moyennecontrole[$i]->moyenne,2);
        }
      $tableau=array();
      array_push($tableau,$sn,$tr);
      $data['tableau']=$tableau;
      $data['idue']=$idue;
      $data['idannee']=$idanneescolaire;
        
      
      
      
      
      
      
      $this->load->view("Prof/Proposition",$data);
        






    }
    public function Validationresponsable(){
       //     echo var_dump($this->input->post());
           
        
       try{
            $choix=0;
            if($this->input->post('choix')!=null){
                $choix=1;
            }    
        
        $this->load->model('MetierUE');
        $annee=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$this->input->post('idannee'));
        if($annee[0]->etat==10){
            throw new \Exception("Cette année est déjà fini");
        }
        else if($annee[0]->etat==0){
            throw new \Exception("Cette année n'a pas encore commence");   
        }
        $ue=$this->DAOGen->ReadGen("ue","where idue=".$this->input->post('idue'));
        $this->MetierUE->validationUESN($this->input->post('idue'),$annee[0]->idanneescolaire,$choix);
        $this->MetierUE->checkUEtotal($annee,$ue[0]->idclasse);
        redirect('UEController/DetailExamen/'.$ue[0]->idue."/".$this->input->post('idannee'));
        }catch(\Exception $e){
            $data['idannee']=$this->input->post('idannee');
            $data['error']=$e->getMessage();
            $idue=$this->input->post('idue');
            $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$this->input->post('idannee'));
            ///  echo var_dump($anneescolaire);
            $getidueannee=$this->DAOGen->ReadGen("ueannee","where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and idue=".$idue);
            $detailue=$this->DAOGen->ReadGen("ue","where idue=".$idue);            
            $count=$this->DAOGen->RequeteFull("select count(idutilisateur) as nombre from inscription where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and idclasse=".$detailue[0]->idclasse);
            $moyennecontrole=$this->DAOGen->RequeteFull(" select sum(note)/".$count[0]->nombre."  as moyenne from totaluesn where idueannee=".$getidueannee[0]->idueannee);
            $moyenneexamen=$this->DAOGen->RequeteFull(" select sum(note)/".$count[0]->nombre." as moyenne from totaluexamen where idueannee=".$getidueannee[0]->idueannee);
            
            $sn = new stdClass; 
            $sn->nom = "Session Normale"; 
            $sn->type="";
            $sn->lien="<a href=".base_url()."UEController/detailCT/1/".$idue."/".$this->input->post('idannee')."><i data-feather='book'></i></a>";
            $sn->check="<input type='radio' name='pseudo' value='ton_pseudo' disabled='disabled' checked /></a>";
            for($i=0;$i<count($moyenneexamen);$i++){
            $sn->moyenne=round($moyenneexamen[$i]->moyenne,2);
            }
            $tr=new stdClass;
            $tr->nom = "Controle Continu"; 
            $tr->type="";
            $tr->lien="<a href=".base_url()."UEController/detailCT/0/".$idue."/".$this->input->post('idannee')."><i data-feather='book'></i></a>";
            $tr->check="<input type='checkbox' name='choix' value='1'>";
            for($i=0;$i<count($moyennecontrole);$i++){
              $tr->moyenne=round($moyennecontrole[$i]->moyenne,2);
            
              }
            $tableau=array();
            array_push($tableau,$sn,$tr);
            $data['tableau']=$tableau;
            $data['idue']=$idue;
            $this->load->view("Prof/Proposition",$data);
        }
        
    
    }
    
    public function detailCT($id,$idue,$idanneescolaire){
        if($id==0){
            $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
            $getidueannee=$this->DAOGen->ReadGen("ueannee","where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and idue=".$idue);
            $detailue=$this->DAOGen->ReadGen("ue","where idue=".$idue);
            $data['idue']=$idue;
            $data['text']="Controle continue";
            $data['idanneescolaire']=$idanneescolaire;
        //    $data['result']=$this->DAOGen->ReadGen("totaluesn","where idueannee=".$getidueannee[0]->idueannee);
            $data['result']=$this->DAOGen->RequeteFull("select totaluesn.*, utilisateur.nom, utilisateur.prenom FROM totaluesn INNER JOIN utilisateur ON totaluesn.idutilisateur = utilisateur.idutilisateur where idueannee=".$getidueannee[0]->idueannee." order by totaluesn.note desc");
            $this->load->view("Prof/Notegen",$data);

            
        }else{
            $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where idanneescolaire=".$idanneescolaire);
            $getidueannee=$this->DAOGen->ReadGen("ueannee","where idanneescolaire=".$anneescolaire[0]->idanneescolaire." and idue=".$idue);
            $detailue=$this->DAOGen->ReadGen("ue","where idue=".$idue);
            $data['idue']=$idue;
            $data['text']="Examen session normale";
        //    $data['result']=$this->DAOGen->ReadGen("totaluexamen","where idueannee=".$getidueannee[0]->idueannee);
            $data['result']=$this->DAOGen->RequeteFull("select        utilisateur.nom, utilisateur.prenom, totaluexamen.* FROM utilisateur INNER JOIN totaluexamen ON utilisateur.idutilisateur = totaluexamen.idutilisateur where idueannee=".$getidueannee[0]->idueannee." order by totaluexamen.note desc");
            $data['idanneescolaire']=$idanneescolaire;
            $this->load->view("Prof/Notegen",$data);

        }
    }
    public function finirunue($idue){
        try{
        $this->load->model("MetierUE");
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $selectcl=$this->DAOGen->ReadGen("ue","where idue='".$idue."'");
        $distinct=$this->DAOGen->RequeteFull("select distinct idutilisateur from notestotalue where idue='".$idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $tailleEc=$this->DAOGen->ReadGen("ecannee","where idue='".$idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $selectUE=$this->DAOGen->ReadGen("ueannee","where idue='".$idue."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        $test=$this->MetierUE->Controle($tailleEc,$idue,$annee[0]->idanneescolaire);
        $this->MetierUE->UpdateExamen($tailleEc,$annee[0]->idanneescolaire);
        for($i=0;$i<count($distinct);$i++){    
            $selectNotes=$this->DAOGen->ReadGen("notestotalue","where idue='".$idue."' and idanneescolaire='".$annee[0]->idanneescolaire."' and idutilisateur='".$distinct[$i]->idutilisateur."'" );
            $total=array();
            for($ii=0;$ii<count($selectNotes);$ii++){
                array_push($total,$selectNotes[$ii]->total);
            }
            $data=array(
                "idutilisateur"=>$distinct[$i]->idutilisateur,
                "idueannee"=>$selectUE[0]->idueannee,
                "idanneescolaire"=>$annee[0]->idanneescolaire,
                "note"=>array_sum($total)/count($tailleEc),
            );
            $this->DAOGen->create($data,null,null,"totaluesn");
            if(array_sum($total)/count($tailleEc)>=10){
                $data2=array(
                    "idutilisateur"=>$distinct[$i]->idutilisateur,
                    "idueannee"=>$selectUE[0]->idueannee,
                    "idclasse"=>$selectcl[0]->idclasse,
                    "notes"=>array_sum($total)/count($tailleEc),
                    "idanneescolaire"=>$annee[0]->idanneescolaire,
                    "obtention"=>1
                );
                $this->DAOGen->create($data2,null,null,"TotalUE");

            }

        }
        $donnee=array(
            "etat"=>5
        );
        $where=array(
            "idue"=>$idue,
            "idanneescolaire"=>$annee[0]->idanneescolaire
        );
        $this->DAOGen->update($donnee,$where,"ueannee");
        redirect("UEController/ListeEcparUeExamen/".$idue);
        
    }catch(\Exception $e){
      //  echo $e->getMessage();
        $where="where idue='".$idue."' and etat=1";
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $tableau=$this->DAOGen->ReadGen("Ec",$where);
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        for($i=0;$i<count($tableau);$i++){
            $tableau2=$this->DAOGen->ReadGen("situationexamen","where idec=".$tableau[$i]->idec." and idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and (etat=5 or etat=10)");
          //  echo "where idec=".$tableau[$i]->idec." and idanneescolaire=".$anneescolaire[0]->idanneescolaire."  and etat=5 or etat=3";
            if(count($tableau2)>0){
                $tableau[$i]->choix="<i class='fa fa-check-circle' aria-hidden='true'></i>";
            }
              else{
                $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
              }
            
            
            
            /*
            
            if($tableau[$i]->choix==1){
              $tableau[$i]->choix="<a href=http://localhost/CommoStagiairemysql/ECcontrolleur/ListeChoice/".$tableau[$i]->idec."   ?'><p style='color:blue;'> <i class='fa fa-check-circle' aria-hidden='true'></i></p></a>";
            }
            else{
              $tableau[$i]->choix="<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
            }
            */
        }
        $data["idue"]=$idue;
        $data['result']=$tableau;
        $data['id']=$idue;
        $data['error']=$e->getMessage();
 //       echo $e->getMessage();
        $this->load->view('admin/EcparClasseExamen',$data);

        

    }
}
    public function UESubmit(){
            $nom=$this->input->post('nom');
            try{
            $ue=new UE('',$this->input->post('nom'),$this->input->post('choix'));
            $chiffre = $this->input->post('coefficient');
            $pattern2 = "/^(?=\d*[1-9])\d+$/";
            $response=preg_match($pattern2, $chiffre);
            if($response==0){
                throw new \Exception("Veuillez bien taper le coefficient");

            }
            $data=array(
              'reference'=>$this->input->post('nom'),
               'idclasse'=>$this->input->post('choix'),
                'coefficient'=>$chiffre
            );
            $this->DAOGen->create($data,null,null,"ue");            
             
            redirect('UEController/Rechercheavecondition/'.$this->input->post('choix'));    
            }catch(\Exception $e){
                $data['result']=$this->DAOGen->ReadGen("classe",null);
                  $data['error']=$e->getMessage();
                  $this->load->view('admin/CreateUE.php',$data);
                

            }
    }
    public function GetClasse(){
        $listeclasse=$this->DAOGen->ReadGen("classe",null);
        $test =new Jsend ("success","Ok",$listeclasse);   
        echo json_encode($test);

    }
    public function GetParUE(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($_POST);
        $iclasse=$this->input->post('idclasse');
        $where="where idclasse='".$iclasse."'";
        $listeclasse=$this->DAOGen->ReadGen("ue",$where);
        for($i=0;$i<count($listeclasse);$i++){

                $listeclasse[$i]->reference="UE".$listeclasse[$i]->position." ".$listeclasse[$i]->reference;



        }
        $test =new Jsend ("success","Ok",$listeclasse);   
        echo json_encode($test);

    }
    public function GetParUEProfs(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        //echo json_encode($_POST);
        $u_rec_id = $this->session->userdata('idutilisateur');
        $idclasse=$this->input->post('idclasse');
        $where="where idclasse='".$idclasse."'";
        $listeclasse=$this->DAOGen->RequeteFull("select distinct(idue),reference,position from detailec where idclasse='".$idclasse."' and idutilisateur='".$u_rec_id."'");
        for($i=0;$i<count($listeclasse);$i++){

                $listeclasse[$i]->reference="UE".$listeclasse[$i]->position." ".$listeclasse[$i]->reference;



        }
        $test =new Jsend ("success","Ok",$listeclasse);   
        echo json_encode($test);

    }
    /*
    public function finirsemestre($idclasse,$rang,$idanneescolaire){
        try{
            ///exception fonction call
            $this->load->model('MetierUE');
            $this->MetierUE->controleSemestre($idclasse,$rang,$idanneescolaire);
            $select=$this->DAOGen->ReadGen("semestreannee","where idanneescolaire='".$idanneescolaire."' and rang='".$rang."' and idclasse='".$idclasse."'");
            $x=array(
                "etat"=>5
            );
            $colonne=array(
                "idsemestreannee"=>$select[0]->idsemestreannee
            );
            $this->DAOGen->update($x,$colonne,"semestreannee");
            if($rang==1){
                $select2=$this->DAOGen->ReadGen("semestreannee","where idanneescolaire='".$idanneescolaire."' and rang='2' and idclasse='".$idclasse."'");
                $update=array(
                    "etat"=>1
                );
                $colonne=array(
                    "idsemestreannee"=>$select2[0]->idsemestreannee
                );
                $this->DAOGen->update($update,$colonne,"ueannee");

                $update2=array(
                    "etat"=>1
                );
                $colonne2=array(
                    "idsemestreannee"=>$select2[0]->idsemestreannee
                );
                $this->DAOGen->update($update,$colonne,"semestreannee");
                $data['classe']=$this->DAOGen->ReadGen("classe",null);
                $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
                $annee[0]->debut=$annee[0]->debut."-".$annee[0]->fin;
                $data['annee']=$annee;
                $this->load->view('admin/DetailAnnee',$data);

            
            }
        }catch(\Exception $e){
            $data['error']=$e->getMessage();
            $data['classe']=$this->DAOGen->ReadGen("classe",null);        
            $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
             $annee[0]->debut=$annee[0]->debut."-".$annee[0]->fin;
             $data['annee']=$annee;
             $this->load->view('admin/DetailAnnee',$data);
     
     



        }
    }
    */
    public function validationRatttrapage(){
        
    }
}
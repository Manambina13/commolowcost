<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\UE;
use App\models\Jsend;
class SemestreController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function Calcul($idsemestre){
        $semestre=$this->DAOGen->ReadGen("semestre","where idsemestre='".$idsemestre."'");
        $annee=$this->DAOGen->ReadGen("annee","where etat=1");
        $semestreAnnee=$this->DAOGen->ReadGen("semestreannee","where etat=1 and idsemestre='".$semestre[0]->idsemestre."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
        try{
        if(count($semestreAnnee)==0){
            $error="Semestre Deja Fini";
            $this->load->view();
        }else{
            
            $whereUefini=$this->DAOGen->ReadGen("ueannee","where idanneescolaire='".$annee[0]->idanneescolaire."' and etat=5 and idsemestreannee='".$semestreAnnee[0]->idsemestreannee."'");
            $whereUeTotal=$this->DAOGen->ReadGen("ueannee","where idanneescolaire='".$annee[0]->idanneescolaire."' and idsemestreannee='".$semestreAnnee[0]->idsemestreannee."'");
            if(count($whereUefini)!=count($whereUeTotal)){
                $error="Certains des UE n'es pas encore fini";
                $this->load->view();
            }else {
                if($semestre[0]->rang==1){
                    $update=array(
                        "etat"=>5
                    );
                    $id=array(
                        "idsemestreannee"=>$semestreAnnee[0]->idsemestreannee
                    );
                    $this->DAOGen->update($update,$id,"semestreannee");



                }else{
                    $listeEleve=$this->DAOGen->ReadGen("inscription","where idclasse='".$semestre[0]->idclasse."' and idanneescolaire='".$annee[0]->idanneescolaire."'");
                    for($i=0;$i<count($listeEleve);$i++){
                        $where1="where idutilisateur='".$listeEleve[$i]->idutilisateur."' and idanneescolaire='".$annee[0]->idanneescolaire."'";
                        $selectparUtilisateur=$this->DAOGen->ReadGen("notesue",$where1);
                        $this->load->model("UEmetier");
                        $notesTotal=$this->UEMetier->CalculeNotes($selectparUtilisateur);
                        $data=array(
                            "idclasse"=>$semestre[0]->idclasse,
                            "idanneescolaire"=>$annee[0]->idscolaire,
                            "idutilisateur"=>$listeEleve[0]->idutilisateur,
                            "resultat"=>$notesTotal
                        );
                        $this->DAOGen->create($data,null,null,"parcours");
                                                        }
                        }
                     }
                }
            }catch(\Exception $e){

                $data['error']=$e->getMessage();
                $data['classe']=$this->DAOGen->ReadGen("classe",null);
                $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
                 $annee[0]->debut=$annee[0]->debut."-".$annee[0]->fin;
                 $data['annee']=$annee;
                 $this->load->view('admin/DetailAnnee',$data);



            }
        }
    }

<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProfController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DAOGen');
        $this->load->helper(array('form','url'));
       }
    public function index(){
        $id=$this->session->userdata('idutilisateur');
       // $data['result']=$this->DAOGen->ReadGen('fichierformateur',"where etat=1 and idformateur=".$id);
       $data['result']=$this->DAOGen->RequeteFull(" select fichierformateur.*,matiere.nom_matiere,ec.nom_ec from ec join matiere on ec.idmatiere=matiere.idmatiere join fichierformateur on ec.idec=fichierformateur.idec and matiere.idmatiere=fichierformateur.idmatiere where fichierformateur.etat=1 and fichierformateur.idformateur=".$id);
       $this->load->view('Prof/listcours',$data);
    }
	public function agenda(){
		
		
        $this->load->view('Admin/agenda');
	}
    public function submitdelete(){
        $idformateur=$this->input->post('id');
        $update=array(
            "etat"=>0
        );
        $id=array(
            "idfichier"=>$idformateur
        );
        $this->DAOGen->update($update,$id,"fichierformateur");
      
        $age = array(
            "status"=>"success", 
            "message"=>"ok"
        );
        echo json_encode($age);


    }
    public function addcours(){
        $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
        $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
        $this->load->view('Prof/addcours',$data);
    }
    public function rechercheparannee(){
         echo $this->input->post('myselect');
         
    }
    public function listecouretudiant(){
        
        $idutilisateur= $this->session->userdata('idutilisateur');
        $data['annee']=$this->DAOGen->RequeteFull('SELECT idanneeuniversitaire, CONCAT(debut, "-", fin) AS annee FROM anneeuniversitaire');
        $data['result']=[];
        if(count($data['annee'])>0){
            $data['result']=$this->DAOGen->ReadGen('listefichieretudiant',"where idanneeuniversitaire='".$data['annee'][0]->idanneeuniversitaire."' and idformateur=".$idutilisateur);
        }
        $this->load->view('Prof/listdevoir',$data);
    }
    public function do_upload(){
        $idec=$this->DAOGen->changement($this->input->post('ec'));
        $idue=$this->DAOGen->changement($this->input->post('ue'));
           // echo $this->input->post('ec');
           // $idec=$this->DAOGen->changement($this->input->post('ec'));
           // echo $idec;
           
                $config['upload_path'] = 'upload/';
                $config['allowed_types']        = 'pdf|docx|xlsv';
            //	$config['max_size']             = 100;
        //		$config['max_width']            = 1024;
        //		$config['max_height']           = 768;
    
                $this->load->library('upload', $config);
    
    // Alternately you can set preferences by calling the ``initialize()`` method. Useful if you auto-load the class:
    $this->upload->initialize($config);


                if ($this->upload->do_upload('profilePic')){
                    
                    $createdby  =   $this->session->userdata('idRestaurant');
                    $data = $this->upload->data();
                        $picture = array(
                            'photoPath' => $this->upload->data('full_path').$data['file_name']
                        );
                        //echo 'upload/'.$data['file_name'];
                        $idec=$this->DAOGen->changement($this->input->post('ec'));
                        $idue=$this->DAOGen->changement($this->input->post('ue'));
                        $idutilisateur= $this->session->userdata('idutilisateur');
                        $fako='upload/'.$data['file_name'];
                        $data = array( 
                            'idniveau'=>$this->DAOGen->changement($this->input->post('niveau')),
                            'idparcour'=>$this->DAOGen->changement($this->input->post('parcour')),
                            'idformateur'=>$idutilisateur,
                            'idmatiere'=>$idue,
                            'idec'=>$idec,
                            'idtypefichier'=>'1',
                            'nom'=>$this->input->post('nom'),
                            'etat'=>1,
                            'lien'=>$fako
                         ); 
                         $this->DAOGen->Create($data,null,null,"fichierformateur");
                         redirect('Prof/ProfController/index');  
                        }
                      
                
                else{
                    $data['error']=$this->upload->display_errors();
                    $data['parcour']=$this->DAOGen->ReadGen("parcour","where etat=1");
                    $data['niveau']=$this->DAOGen->ReadGen("niveau",null);
                    $this->load->view('Prof/addcours',$data);
                      
                }
                
            
            }
                
            public function updatecours(){
                $this->load->view('Prof/editcours');
            }   
               
        }
        
    
      
    
    

 


?>
<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\Jsend;
class Niveaucontrolleur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('form','url'));
        $this->load->model("DAOGen");
        $this->load->library("pagination");
        $this->load->library("session");
        $this->load->model("MetierMessage");
        $this->load->model('ExtensionsModel');
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function AjoutNiveau(){
        $data['result']=$this->DAOGen->ReadGen("classe",null);
        $this->load->view("admin/Creerniveau",$data);
    }
    public function Supprimer($idclasse){
        try{
        $update=array(
            "etat"=>0
        );
        $where=array(
            "idclasse"=>$idclasse
        );
          $test=$this->DAOGen->ReadGen("classe","where suivant=".$idclasse." or precedent=".$idclasse);
           if(count($test)>0){
               throw new \Exception("Ce niveau est relié à un ou plusieurs autres niveaux");
           }
           $this->DAOGen->update($update,$where,"classe");
        redirect('Niveaucontrolleur/AffichageNiveau');
        }catch(\Exception $e){
            $result=$this->DAOGen->ReadGen("classe","where etat=1");
            for($i=0;$i<count($result);$i++){
                if($result[$i]->suivant!=null){
                    $suivant=$result[$i]->suivant;
                    $value=array_values(array_filter($result,function($a) use ($suivant){return $a->idclasse==$suivant;}));
                    $result[$i]->suivant=$value[0]->classe;
                }
                if($result[$i]->precedent!=null){
                    $precedent=$result[$i]->precedent;
                    $value=array_values(array_filter($result,function($a) use ($precedent){return $a->idclasse==$precedent;}));
                    $result[$i]->precedent=$value[0]->classe;
                }
            }
            $data['error']=$e->getMessage();
            $data['result']=$result;
            $this->load->view("admin/Niveau",$data);
              

        }
    }
    public function AffichageNiveau(){
        $result=$this->DAOGen->ReadGen("classe","where etat=1");
        for($i=0;$i<count($result);$i++){
            if($result[$i]->suivant!=null){
                $suivant=$result[$i]->suivant;
                $value=array_values(array_filter($result,function($a) use ($suivant){return $a->idclasse==$suivant;}));
                $result[$i]->suivant=$value[0]->classe;
            }
            if($result[$i]->precedent!=null){
                $precedent=$result[$i]->precedent;
                $value=array_values(array_filter($result,function($a) use ($precedent){return $a->idclasse==$precedent;}));
                $result[$i]->precedent=$value[0]->classe;
            }
        }
        $data['result']=$result;
        $this->load->view("admin/Niveau",$data);
    }
    public function Create(){
        try{
        $suivant=$this->input->post('suivant');
        $precedent=$this->input->post('precedent');
        $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        if(count($anneescolaire)>0){
            throw new \Exception("L'année universitaire a déjà débuté donc il est impossible de créer un nouveau niveau"); 
        }
        if($suivant==0){
            $suivant=null;
        }
        if($precedent==0){
            $precedent=null;
        }

        $nom=$this->input->post('nom');
        if($nom==null){
            throw new \Exception("Veuillez compléter le nom");   
        }
        if($suivant==$precedent){
            if($suivant!=null && $precedent!=null){
            throw new \Exception("Mêmes niveaux sélectionnés");
            }
        }
        $check=$this->DAOGen->ReadGen("classe","where classe='".strtoupper($nom)."'");
        if(count($check)>0){
            throw new \Exception("Ce niveau a déjà été ajoutée");
        }
        $ajout=array(
            "classe"=>strtoupper($nom),
            "precedent"=>$precedent,
            "suivant"=>$suivant,
            "etat"=>1,
            "couleur"=> $this->input->post('couleur')
        );
        $this->DAOGen->create($ajout,null,null,"classe");
        }catch(\Exception $e){
             $data['error']=$e->getMessage();
             $data['result']=$this->DAOGen->ReadGen("classe",null);
             $this->load->view("admin/Creerniveau",$data);

        }
        
    }
    public function ficheModif($idclasse){
        $data['fiche']=$this->DAOGen->ReadGen("classe","where idclasse=".$idclasse);
        $result=$this->DAOGen->ReadGen("classe","where etat=1");
        $precedentliste="";
        $suivantliste="";
        for($i=0;$i<count($result);$i++){
                if($result[$i]->idclasse==$data['fiche'][0]->suivant){
                   // $data['fiche'][0]->idclasse= '<option value="'.$result[$i]->idclasse."'>".$result[$i]->classe."</option>'";
                    $suivantliste=$suivantliste."<option value='".$result[$i]->idclasse."'selected>".$result[$i]->classe."</option>";

                }else{
                    $suivantliste=$suivantliste."<option value='".$result[$i]->idclasse."'>".$result[$i]->classe."</option>";
                }
        }
        for($i=0;$i<count($result);$i++){
            if($result[$i]->idclasse==$data['fiche'][0]->precedent){
               // $data['fiche'][0]->idclasse= '<option value="'.$result[$i]->idclasse."'>".$result[$i]->classe."</option>'";
                $precedentliste=$precedentliste."<option value='".$result[$i]->idclasse."'selected>".$result[$i]->classe."</option>";
               
            }else{
                $precedentliste=$precedentliste."<option value='".$result[$i]->idclasse."'>".$result[$i]->classe."</option>";
            }
        }
        //echo var_dump($precedentliste);
        $data['idclasse']=$idclasse;
        $data['fiche'][0]->suivant=$suivantliste;
        $data['fiche'][0]->precedent=$precedentliste;
        $this->load->view('admin/modifniveau',$data);
    }
    public function Modifier($idclasse){
        try{
        $suivant=$this->input->post('debut');
        $precedent=$this->input->post('fin');
        $nom=$this->input->post('nom');
        if($nom==null){
            throw new \Exception("Veuillez compléter le nom");   
        }
        if($suivant==$precedent){
            if($suivant!=0 && $precedent!=0){
                throw new \Exception("Mêmes niveaux sélectionnés");
            }
        }
        $check2=$this->DAOGen->ReadGen("classe","where idclasse=".$idclasse);
        if($nom!=$check2[0]->classe){
            $check=$this->DAOGen->ReadGen("classe","where classe='".strtoupper($nom)."'");
            if(count($check)>0){
                throw new \Exception("Ce niveau a déjà été ajoutée");
            }
        }
        $where=array(
            "idclasse"=>$idclasse
        );
        if($suivant==0){
            $suivant=null;
        }
        if($precedent==0){
            $precedent=null;
        }
        $ajout=array(
            "classe"=>strtoupper($nom),
            "precedent"=>$precedent,
            "suivant"=>$suivant,
            "etat"=>1,
            "couleur"=>$this->input->post('couleur')
        );
        $this->DAOGen->update($ajout,$where,"classe");
        redirect('Niveaucontrolleur/AffichageNiveau');
        }catch(\Exception $e){
        $data['idclasse']=$idclasse;
        $data['error']=$e->getMessage();
        $data['fiche']=$this->DAOGen->ReadGen("classe","where idclasse=".$idclasse);
        $result=$this->DAOGen->ReadGen("classe","where etat=1");
        $precedentliste="";
        $suivantliste="";
        for($i=0;$i<count($result);$i++){
                if($result[$i]->idclasse==$data['fiche'][0]->suivant){
                   // $data['fiche'][0]->idclasse= '<option value="'.$result[$i]->idclasse."'>".$result[$i]->classe."</option>'";
                    $suivantliste=$suivantliste."<option value='".$result[$i]->idclasse."'selected>".$result[$i]->classe."</option>";

                }else{
                    $suivantliste=$suivantliste."<option value='".$result[$i]->idclasse."'>".$result[$i]->classe."</option>";
                }
        }
        for($i=0;$i<count($result);$i++){
            if($result[$i]->idclasse==$data['fiche'][0]->precedent){
               // $data['fiche'][0]->idclasse= '<option value="'.$result[$i]->idclasse."'>".$result[$i]->classe."</option>'";
                $precedentliste=$precedentliste."<option value='".$result[$i]->idclasse."'selected>".$result[$i]->classe."</option>";
               
            }else{
                $precedentliste=$precedentliste."<option value='".$result[$i]->idclasse."'>".$result[$i]->classe."</option>";
            }
        }
        $data['fiche'][0]->suivant=$suivantliste;
        $data['fiche'][0]->precedent=$precedentliste;
        $this->load->view('admin/modifniveau',$data);
        }
    }

}
    



    
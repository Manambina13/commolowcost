<?php defined('BASEPATH') OR exit('No direct script access allowed');
use App\models\UtilisateurClasse;
use App\models\Jsend;
class Utilisateur extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    //    $this->load->library("pagination");
      //  $this->load->model('MetierUtilisateur');
       // $this->load->model('DAOGen');
        //$this->load->helper(array('form','url'));
     //   $this->load->model("DAOUsers");
    // $this->load->model('DAOGen');
        header('Cache-Control:no-cache');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length,application/json,text/plain, Accept-Encoding");
    }
    public function ProfBio(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $data['bio']=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$u_rec_id);
        $this->load->view('Prof/Editprof',$data);
    }
   
    public function getNomUtilisateur(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $utilisateur=$this->DAOGen->RequeteFull("select*from utilisateur where idutilisateur=".$u_rec_id);
        $users= new stdClass; 
        $users->name =ucfirst($utilisateur[0]->prenom);     
        $test =new Jsend ("success","Ok",$users);   
           echo json_encode($test);
    }
    public function Login(){
        $this->load->view('Login');
    }
    public function getDashboard(){
        $anneescolaire=$this->DAOGen->RequeteFull("select idanneescolaire,CONCAT(debut,'-',fin) as debut FROM anneescolaire order by idanneescolaire desc");
       // $data['listeEleve']=$this->DAOGen->RequeteFull("select utilisateur.nom,utilisateur.email, utilisateur.prenom, inscription.*, classe.classe FROM utilisateur INNER JOIN inscription ON utilisateur.idutilisateur = inscription.idutilisateur INNER JOIN classe ON inscription.idclasse = classe.idclasse where idanneescolaire=1");
       if(count($anneescolaire)>0){
            $data['listeEleve']=$this->DAOGen->RequeteFull("
            SELECT *,
            CASE
            WHEN statut = 0 THEN 'Redoublant'
            WHEN statut = 1 THEN 'Passant' 
            END as info
            FROM inscriptionfull where idanneescolaire=1");
            $data['anneescolaire']=$anneescolaire;    
            $this->load->view("admin/admindash",$data);
       }else{


       }
    }
    public function CalendarSecr(){
        $data['result']=$this->DAOGen->ReadGen("classe","where etat=1");
        $this->load->view("admin/calendar",$data);
    }
    public function CalendarProf(){
        $data['result']=$this->DAOGen->ReadGen("classe","where etat=1");
        $this->load->view("Prof/calendar",$data);
    }
    public function  CalendarEtudiant(){
        $this->load->view("Etudiant/calendar");
    }
   
    public function process(){
        $email=$this->input->post('email');
        $pass=$this->input->post('pass');
       // echo $email." ".$pass;
        $where="Where email='".trim($email)."' and motdepasse='".trim($pass)."' and etat=1";
     ///   echo $where;
        $this->load->model('DAOGen');
        $login=$this->DAOGen->ReadGen("utilisateur",$where);
        if(count($login)>0){
            if($login[0]->idprofile=='1'){
            $session_data = array(  
                'idutilisateur'     =>     $login[0]->idutilisateur,
                'idprofile' =>$login[0]->idprofile
               );
               $this->session->set_userdata($session_data);
               redirect('Utilisateur/getDashboard');
            }else if($login[0]->idprofile=='3'){
                $session_data = array(  
                    'idutilisateur'     =>     $login[0]->idutilisateur,
                    'idprofile' =>$login[0]->idprofile
                   );
                   $this->session->set_userdata($session_data);
                 //  $data['dataPoints']=$dataPoints;               
                  // $this->load->view('Prof/index',$data);   
                    redirect('Utilisateur/getDashProf/0');
                    


            }
            else if($login[0]->idprofile=='2'){
                $session_data = array(  
                    'idutilisateur'     =>     $login[0]->idutilisateur,
                    'idprofile' =>$login[0]->idprofile
                   );

                   $this->session->set_userdata($session_data);
                   $this->load->view('Etudiant/index');
            }
        }else{
            $data['error']="Utilisateur Inexistant";
            $this->load->view('Login',$data);
        }
       
    }
    ///Admin
    public function getListeProf(){
        /*
        $this->load->model('DAOGen');
        $where="where idprofile ='PR3'";
        $eleve=$this->DAOGen->ReadGen("utilisateur",$where);
        */
        $this->load->model('DAOUtilisateur');
        $config = array();
        $config["base_url"] = base_url() . "Utilisateur/getListeProf";
        $config["total_rows"] = $this->DAOUtilisateur->countProf();
        $config["per_page"] = 14;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['detailec']=$this->DAOGen->ReadGen("detailec",null);
        $data["links"] = $this->pagination->create_links();
        $data['result'] = $this->DAOUtilisateur->LimitProf($config["per_page"], $page);
       // $data['result']=$eleve;
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $this->load->view('admin/ListeProf',$data);

    }
    public function ListeMatiereuchoix(){
        //$anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1");
        $u_rec_id = $this->session->userdata('idutilisateur');
        $lastInscription=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");    
      //  $lastInscription=$this->DAOGen->ReadGen("inscription","where idanneescolaire=".$anneescolaire[0]->etat." and idutilisateur=".$u_rec_id);    
        $getEC=$this->DAOGen->RequeteFull("select ec.*, ue.idclasse,ue.reference FROM ec INNER JOIN ue ON ec.idue = ue.idue where choix>0 and idclasse=".$lastInscription[0]->idclasse);
        $getChoice=$this->DAOGen->RequeteFull("select matiereauchoix.*, ue.idclasse FROM matiereauchoix INNER JOIN ec ON matiereauchoix.idec = ec.idec INNER JOIN ue ON ec.idue = ue.idue where idclasse=".$lastInscription[0]->idclasse);
  //      echo var_dump($getChoice);
        for($i=0;$i<count($getEC);$i++){
            $idec=$getEC[0]->idec;

            $matiereauchoix=array_values(array_filter($getChoice,function($a) use ($idec){return $a->idec==$idec;}));
    //        echo var_dump($matiereauchoix);
            $getEC[$i]->choix="<select name='".$getEC[0]->idec."'>";
            for($j=0;$j<count($matiereauchoix);$j++){
                $getEC[$i]->choix=$getEC[$i]->choix." "."<option value='".$matiereauchoix[$j]->idmatiereauchoix."'>".$matiereauchoix[$j]->matiere."</option>";
            
            }
            $getEC[$i]->choix= $getEC[$i]->choix."</select>";
        }
        $data['result']=$getEC;
        $this->load->view('Etudiant/ChoixInscription',$data);

      
    }
    public function getListeEleve(){
        $this->load->model('DAOUtilisateur');
        $config = array();
        $config["base_url"] = base_url() . "Utilisateur/getListeEleve";
        $config["total_rows"] = $this->DAOUtilisateur->countEleve();
        $config["per_page"] = 15;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $data['result'] = $this->DAOUtilisateur->LimitEleve($config["per_page"], $page);
        
        
        //
        $this->load->model('DAOGen');
    //    $where="where idprofile ='PR2'";
     //  $eleve=$this->DAOGen->ReadGen("utilisateur",$where);
       // $data['result']=$eleve;
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $this->load->view('admin/ListeEleve',$data);
    }
    public function Ajout(){
        $this->load->model('DAOGen');
        $data['result']=$this->DAOGen->ReadGen("classe",null);
   //     echo var_dump($data["result"]);
        $this->load->view('admin/CreateUtilisateur',$data);
    }
    public function Deconnexion(){
        $this->session->session_destroy;
        $this->session->unset_userdata('Utilisateur');
        $this->load->view('Login');

    }
    public function AjoutProf(){
        $this->load->model('DAOGen');
        $data['result']=$this->DAOGen->ReadGen("classe",null);
   //     echo var_dump($data["result"]);
        $this->load->view('admin/AjoutProf',$data);
    }
    public function ModifierProfC(){
           
     //       echo var_dump($this->input->post()); 
            try{
          
                $where2="where idutilisateur='".$this->input->post('idutilisateur')."'";
                $test=$this->DAOGen->ReadGen("utilisateur",$where2);
                ///function __construct1($idUtilisateur,$idProfile,$Nom,$Prenom,$MotDePasse,$Date,$Numero,$email) 
                $utilisateur=new UtilisateurClasse($this->input->post('nom'),$this->input->post('prenom'),$this->input->post('email'),$this->input->post('date'),$test[0]->email);
                $modifier=array(
                    "nom"=>$this->input->post('nom'),
                    "prenom"=>$this->input->post('prenom'),
                    "date"=>$this->input->post('date'),
                    "numero"=>$this->input->post('numero'),
                    "email"=>$this->input->post('email')
                );
                $where=array(
                    "idutilisateur"=>$this->input->post('idutilisateur')
                );
                $this->DAOGen->update($modifier,$where,"utilisateur");
             //   $this->load->view('Prof/index'); 
             redirect('Utilisateur/getDashProf/0');
            }catch(\Exception $e){
                $data['error']=$e->getMessage();
                $u_rec_id = $this->session->userdata('idutilisateur');
                $data['bio']=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$u_rec_id);
                $this->load->view('Prof/Editprof',$data);




            }
            
    }
    public function Getdashannee(){
        redirect('Utilisateur/getDashProf/'.$this->input->post('sel'));

    }
    public function insertionProf(){
        $this->load->model('DAOGen');
        $result=$this->DAOGen->ReadGen("Classe",null);
        try{
        $utilisateur=new UtilisateurClasse('','',$this->input->post('nom'),$this->input->post('prenom'),'',$this->input->post('date'),$this->input->post('numero'),$this->input->post('email'));
        $sequence=$this->DAOGen->getSequence("idutilisateur","utilisateur");
        $id=$sequence;
        $result1 = uniqid();
        $x=array(
            "idutilisateur"=>$id->sequence,
            "idprofile"=>'3',
            "nom"=>$this->input->post('nom'),
            "prenom"=>$this->input->post('prenom'),
            "email"=>$this->input->post('email'),
            "date"=>$this->input->post('date'),
            "numero"=>$this->input->post('numero'),
            "motdepasse"=>$result1,
            "etat"=>1
        );
       
       // $this->DAOGen->create($x,'idutilisateur','UT','utilisateur');
       $this->load->model('MetierUtilisateur');  
       $this->DAOGen->create($x,null,null,"utilisateur");
        // $this->MetierUtilisateur->sendEmail($this->input->post('nom'),$result1,$this->input->post('email'),$this->input->post('prenom'));
         $where="where etat=1";
        $annee=$this->DAOGen->ReadGen("anneescolaire",$where);
        
        /*
        $x=array(
            'idanneescolaire'=>$annee[0]->idanneescolaire,
            'idclasse'=>$this->input->post('choix'),
            'idutilisateur'=>$id,
            'statut'=> '1',
            'annee'=>$annee[0]->debut,
            'etat'=>1
        );
        */
        redirect("Utilisateur/getListeProf");
        }catch(Exception $e ){
            $data['result']=$result;
            $data['error']=$e->getMessage();
       //     echo var_dump($data["result"]);
            $this->load->view('admin/AjoutProf',$data);

        }
    
        
      //  $this->load->view('admin/CreateUtilisateur',$data);
    }

    public function Assignation($id){
        $data['result']=$id;
        $this->load->view("admin/Assignation",$data);

    }
    public function send() {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com', 
            'smtp_port' => 465,
            'smtp_user' => 'manambinarazafindralambo@gmail.com',
            'smtp_pass' => '13NOVEMBRE',
        
            'mailtype' => 'text',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );


        $this->load->library('email');
        $this->email->initialize($config);
       // $from = $this->config->item('smtp_user');
        $to = "manambina1@yahoo.fr";
        $subject = "Entree dans la";
        $message = "Kozy";
        
        $this->email->set_newline("\r\n");
        $this->email->from('manambinarazafindralambo@gmail.com','admin');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            echo 'Your Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
    }
    public function insertionEtudiant(){
        $this->load->model('DAOGen');
        $result=$this->DAOGen->ReadGen("Classe",null);
        try{
        $utilisateur=new UtilisateurClasse('','',$this->input->post('nom'),$this->input->post('prenom'),'',$this->input->post('date'),$this->input->post('numero'),$this->input->post('email'));
        $sequence=$this->DAOGen->getSequence("idutilisateur","utilisateur");
        $id=$sequence->sequence;
        $result = uniqid(); 
        $x=array(
            "idutilisateur"=>$id,
            "idprofile"=>'2',
            "nom"=>$this->input->post('nom'),
            "prenom"=>$this->input->post('prenom'),
            "email"=>$this->input->post('email'),
            "date"=>$this->input->post('date'),
            "motdepasse"=>$result,
            "etat"=>1,
            "numero"=>$this->input->post('numero')
        );
        
       $this->load->model('MetierUtilisateur');
    //   $this->MetierUtilisateur->sendEmail($this->input->post('nom'),$result,$this->input->post('email'),$this->input->post('prenom'));
              // $this->DAOGen->create($x,'idutilisateur','UT','utilisateur');
         $this->DAOGen->create($x,null,null,"utilisateur");
        $where="where etat=1";
        $annee=$this->DAOGen->ReadGen("anneescolaire",$where);
        
        $x=array(
           // 'idanneeScolaire'=>$annee[0]->idanneescolaire,
            'idclasse'=>$this->input->post('choix'),
            'idutilisateur'=>$id,
            'statut'=> '1',
          //  'annee'=>$annee[0]->debut,
           // 'etat'=>1
        );
        $this->DAOGen->create($x,null,null,"inscription");
        redirect("Utilisateur/getListeEleve");
        }catch(Exception $e ){
            $data['result']=$result;
            $data['error']=$e->getMessage();
       //     echo var_dump($data["result"]);
            $this->load->view('admin/CreateUtilisateur',$data);

        }
    
        
      //  $this->load->view('admin/CreateUtilisateur',$data);
    }
    public function AssignationMatiere(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        
        $idclasse=$this->input->post('idclasse');
        $idue=$this->input->post('idue');
        $idec=$this->input->post('idec');
        $idutilisateur=$this->input->post('idutilisateur');
        
        $data=array(
            "idutilisateur"=>$idutilisateur
        );
        $condition=array(
            "idec"=>$idec
        );
        $this->DAOGen->update($data,$condition,"ec");
        
        $test =new Jsend ("success","Ok","");   
     //   echo json_encode($test);

        
    }
    public function ModifierEleve($id){
        $where="where idutilisateur='".$id."'";       
        $data['result']=$this->DAOGen->ReadGen("utilisateur",$where);
        $this->load->view("admin/ficheModifEleve",$data);


    }
    public function getForum(){
         // echo var_dump($this->input->post('classe'));
        $u_rec_id = $this->session->userdata('idutilisateur');
        $this->load->model('MetierForum');
        $config = array();
        $config["base_url"] = base_url() . "Utilisateur/getForum";
        $config["total_rows"] = $this->MetierForum->getAll($u_rec_id);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
       // echo  var_dump($this->uri->segment(3));
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["links"] = $this->pagination->create_links();

        $data['authors'] = $this->MetierForum->get_authors($u_rec_id,$config["per_page"], $page);

     //   $this->load->view('authors/index', $data);
        
        $this->load->view('Prof/Forum',$data);


    }
    //--------------CRUD Prof---------------------//
    public function ModifierProf($idutilisateur){
        $requete="where idutilisateur='".$idutilisateur."'";
        $daogen=$this->DAOGen->ReadGen("utilisateur",$requete);
        $data['result']=$daogen;
        $this->load->view("admin/FicheModicationProf",$data);
    }
    public function SupprimerProf($id){
        $where=array
        (
            "idutilisateur"=>$id
        );
        $change=array
        (
            "etat"=>0
        );
        $ec=array
        (
            "idutilisateur"=>NULL
        );

        $this->DAOGen->update($change,$where,"utilisateur");
        //$this->DAOGen->update($ec,$where,"ec");
        $listeidprof=$this->DAOGen->ReadGen("prof","where idutilisateur=".$id);
        for($i=0;$i<count($listeidprof);$i++){
            $this->db->query("update prof SET idutilisateur=null where idprof=".$listeidprof[$i]->idprof);
            $this->db->query("update matiereauchoix SET idprof=null where idprof=".$listeidprof[$i]->idprof);

        }
        
        
        
        
        redirect("Utilisateur/getListeProf");    
    }
    public function SubmitProfmodification($ideleve){
        //echo "ato o";
        try{
            $requete="where idutilisateur='".$ideleve."'";
            $daogen=$this->DAOGen->ReadGen("utilisateur",$requete);
            if($daogen[0]->idprofile!=3){
                throw new \Exception("Vous ne pas modifier un prof là mais un élève");
            }
            $where2="where idutilisateur='".$ideleve."'";
            $test=$this->DAOGen->ReadGen("utilisateur",$where2);
        
            $utilisateur=new UtilisateurClasse($this->input->post('nom'),$this->input->post('prenom'),$this->input->post('email'),$this->input->post('date'),$test[0]->email);
            echo var_dump($utilisateur);
            $id=array(
                "idutilisateur"=>$ideleve
            );
            echo $this->input->post('nom');
            $where=array(
                "Nom"=>$this->input->post('nom'),
                "Prenom"=>$this->input->post('prenom'),
                "email"=>$this->input->post('email'),
                "date"=>$this->input->post('date')
            );
            $this->DAOGen->update($where,$id,"utilisateur");
            redirect("Utilisateur/getListeProf");
            

        }catch(\Exception $e){
            $where="where idutilisateur='".$ideleve."'";       
            $data['result']=$this->DAOGen->ReadGen("utilisateur",$where);
            $data['error']=$e->getMessage();
            $this->load->view("admin/ficheModifEleve",$data);


        }

    }
//    RechercheProf
    public function RechercheProf(){
        $nom=$this->input->post('nom');
        $prenom=$this->input->post('prenom');
       
        //$select="0";
        
        $this->load->model('MetierUtilisateur');
    //    $this->MetierUtilisateur->RechercheEleve($name,$prenom,$select);
        //
        $this->load->model('DAOUtilisateur');
        $config = array();
        $config["base_url"] = base_url() . "Utilisateur/RechercheProf";
        $config["total_rows"] = $this->MetierUtilisateur->CountAllRechercheProf($nom,$prenom);
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);
  //      echo $this->uri->segment(3);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $data['result'] = $this->MetierUtilisateur->getLimitRechercheProf($nom,$prenom,$config["per_page"], $page);
       // $data['result']=$eleve;
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
      //  $data['select']=$select;
       //  $this->load->view('admin/ResultatRechercheProf',$data);
  //       $this->load->view('admin/ResultatRechercheProf',$data);









    }
     //------------Cote eleve Admin---------------------
   //-----------Delete
   public function getStat(){
        $idanneescolaire=$this->input->post('idanneescolaire');
    //    $idanneescolaire=1;
        $data['anneescolaire']=$this->DAOGen->RequeteFull("select idanneescolaire,CONCAT(debut,'-',fin) as debut FROM anneescolaire");
        $result=$this->DAOGen->ReadGen("filtrefromage","where idanneescolaire=".$idanneescolaire);
        $data['listeEleve']=$this->DAOGen->RequeteFull("
        SELECT *,
        CASE
        WHEN statut = 0 THEN 'Redoublant'
        WHEN statut = 1 THEN 'Passant' 
    END as info

FROM inscriptionfull where idanneescolaire=$idanneescolaire");
        for($i=0;$i<count($result);$i++){
            if($result[$i]->statut==1){
                $result[$i]->statut="Passant";
            }
            else{
                $result[$i]->statut="Redoublant";
            }
        }
        if(count($result)==1){
            $data['courbe'] = array( 
                array("label"=>$result[0]->statut, "y"=>100.0),
            );
        }else if(count($result)==2){
            $getotal=$this->DAOGen->RequeteFull("select sum(total) as somme from filtrefromage where idanneescolaire=".$idanneescolaire);
            $data['courbe'] = array( 
                array("label"=>$result[0]->statut, "y"=>(100*$result[0]->somme)/100),
                array("label"=>$result[1]->statut, "y"=>(100*$result[1]->somme)/100),
            );
        }else if(count($result)==0){

        }
        $this->load->view("admin/admindash",$data);
   }
   public function SupprimerEleve($id){
    $where=array
    (
        "idutilisateur"=>$id
    );
    $change=array
    (
        "etat"=>0
    );
    $ec=array
    (
        "idutilisateaur"=>NULL
    );
    $this->DAOGen->update($change,$where,"utilisateur");
    redirect("Utilisateur/getListeEleve");    
    }
    public function SubmitElevemodification($ideleve){
        //echo "ato o";
        try{
            $where2="where idutilisateur='".$ideleve."'";
            $test=$this->DAOGen->ReadGen("utilisateur",$where2);
        
            $utilisateur=new UtilisateurClasse($this->input->post('nom'),$this->input->post('prenom'),$this->input->post('email'),$this->input->post('date'),$test[0]->email);
            echo var_dump($utilisateur);
            $id=array(
                "idUtilisateur"=>$ideleve
            );
            echo $this->input->post('nom');
            $where=array(
                "Nom"=>$this->input->post('nom'),
                "Prenom"=>$this->input->post('prenom'),
                "email"=>$this->input->post('email'),
                "date"=>$this->input->post('date')
            );
            $this->DAOGen->update($where,$id,"utilisateur");
            redirect("Utilisateur/getListeEleve");
            

        }catch(\Exception $e){
            $where="where idutilisateur='".$ideleve."'";       
            $data['result']=$this->DAOGen->ReadGen("utilisateur",$where);
            $data['error']=$e->getMessage();
            $this->load->view("admin/ficheModifEleve",$data);


        }

    }
    public function RechercheEleve(){
        $nom=$this->input->get('nom');
        $prenom=$this->input->get('prenom');
        $select=$this->input->get('choix');
        //$select="0";
        
        $this->load->model('MetierUtilisateur');
    //    $this->MetierUtilisateur->RechercheEleve($name,$prenom,$select);
        //
        $this->load->model('DAOUtilisateur');
        $config = array();
        $config["base_url"] = base_url() . "Utilisateur/RechercheEleve";
        $config["total_rows"] = $this->MetierUtilisateur->CountAllRechercheEleve($nom,$prenom,$select);
        $config["per_page"] = 9;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);
  //      echo $this->uri->segment(3);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
        $data['result'] = $this->MetierUtilisateur->getLimitRechercheEleve($nom,$prenom,$select,$config["per_page"], $page);
       // $data['result']=$eleve;
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $data['select']=$select;
         $this->load->view('admin/ResultatRechercheEleve',$data);
  
    

    }
    //--------------Cote Prof--------------------
    public function GetClasse(){
        $u_rec_id = $this->session->userdata('idutilisateur');
        $requete="select distinct classe,idclasse from detailec where idutilisateur='".$u_rec_id."'";
        $listeClasse=$this->DAOGen->RequeteSpecifique($requete);
        $test =new Jsend ("success","Ok",$listeClasse);   
        echo json_encode($test);
    }
  //  GetNotes/'.$result[$i]->idutilisateur."/".$idannee
    public function GetNotes($idutilisateur,$idannee){
         
        $notesfinal=$this->DAOGen->RequeteFull("select (calculuesnexamenapresnom .note*calculuesnexamenapresnom .coefficient) as note,note as notesans,idutilisateur,idue,idanneescolaire,classe,idclasse,coefficient,reference,position,nom,prenom from calculuesnexamenapresnom  where idutilisateur=".$idutilisateur." and idanneescolaire=".$idannee);
        $result=$this->DAOGen->ReadGen("utilisateurrattrapage","where idutilisateur=".$idutilisateur." and idanneescolaire=".$idannee);
        $infoinscription=$this->DAOGen->ReadGen("inscription","where idutilisateur=".$idutilisateur." and idanneescolaire=".$idannee);
        $utilisateur=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$idutilisateur);
        for($j=0;$j<count($notesfinal);$j++){
            if($notesfinal[$j]->notesans>=10){
            $notesfinal[$j]->idutilisateur="Session normal";
            }
            else{
                $notesfinal[$j]->idutilisateur="";
             
            }
        }
        
        if(count($result)>0){
            $listeUE=$this->DAOGen->ReadGen("ratrrapagetotalue","where idutilisateur=".$idutilisateur." and idanneescolaire=".$idannee." and note>=10");
            for($i=0;$i<count($notesfinal);$i++){
                $notesfinal[$i]->reference="UE".$notesfinal[$i]->position." ".$notesfinal[$i]->reference;
                for($ii=0;$ii<count($listeUE);$ii++){
                    if($notesfinal[$i]->idue==$listeUE[$ii]->idue){
                        $notesfinal[$i]->note=$listeUE[$ii]->note*$notesfinal[$i]->coefficient;
                        $notesfinal[$i]->idutilisateur="Session rattrapage";
                    }
                        

                    

                }
            }
        }else{
        for($i=0;$i<count($notesfinal);$i++){
            $notesfinal[$i]->reference="UE".$notesfinal[$i]->position." ".$notesfinal[$i]->reference;
            if($notesfinal[$i]->notesans>=10){
                $notesfinal[$i]->idutilisateur="Session normal";
                }
                else{
                    $notesfinal[$i]->idutilisateur="";
                }

        }
        }

        $data['result']=$notesfinal;
      //  echo var_dump($notesfinal);
     // RechercheNoteRetour($idclasse,$idanneescolaire)
        $data['lienretour']="".base_url('UEController/RechercheNoteRetour/'.$infoinscription[0]->idclasse."/".$idannee)."";
                
        $data['nom']=ucfirst($utilisateur[0]->prenom);
        $this->load->view('Prof/ListesNoteAnnee',$data);  
    }
   // ---------------Etudiant--------------------
   

   public function GetClasseEleve(){
    $u_rec_id = $this->session->userdata('idutilisateur');
  //  $ewault=$this->DAOGen->ReadGen("inscriptionClasse","where idutilisateur=".$u_rec_id);
        $ewault=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");
    $requete="select *from inscriptionClasse where idinscription=".$ewault[0]->idinscription;
    $listeClasse=$this->DAOGen->RequeteSpecifique($requete);
    $e=$listeClasse[0];
    $test =new Jsend ("success","Ok",$listeClasse);   
    echo json_encode($test);
   }
   public function GetUEClasse(){
    $u_rec_id = $this->session->userdata('idutilisateur');
 //   $ewault=$this->DAOGen->ReadGen("inscriptionClasse","where idutilisateur=".$u_rec_id);
    $ewault=$this->DAOGen->RequeteFull("select inscriptionClasse.classe,inscriptionClasse.idclasse,inscriptionClasse.idinscription from inscriptionClasse where idutilisateur=".$u_rec_id." and idanneescolaire is not null  order by idinscription desc");
    $requete="select *from inscriptionClasse where idinscription=".$ewault[0]->idinscription;
    $listeClasse=$this->DAOGen->RequeteSpecifique($requete);
    $e=$listeClasse[0];
    $listeUE=$this->DAOGen->ReadGen("ue","where idclasse=".$e->idclasse);
    $test =new Jsend ("success","Ok",$listeUE);  
    echo json_encode($test);    




   }
    //---
   public function accueilEtudiantMatiere(){
        $this->load->model('DAOGen');
        $annee=$this->DAOGen->ReadGen("anneescolaire","where etat=1");

   }
   public function GetClasseFull(){
        $this->load->model('DAOGen');
        $listeUE=$this->DAOGen->ReadGen("classe",null);
        $test =new Jsend ("success","Ok",$listeUE);  
        echo json_encode($test);    

   }

   public function inscription(){
   // echo var_dump($this->input->post());
    try{
    $anneescolaire=$this->DAOGen->ReadGen("anneescolaire","where etat=1"); 
    if(count($anneescolaire)==0){
            throw new \Exception("La nouvelle année scolaire n'a pas encore débuté");
    }
    if($this->input->post('bordereaux')==null){
        throw new \Exception("Veuillez compléter le bordereau");
    }
    
    
    $inscription=array(
        "idinscription" => $this->input->post('idinscription')
     );
     $condition=array(
        'idclasse' => $this->input->post('choix'),
        'idanneescolaire'=>$anneescolaire[0]->idanneescolaire,
        'bordereau' => $this->input->post('bordereaux'),
     );
     $getinfoInscription=$this->DAOGen->ReadGen("inscription","where idinscription=".$this->input->post('idinscription'));
     if($getinfoInscription[0]->statut==0){
                $getnoteplus10=$this->DAOGen->ReadGen("obtention","where idutilisateur=".$getinfoInscription[0]->idutilisateur." and mention is not null");
                if(count($getnoteplus10)>0){
                    for($i=0;$i<count($getnoteplus10);$i++){
                    $totaluepares=array(
                        "idutilisateur"=>$getinfoInscription[$i]->idutilisateur,
                        "idue"=>$getnoteplus10[$i]->idue,
                        "idanneescolaire"=>$anneescolaire[0]->idanneescolaire,
                        "note"=>$getnoteplus10[$i]->note
                    );
                    $this->DAOGen->create($totaluepares,null,null,"totalueapres");
                }
            }



     }
     $this->DAOGen->update($condition,$inscription,'inscription');
     redirect('Utilisateur/ListeInscription');
    }catch(\Exception $e){

        $data['error']="La nouvelle année scolaire n'a pas encore débuté";
        $data['result']=$this->DAOGen->RequeteFull("select utilisateur.nom, utilisateur.prenom, inscription.*, classe.classe FROM utilisateur INNER JOIN inscription ON utilisateur.idutilisateur = inscription.idutilisateur INNER JOIN classe ON inscription.idclasse = classe.idclasse where idanneescolaire is null and inscription.idclasse is  not null");
        $data['classe']=$this->DAOGen->ReadGen("classe",null);
        $this->load->view("admin/ListeInscription",$data);
    }
   }
   public function ListeInscription(){
       $data['result']=$this->DAOGen->RequeteFull("select utilisateur.nom, utilisateur.prenom, inscription.*, classe.classe FROM utilisateur INNER JOIN inscription ON utilisateur.idutilisateur = inscription.idutilisateur INNER JOIN classe ON inscription.idclasse = classe.idclasse where idanneescolaire is null and inscription.idclasse is  not null");
       $data['classe']=$this->DAOGen->ReadGen("classe",null);
       for($i=0;$i<count($data['result']);$i++){
                $idclasse=$data['result'][$i]->idclasse;
                $classe=array_values(array_filter($data['classe'],function($a) use ($idclasse){return $a->idclasse==$idclasse;}));       
                $precedent=$classe[0]->precedent;
                if($precedent!=null){
                $getprecedent=array_values(array_filter($data['classe'],function($a) use ($precedent){return $a->precedent==$precedent;}));
            //    echo var_dump($getprecedent);    
                for($jj=0;$jj<count($getprecedent);$jj++){
                        $data['result'][$i]->idclasse="<option value=".$getprecedent[$jj]->idclasse.">".$getprecedent[$jj]->classe."</option>"." ".$data['result'][$i]->idclasse;
                    }
                 
                    }else{
                            $data['result'][$i]->idclasse="<option value=".$data['result'][$i]->idclasse.">".$data['result'][$i]->classe."</option>";
   
                }

       }
       // echo var_dump($data['result']);
          $this->load->view("admin/ListeInscription",$data);
    
    }
   //--Auto COmplete Prof
   public function AutoListeProfEc(){
        $term=$this->input->get('term');
        $this->load->model('DAOGen');
        $result=$this->DAOGen->ReadGen("utilisateur","where idprofile='3' and nom LIKE '%".$term."%' order by Nom asc limit 10");
      //  echo var_dump($result);
        $list=array();
        foreach($result as $result){
            $list[]=$result->nom." ".$result->prenom;
        }
        echo json_encode($list);
   }

   public function AutoListeFull(){
    $term=$this->input->get('term');
    $this->load->model('DAOGen');
    $result=$this->DAOGen->ReadGen("utilisateur","where nom LIKE '%".$term."%' order by Nom asc limit 10");
  //  echo var_dump($result);
    $list=array();
    foreach($result as $result){
        $list[]=$result->nom." ".$result->prenom;
    }
    echo json_encode($list);
    }

    
   public function AutoListeEtudiant(){
    $term=$this->input->get('term');
    $this->load->model('DAOGen');
    $result=$this->DAOGen->ReadGen("utilisateur","where nom LIKE '%".$term."%' and idprofile=3 order by Nom asc limit 10");
  //  echo var_dump($result);
    $list=array();
    foreach($result as $result){
        $list[]=$result->nom." ".$result->prenom;
    }
    echo json_encode($list);
    }
   public function testAutocomplet(){
       $this->load->view('Autocomplete');
   }
   public function getNombreMessage(){
       $u_rec_id=$this->session->userdata('idutilisateur');
       $listeConsultation=$this->DAOGen->ReadGen("consultationmessage","where etat=0 and idutilisateur=".$u_rec_id);
       $result=count($listeConsultation);
       $employee_object = new stdClass;
       $employee_object->nombre=$result;
       echo json_encode($employee_object);
   }
   public function getNombreForum(){
       $u_rec_id = $this->session->userdata('idutilisateur');
       $listeConsultation=$this->DAOGen->ReadGen("consultationforum","where etat=0 and idutilisateur=".$u_rec_id);
       $result=count($listeConsultation);
       $employee_object = new stdClass;
       $employee_object->nombre=$result;
       echo json_encode($employee_object);
   }
   public function getAccueil(){
    $u_rec_id = $this->session->userdata('idutilisateur');
    $listeConsultation=$this->DAOGen->ReadGen("utilisateur","where idutilisateur=".$u_rec_id);
    if($listeConsultation[0]->idprofile==1){
        $this->load->view('admin/Admin'); 
    }else if($listeConsultation[0]->idprofile==2){
        $this->load->view('Etudiant/index'); 
    }
    else if($listeConsultation[0]->idprofile==3){
        //$this->load->view('Prof/index'); 
        redirect('Utilisateur/getDashProf/0');
    }
    
   }
   public function getDashProf($idanneescolaire){
        if($idanneescolaire==0){
            $anneescolaire=$this->DAOGen->RequeteFull("select max(idanneescolaire) as id from anneescolaire");
            if(count($anneescolaire)==0){
                $data['listedevoir']=[];
                $data['listeEleve']=[];
                $data['anneescolaire']=[];
                $this->load->view('Prof/index',$data); 

            }else{
                $listedevoir=[];
                $u_rec_id = $this->session->userdata('idutilisateur');
                $listedevoir = $this->DAOGen->ReadGen("devoir","where idutilisateur=".$u_rec_id." and idanneescolaire=".$anneescolaire[0]->id."");
                if(count($listedevoir)>0){
                    $lis=$this->DAOGen->FonctiongetCreateORVersion2($listedevoir,"iddevoir"); 
                    $getDevoir=$this->DAOGen->ReadGen("participantdevoircomplet","where ".$lis);
                for($i=0;$i<count($listedevoir);$i++){
                    $iddevoir=$listedevoir[$i]->iddevoir;
                    $resultat=array_filter($getDevoir,function($a) use ($iddevoir){return $a->iddevoir==$iddevoir;});
                    $resultatfini=array_values(array_filter($resultat,function($a){return $a->idfichier!=null;}));
                    $resultatnonfini=array_values(array_filter($resultat,function($a){return $a->idfichier==null;}));
                    $listedevoir[$i]->etat=round((count($resultatfini)*100)/count($resultat));
                }       
            } else{
                $listedevoir=[];
            }
                
            $listeEleve=$this->DAOGen->RequeteFull("select utilisateur.nom,utilisateur.email, utilisateur.prenom, inscription.*, classe.classe FROM utilisateur INNER JOIN inscription ON utilisateur.idutilisateur = inscription.idutilisateur INNER JOIN classe ON inscription.idclasse = classe.idclasse where idanneescolaire=".$anneescolaire[0]->id);
            for($i=0;$i<count($listeEleve);$i++){
                if($listeEleve[$i]->statut==1){
                    $listeEleve[$i]->statut="<span class='badge bg-success'>Passant</span>";
                }else{
                    $listeEleve[$i]->statut="<span class='badge bg-danger'>Redoublant</span>";
                }

            }
            $data['listeEleve']=$listeEleve;
            $data['listedevoir']=$listedevoir;
            $result=$this->DAOGen->ReadGen("anneescolaire",null);
            for($i=0;$i<count($result);$i++){
                 $result[$i]->debut=$result[$i]->debut."-".$result[$i]->fin;
            }
            $data['anneescolaire']=$result;
               // echo var_dump($listedevoir);
              $this->load->view('Prof/index',$data); 




            }
        }else{
        
        $u_rec_id = $this->session->userdata('idutilisateur');
        $listedevoir = $this->DAOGen->ReadGen("devoir","where idutilisateur=".$u_rec_id." and idanneescolaire=".$idanneescolaire."");
        $lis=$this->DAOGen->FonctiongetCreateORVersion2($listedevoir,"iddevoir");
        $getDevoir=$this->DAOGen->ReadGen("participantdevoircomplet","where ".$lis);
        for($i=0;$i<count($listedevoir);$i++){
               $iddevoir=$listedevoir[$i]->iddevoir;
               $resultat=array_filter($getDevoir,function($a) use ($iddevoir){return $a->iddevoir==$iddevoir;});
               
               $resultatfini=array_values(array_filter($resultat,function($a){return $a->idfichier!=null;}));
               $resultatnonfini=array_values(array_filter($resultat,function($a){return $a->idfichier==null;}));
               $listedevoir[$i]->etat=round((count($resultatfini)*100)/count($resultat));
            }
            $listeEleve=$this->DAOGen->RequeteFull("select utilisateur.nom,utilisateur.email, utilisateur.prenom, inscription.*, classe.classe FROM utilisateur INNER JOIN inscription ON utilisateur.idutilisateur = inscription.idutilisateur INNER JOIN classe ON inscription.idclasse = classe.idclasse where idanneescolaire=".$idanneescolaire);
           
            for($i=0;$i<count($listeEleve);$i++){
                if($listeEleve[$i]->statut==1){
                    $listeEleve[$i]->statut="<span class='badge bg-success'>Passant</span>";
                }else{
                    $listeEleve[$i]->statut="<span class='badge bg-danger'>Redoublant</span>";
                }

            }
            
            $data['listeEleve']=$listeEleve;
            $data['listedevoir']=$listedevoir;
         //   $data['listedevoir']=$listedevoir;
            $result=$this->DAOGen->ReadGen("anneescolaire",null);
            for($i=0;$i<count($result);$i++){
                 $result[$i]->debut=$result[$i]->debut."-".$result[$i]->fin;
            }
            $data['anneescolaire']=$result;
            $this->load->view('Prof/index',$data); 

        }
   }
   public function gett(){
    $u_rec_id = $this->session->userdata('idutilisateur');
    $forum=$this->DAOGen->ReadGen("consultationforum","where etat=0 and idutilisateur=".$u_rec_id);
    $result1=count($forum);
    $listeConsultation=$this->DAOGen->ReadGen("consultationmessage","where etat=0 and idutilisateur=".$u_rec_id);
    $result=count($listeConsultation);
    if($result+$result1>0){
        $employee_object = new stdClass;
        $employee_object->nombre=1;
        echo json_encode($employee_object);

    }else{
        $employee_object = new stdClass;
       $employee_object->nombre="";
       echo json_encode($employee_object);

    }
   }
}


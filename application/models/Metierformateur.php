<?php
   Class Metierformateur extends CI_Model { 
//	insert into Project values  (Concat('PRO',nextval('idproject')),'Fampiasana','1994/11/13','2000/11/13');
//insert into taches values  (Concat('TA',nextval('itaches')),'PRO1','Design','TA1',1);
      Public function __construct() { 
         parent::__construct();
         $this->load->model('DAOGen'); 
     }
     public function changement($nom){
        // $arr1 = str_split($nom);
        // return $arr1[7];
     
        $pieces = explode(":", $nom);
       // echo $pieces[0]; // piece1
       // echo $pieces[1]; // piece2
         return $pieces[1];
     }
    public function setnom($nom){
        if($nom==null || $nom==""){
            throw new \Exception("Veuillez completer votre nom") ;       
        }
    }
    public function setprenom($prenom){
        if($prenom==null || $prenom==""){
            throw new \Exception("Veuillez completer votre nom") ;       
        }

    }
    public function setnumero($numero){
        if($numero==null || $numero==""){
            throw new \Exception("Veuillez completer votre numero") ;       
        }
        else{
            $this->checknumero2($numero);
        }

    }
    public function setdirection($direction){
        if($direction==null || $direction==""){
            throw new \Exception("Veuillez completer votre direction") ;       
        }

    }
    public function setemail($email){
        if($email==null || $email==""){
            throw new \Exception("Veuillez completer votre email") ;       
        }
        else{
            $this->checkemail($email);
        }

    }
    
    public function setmdp($mdp){
        if($mdp==null || $mdp==""){
            throw new \Exception("Veuillez completer votre mot de passe") ;       
        }

    }
    //--------xxxx----------controle
    public function checkemail($marque){
        if (!filter_var($marque,FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Email non valide");
        }
     }
     public function checknumero2($numero){
        $pattern = "/^03[3|4|2][0-9]{7}/";
        $x=preg_match($pattern, $numero);
        if($x==0){
			throw new \Exception("Numero au Format 033 ou 034 ou 032");
		} 
     }
     public function doublemdp($mdp1,$mdp2){
            if($mdp1!=$mdp2){
                throw new \Exception("Mot de passe non identique");

            }

     }
     public  function doublenumero($num){
        $result=$this->DAOGen->ReadGen("formateur","where numero='".$num."'");
        if(count($result)>0){
            throw new \Exception("numero deja existant");
        }
     }
     public function doublonumeroedit($Numero,$idutilisateur){
        $x=$this->DAOGen->ReadGen("formateur","where etat=1 and numero='".$Numero."'");
        if(count($x)>0){
            if($x[0]->idformateur!=$idutilisateur){
                throw new \Exception("Numero deja existant");
            }
        }
     }
     public function doubloemailedit($email,$idutilisateur){
        $x=$this->DAOGen->ReadGen("formateur","where etat=1 and email='".$email."'");
        if(count($x)>0){
            if($x[0]->idformateur!=$idutilisateur){
                throw new \Exception("Email deja existant");
            }
        }
     }
      public function controleEmail($marque){
        $this->load->Model('DAOGen');
        $result=$this->DAOGen->ReadGen("formateur","where email='".$marque."'");
        if(count($result)>0){
            throw new Exception("Email existant");
        }
     } 
     public function doublonemail($email){
        $result=$this->DAOGen->ReadGen("formateur","where email='".$email."'");
        if(count($result)>0){
            throw new \Exception("email deja existant");
        }
     }   
     public function controleajout($nom,$prenom,$mdp,$numero,$email,$mdp2){
        $this->setnom($nom);
        $this->setprenom($prenom);
        $this->doublemdp($mdp,$mdp2);
        $this->setmdp($mdp);
        $this->setnumero($numero);
        $this->setemail($email);
        $this->doublonemail($email);
    }
    public function controleedit($nom,$prenom,$mdp,$numero,$email,$mdp2,$idutilisateur){
        $this->setnom($nom);
        $this->setprenom($prenom);
        $this->doublemdp($mdp,$mdp2);
        $this->setmdp($mdp);
        $this->doublonumeroedit($numero,$idutilisateur);
        $this->doubloemailedit($email,$idutilisateur);
    }
    
    

   } 
?> 
<?php namespace App\models;
defined('BASEPATH') OR exit('No direct script access allowed');
class Connection
{
public function Connection()
{
    static $connect = null;
    if ($connect === null) {
        $connect = new \PDO('mysql:host=localhost;dbname=simulation2', 'root', 'root');
    }
    return $connect;
}
}
?>
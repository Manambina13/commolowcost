document.addEventListener('DOMContentLoaded', function() {

    //  var url ='./';
    var url ='http://localhost/CommoStagiairemysql/CalendrierControlleur/';
  
      $('body').on('click', '.datetimepicker', function() {
          $(this).not('.hasDateTimePicker').datetimepicker({
              controlType: 'select',
              changeMonth: true,
              changeYear: true,
              dateFormat: "dd-mm-yy",
              timeFormat: 'HH:mm:ss',
              yearRange: "1900:+10",
              showOn:'focus',
              firstDay: 1
          }).focus();
      });
  
      $(".colorpicker").colorpicker();
      console.log(url+'load');
      var calendarEl = document.getElementById('calendar');
  
      var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
          header: {
              left: 'prev,next today',
              center: 'title',
              right: 'dayGridMonth,timeGridWeek'
          },
          locale: 'fr',
          defaultView: 'timeGridWeek',
          navLinks: true, // can click day/week names to navigate views
          businessHours: true, // display business hours
          editable: true,
          //uncomment to have a default date
          //defaultDate: '2020-04-07',
          events: url+'load',
          eventDrop: function(arg) {
              var start = arg.event.start.toDateString()+' '+arg.event.start.getHours()+':'+arg.event.start.getMinutes()+':'+arg.event.start.getSeconds();
              if (arg.event.end == null) {
                  end = start;
              } else {
                  var end = arg.event.end.toDateString()+' '+arg.event.end.getHours()+':'+arg.event.end.getMinutes()+':'+arg.event.end.getSeconds();
              }
  
              $.ajax({
                url:url+"editevent",
                type:"POST",
                data:{id:arg.event.id, start:start, end:end},
              });
          },
          eventResize: function(arg) {
              var start = arg.event.start.toDateString()+' '+arg.event.start.getHours()+':'+arg.event.start.getMinutes()+':'+arg.event.start.getSeconds();
              var end = arg.event.end.toDateString()+' '+arg.event.end.getHours()+':'+arg.event.end.getMinutes()+':'+arg.event.end.getSeconds();
  
              $.ajax({
                url:url+"editevent",
                type:"POST",
                data:{id:arg.event.id, start:start, end:end},
              });
          },
      });
  
      calendar.render();
  
      
  
      $('#createEvenement').submit(function(event) {
  
          // stop the form refreshing the page
          event.preventDefault();
  
          $('.form-group').removeClass('has-error'); // remove the error class
          $('.help-block').remove(); // remove the error text
      
          // process the form
          $.ajax({
              type        : "POST",
              url         : url+'AjoutEvent',
              data        : $(this).serialize(),
              dataType    : 'json',
              encode      : true
          }).done(function(data) {
  
                  
              // insert worked
              
              if (data.success) {
                  
                  //remove any form data
                  $('#createEvenement').trigger("reset");
  
                  //close model
                  $('#addevent').modal('hide');
  
                  //refresh calendar
                  calendar.refetchEvents();
  
              } else {
  
                  //if error exists update html
                  if (data.errors.date) {
                      $('#date-group').addClass('has-error');
                      $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                  }
  
                  if (data.errors.title) {
                      $('#title-group').addClass('has-error');
                      $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                  }
  
              }
  
          });
      });
      $('#createEvent2').submit(function(event) {
  
          // stop the form refreshing the page
          event.preventDefault();
          console.log("ato 2");
          $('.form-group').removeClass('has-error'); // remove the error class
          $('.help-block').remove(); // remove the error text
  
          // process the form
          $.ajax({
              type        : "POST",
              url         : url+'AjoutEventEmploi',
              data        : $(this).serialize(),
              dataType    : 'json',
              encode      : true
          }).done(function(data) {
  
                  console.log(data);
              // insert worked
              
              if (data.success) {
                  console.log("ato le succes")
                  //remove any form data
                  $('#createEvent2').trigger("reset");
  
                  //close model
                  $('#addevenemploi').modal('hide');
  
                  //refresh calendar
                  calendar.refetchEvents();
  
              } else {
  
                  //if error exists update html
                  if (data.errors.date) {
                      $('#date-group').addClass('has-error');
                      $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                  }
  
                  if (data.errors.title) {
                      $('#title-group').addClass('has-error');
                      $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                  }
  
              }
  
          });
      });
  
      $('#editeventmodal').submit(function(event) {
  
          // stop the form refreshing the page
          event.preventDefault();
  
          $('.form-group').removeClass('has-error'); // remove the error class
          $('.help-block').remove(); // remove the error text
          
          //form data
          var id = $('#editEventId').val();
          var title = $('#editEventTitle').val();
          var start = $('#editStartDate').val();
          var end = $('#editEndDate').val();
     //     console.log(id);
       //   console.log(title);
         // console.log(start);
        //  console.log(end);
     
          // process the form
          $.ajax({
              type        : "POST",
              url         : url+'editevent',
              data        : {
                  id:id, 
                  title:title, 
                  start:start,
                  end:end,
              },
              dataType    : 'json'
          }).done(function(data) {
                console.log(data);
              // insert worked
              if (data.success) {
                  
                  //remove any form data
                  $('#editEvent').trigger("reset");
  
                  //close model
                  $('#editeventmodal').modal('hide');
  
                  //refresh calendar
                  calendar.refetchEvents();
  
              } else {
  
                  //if error exists update html
                  if (data.errors.date) {
                      $('#date-group').addClass('has-error');
                      $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                  }
  
                  if (data.errors.title) {
                      $('#title-group').addClass('has-error');
                      $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                  }
  
              }
  
          });
      });

      $('#editemploisdutemps').submit(function(event) {
  
        // stop the form refreshing the page
        event.preventDefault();

        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text
        
        //form data
        var id = $('#editEventId').val();
        var title = $('#modifclasse').val();
        var ue = $('#modifue').val();
        var modifidchoice = $('#modifidchoice').val();
        var modifec=$('#modifec').val();
        var start = $('#debut').val();
        var end = $('#fin').val();
        var prof=$('#modifprof').val();
        console.log("bozo");
        console.log(id);
        console.log(title);
        console.log(start);
        console.log(end);
        console.log(prof);
        console.log(modifidchoice);
        // process the form
        $.ajax({
            type        : "POST",
            url         : url+'editemploi',
            data        : {
                id:id,
                ue:ue,
                modifec:modifec, 
                title:title,
                idmatiereauchoix:modifidchoice, 
                idprof:prof,
                start:start,
                end:end,
            },
            dataType    : 'json'
        }).done(function(data) {
              console.log(data);
            // insert worked
            if (data.success) {
                
                //remove any form data
                $('#editEvent2').trigger("reset");

                //close model
                $('#editemploisdutemps').modal('hide');

                //refresh calendar
                calendar.refetchEvents();

            } else {

                //if error exists update html
                if (data.errors.date) {
                    $('#date-group').addClass('has-error');
                    $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                }

                if (data.errors.title) {
                    $('#title-group').addClass('has-error');
                    $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                }

            }

        });
    });





  });
  
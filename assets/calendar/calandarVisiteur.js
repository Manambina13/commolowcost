document.addEventListener('DOMContentLoaded', function() {

    var url = './';

    $('body').on('click', '.datetimepicker', function() {
        $(this).not('.hasDateTimePicker').datetimepicker({
            controlType: 'select',
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            timeFormat: 'HH:mm:ss',
            yearRange: "1900:+10",
            showOn: 'focus',
            firstDay: 1
        }).focus();
    });

    $(".colorpicker").colorpicker();

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
        },
        locale: 'fr',
        defaultView: 'dayGridMonth',
        navLinks: true, // can click day/week names to navigate views
        businessHours: true, // display business hours
        editable: true,
        //uncomment to have a default date
        //defaultDate: '2020-04-07',
        events: 'http://gestioncourriermedd.alwaysdata.net/api/v1/Calendar/loadvisiteur',
        eventDrop: function(arg) {
            var start = arg.event.start.toDateString() + ' ' + arg.event.start.getHours() + ':' + arg.event.start.getMinutes() + ':' + arg.event.start.getSeconds();
            if (arg.event.end == null) {
                end = start;
            } else {
                var end = arg.event.end.toDateString() + ' ' + arg.event.end.getHours() + ':' + arg.event.end.getMinutes() + ':' + arg.event.end.getSeconds();
            }

            $.ajax({
                url: 'http://gestioncourriermedd.alwaysdata.net/api/v1/Calendar/updatevisiteur',
                type: "POST",
                data: { id: arg.event.id, start: start, end: end },
            });
        },
        eventResize: function(arg) {
            var start = arg.event.start.toDateString() + ' ' + arg.event.start.getHours() + ':' + arg.event.start.getMinutes() + ':' + arg.event.start.getSeconds();
            var end = arg.event.end.toDateString() + ' ' + arg.event.end.getHours() + ':' + arg.event.end.getMinutes() + ':' + arg.event.end.getSeconds();

            $.ajax({
                url: 'http://gestioncourriermedd.alwaysdata.net/api/v1/Calendar/updatevisiteur',
                type: "POST",
                data: { id: arg.event.id, start: start, end: end },
            });
        },
        eventClick: function(arg) {


            var id = arg.event.id;
            $('#editEventId').val(id);
            $('#deleteEvent').attr('data-id', id);
            console.log(id);
            $.ajax({
                url: 'http://gestioncourriermedd.alwaysdata.net/api/v1/Calendar/geteventvisiteur',
                type: "POST",
                dataType: 'json',
                data: { id: id },
                success: function(data) {
                    $('#editEventTitle').val(data.title);
                    $('#editStartDate').val(data.start);
                    $('#editEndDate').val(data.end);
                    $('#editColor').val(data.color);
                    $('#editTextColor').val(data.textColor);
                    $('#editDirection').val(data.direction);
                    $('#editContact').val(data.contact);
                    $('#editeventmodal').modal();
                    console.log(data);
                }
            });

            $('body').on('click', '#deleteEvent', function() {
                if (confirm("Voulez-vous vraiment le supprimer?")) {
                    $.ajax({
                        url: 'http://gestioncourriermedd.alwaysdata.net/api/v1/Calendar/deletevisiteur',
                        type: "POST",
                        data: { id: arg.event.id },
                    });

                    //close model
                    $('#editeventmodal').modal('hide');

                    //refresh calendar
                    calendar.refetchEvents();
                }
            });

            calendar.refetchEvents();
        }
    });

    calendar.render();

    $('#createEvent').submit(function(event) {

        // stop the form refreshing the page
        event.preventDefault();

        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text

        console.log($(this).serialize());
        $.ajax({
            url: 'http://gestioncourriermedd.alwaysdata.net/api/v1/Calendar/createvisiteur',
            type: 'POST',
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data, textStatus, xhr) {
                console.log(data); //remove any form data

                // insert worked
                if (data.success) {

                    //remove any form data
                    $('#createEvent').trigger("reset");

                    //close model
                    $('#addeventmodal').modal('hide');

                    //refresh calendar
                    calendar.refetchEvents();

                } else {

                    //if error exists update html
                    if (data.errors.date) {
                        $('#date-group').addClass('has-error');
                        $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                    }

                    if (data.errors.title) {
                        $('#title-group').addClass('has-error');
                        $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                    }

                }

            },
            error: function(xhr, textStatus, errorThrown) {
                console.log('Error in Operation');
            }
        });


    });
    $('#editEvent').submit(function(event) {

        // stop the form refreshing the page
        event.preventDefault();

        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text

        //form data
        var id = $('#editEventId').val();
        var title = $('#editEventTitle').val();
        var start = $('#editStartDate').val();
        var end = $('#editEndDate').val();
        var color = $('#editColor').val();
        var textColor = $('#editTextColor').val();
        var direction = $('#editDirection').val();
        var contact = $('#editContact').val();
        console.log(id);

        // process the form
        $.ajax({
            url: 'http://gestioncourriermedd.alwaysdata.net/api/v1/Calendar/updatevisiteur',
            type: 'POST',
            data: {
                id: id,
                title: title,
                'startDate': start,
                'endDate': end,
                color: color,
                text_color: textColor,
                direction: direction,
                contact: contact
            },
            dataType: 'json',

            success: function(data, textStatus, xhr) {
                //remove any form data
                console.log(data.success);
                // insert worked
                if (data.success === true) {
                    //              console.log('atooo');
                    //remove any form data
                    $('#editEvent').trigger("reset");

                    //close model
                    $('#editeventmodal').modal('hide');

                    //refresh calendar
                    calendar.refetchEvents();

                } else {

                    //if error exists update html
                    if (data.errors.date) {
                        $('#date-group').addClass('has-error');
                        $('#date-group').append('<div class="help-block">' + data.errors.date + '</div>');
                    }

                    if (data.errors.title) {
                        $('#title-group').addClass('has-error');
                        $('#title-group').append('<div class="help-block">' + data.errors.title + '</div>');
                    }

                }

            },
            error: function(xhr, textStatus, errorThrown) {
                console.log('Error in Operation');
            }
        });
    });
});
s
create or replace  view listeparcoursvue as (select assignationec.*,ec.nom_ec,niveau.chiffre,matiere.idmatiere,matiere.nom_matiere,matiere.idniveau,parcour.nom_parcour,parcour.idparcour from parcour join matiere on parcour.idparcour=matiere.idparcour join niveau on matiere.idniveau=niveau.idniveau join ec  on ec.idmatiere=matiere.idmatiere join assignationec on ec.idec=assignationec.idec  where ec.etat=1 and assignationec.etat=1 and ec.etat=1);





Create or replace view listefichieretudiant AS (select fichieretudiant.*,etudiant.nom as nom_etudiant,etudiant.prenom,matiere.nom_matiere,ec.nom_ec,assignation.idformateur,parcour.nom_parcour from fichieretudiant join etudiant  on fichieretudiant.idetudiant = etudiant.idetudiant join matiere on fichieretudiant.idmatiere = matiere.idmatiere join ec on ec.idmatiere=matiere.idmatiere join  assignation  on matiere.idmatiere = assignation.idmatiere join parcour on fichieretudiant.idparcour = parcour.idparcour and fichieretudiant.idparcour = parcour.idparcour and fichieretudiant.idparcour = parcour.idparcour and matiere.idparcour = parcour.idparcour where ec.etat=1 and matiere.etat=1 and  assignation.etat = 1);



Create or replace view listefichieretudiant AS (select fichieretudiant.*,etudiant.nom as nom_etudiant,etudiant.prenom,matiere.nom_matiere,ec.nom_ec,parcour.nom_parcour from fichieretudiant join etudiant  on fichieretudiant.idetudiant = etudiant.idetudiant join matiere on fichieretudiant.idmatiere = matiere.idmatiere join ec on ec.idmatiere=matiere.idmatiere join  assignation  on matiere.idmatiere = assignation.idmatiere join parcour on fichieretudiant.idparcour = parcour.idparcour and fichieretudiant.idparcour = parcour.idparcour and fichieretudiant.idparcour = parcour.idparcour and matiere.idparcour = parcour.idparcour where ec.etat=1 and matiere.etat=1 and  assignation.etat = 1);





CREATE TABLE edt (
  idedt INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  idanneeuniversitaire INTEGER UNSIGNED NOT NULL,
  idmatiere INTEGER UNSIGNED NOT NULL,
  idec INTEGER UNSIGNED NOT NULL,
  idparcour INTEGER UNSIGNED NOT NULL,
  debut DATETIME NULL,
  fin DATETIME NULL,
  PRIMARY KEY(idedt),
  FOREIGN KEY(idparcour)
    REFERENCES parcour(idparcour)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(idec)
    REFERENCES ec(idec)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(idmatiere)
    REFERENCES matiere(idmatiere)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(idanneeuniversitaire)
    REFERENCES anneeuniversitaire(idanneeuniversitaire)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);




INSERT INTO `edt` (`idniveau`, `idparcour`, `idformateur`, `idmatiere`, `idec`, `debut`, `fin`) VALUES ('1',
			'1', '2', '1', '1', '12-01-2022 00:00:00', '15-01-2022 00:00:00');